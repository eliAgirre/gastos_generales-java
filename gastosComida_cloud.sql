CREATE DATABASE  IF NOT EXISTS `ad_eb9a02d9ab87698` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ad_eb9a02d9ab87698`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: us-cdbr-iron-east-03.cleardb.net    Database: ad_eb9a02d9ab87698
-- ------------------------------------------------------
-- Server version	5.5.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ahorro_mensual`
--

DROP TABLE IF EXISTS `ahorro_mensual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahorro_mensual` (
  `idAhorro_M` int(11) NOT NULL,
  `fecha_inicio` varchar(10) DEFAULT NULL,
  `fecha_fin` varchar(10) DEFAULT NULL,
  `presu_M` double(9,2) DEFAULT NULL,
  `gastos_M` double(9,2) DEFAULT NULL,
  `ahorro_M` double(9,2) DEFAULT NULL,
  PRIMARY KEY (`idAhorro_M`),
  UNIQUE KEY `idAhorro_M_UNIQUE` (`idAhorro_M`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahorro_mensual`
--

LOCK TABLES `ahorro_mensual` WRITE;
/*!40000 ALTER TABLE `ahorro_mensual` DISABLE KEYS */;
/*!40000 ALTER TABLE `ahorro_mensual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahorro_semanal`
--

DROP TABLE IF EXISTS `ahorro_semanal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahorro_semanal` (
  `idAhorro` int(11) NOT NULL,
  `fecha_inicio` varchar(10) DEFAULT NULL,
  `fecha_fin` varchar(10) DEFAULT NULL,
  `presu_S` double(9,2) DEFAULT NULL,
  `gastos_S` double(9,2) DEFAULT NULL,
  `ahorro_S` double(9,2) DEFAULT NULL,
  PRIMARY KEY (`idAhorro`),
  UNIQUE KEY `idAhorro_S_UNIQUE` (`idAhorro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahorro_semanal`
--

LOCK TABLES `ahorro_semanal` WRITE;
/*!40000 ALTER TABLE `ahorro_semanal` DISABLE KEYS */;
INSERT INTO `ahorro_semanal` VALUES (1,'22/02/2016','28/02/2016',150.00,205.98,-55.98);
/*!40000 ALTER TABLE `ahorro_semanal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastos_mensuales`
--

DROP TABLE IF EXISTS `gastos_mensuales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastos_mensuales` (
  `idMensual` int(11) NOT NULL,
  `fecha_inicio` varchar(10) DEFAULT NULL,
  `fecha_fin` varchar(10) DEFAULT NULL,
  `total_M` double(9,2) DEFAULT NULL,
  PRIMARY KEY (`idMensual`),
  UNIQUE KEY `idMensual_UNIQUE` (`idMensual`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastos_mensuales`
--

LOCK TABLES `gastos_mensuales` WRITE;
/*!40000 ALTER TABLE `gastos_mensuales` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastos_mensuales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastos_producto`
--

DROP TABLE IF EXISTS `gastos_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastos_producto` (
  `idGastosP` int(11) NOT NULL,
  `fechaGP` varchar(10) DEFAULT NULL,
  `tienda` varchar(15) DEFAULT NULL,
  `producto` varchar(50) DEFAULT NULL,
  `costeP` double(9,2) DEFAULT NULL,
  PRIMARY KEY (`idGastosP`),
  UNIQUE KEY `idGastosP_UNIQUE` (`idGastosP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastos_producto`
--

LOCK TABLES `gastos_producto` WRITE;
/*!40000 ALTER TABLE `gastos_producto` DISABLE KEYS */;
INSERT INTO `gastos_producto` VALUES (1,'23/02/2016','Eroski','Baguette',0.99),(2,'23/02/2016','Eroski','Integral SinSal',0.94),(3,'25/02/2016','Eroski','Leche Semi Kaiku',0.95),(4,'25/02/2016','Eroski','Leche S/Lact Semi',1.44),(5,'25/02/2016','Eroski','Yogur de macedonia',1.09),(6,'25/02/2016','Eroski','Yogur de fresa',1.09),(7,'25/02/2016','Eroski','Pimiento de piquillo',1.00),(8,'25/02/2016','Eroski','Atun claro',4.75),(9,'26/02/2016','Eroski','Huevos L',2.10),(10,'26/02/2016','Eroski','Solomillo cerdo',3.49),(11,'26/02/2016','Eroski','Suavizante Flor',3.95),(12,'27/02/2016','Eroski','Platano de Canarias',1.18),(13,'27/02/2016','Eroski','Patata a granel',1.34),(14,'27/02/2016','Eroski','Lubina 400/60',14.25),(15,'27/02/2016','Eroski','Palillero',1.00),(16,'27/02/2016','Eroski','Zanahoria Natur 1',0.59),(17,'27/02/2016','Eroski','Pechuga fileteada',3.88),(18,'27/02/2016','Otro','Legging Domyos',9.98),(19,'27/02/2016','Otro','Mallas cortas Kalenji',9.95),(20,'27/02/2016','Otro','Jersey Quechua',14.95),(21,'27/02/2016','Otro','Camiseta Termica Kalenji',9.95);
/*!40000 ALTER TABLE `gastos_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastos_semanales`
--

DROP TABLE IF EXISTS `gastos_semanales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastos_semanales` (
  `idSemanal` int(11) NOT NULL,
  `fecha_inicio` varchar(10) DEFAULT NULL,
  `fecha_fin` varchar(10) DEFAULT NULL,
  `total` double(9,2) DEFAULT NULL,
  PRIMARY KEY (`idSemanal`),
  UNIQUE KEY `idSemanal_UNIQUE` (`idSemanal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastos_semanales`
--

LOCK TABLES `gastos_semanales` WRITE;
/*!40000 ALTER TABLE `gastos_semanales` DISABLE KEYS */;
INSERT INTO `gastos_semanales` VALUES (1,'22/02/2016','24/02/2016',82.89),(2,'22/02/2016','28/02/2016',205.98);
/*!40000 ALTER TABLE `gastos_semanales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastos_tienda`
--

DROP TABLE IF EXISTS `gastos_tienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastos_tienda` (
  `idGastosT` int(11) NOT NULL,
  `fechaGT` varchar(10) DEFAULT NULL,
  `tienda` varchar(15) DEFAULT NULL,
  `costeCompraT` double(9,2) DEFAULT NULL,
  `forma_pago` varchar(8) DEFAULT NULL,
  `tarjeta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idGastosT`),
  UNIQUE KEY `idGastosT_UNIQUE` (`idGastosT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastos_tienda`
--

LOCK TABLES `gastos_tienda` WRITE;
/*!40000 ALTER TABLE `gastos_tienda` DISABLE KEYS */;
INSERT INTO `gastos_tienda` VALUES (1,'22/02/2016','Eroski',10.22,'Efectivo','-'),(2,'23/02/2016','Eroski',18.78,'Efectivo','-'),(3,'23/02/2016','Vincen',16.85,'Efectivo','-'),(4,'24/02/2016','Okela',18.45,'Efectivo','-'),(5,'24/02/2016','Eroski',12.00,'Efectivo','-'),(6,'24/02/2016','Sustrai',6.59,'Efectivo','-'),(7,'25/02/2016','Eroski',21.29,'Efectivo','-'),(8,'26/02/2016','Eroski',12.92,'Efectivo','-'),(9,'27/02/2016','Okela',23.87,'Efectivo','-'),(10,'27/02/2016','Eroski',32.45,'Efectivo','-'),(11,'27/02/2016','Otro',30.31,'Tarjeta','Peio'),(12,'28/02/2016','OgiBerri',2.25,'Efectivo','-'),(13,'29/02/2016','Okela',14.68,'Efectivo','-'),(14,'29/02/2016','Eroski',13.33,'Efectivo','-'),(15,'29/02/2016','Otro',3.02,'Efectivo','-');
/*!40000 ALTER TABLE `gastos_tienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presupuestos`
--

DROP TABLE IF EXISTS `presupuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presupuestos` (
  `idPresu` int(11) NOT NULL,
  `fecha_presu` varchar(10) DEFAULT NULL,
  `cantidad` double(9,2) DEFAULT NULL,
  `tipo` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`idPresu`),
  UNIQUE KEY `idPresu_UNIQUE` (`idPresu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presupuestos`
--

LOCK TABLES `presupuestos` WRITE;
/*!40000 ALTER TABLE `presupuestos` DISABLE KEYS */;
INSERT INTO `presupuestos` VALUES (1,'22/02/2016',150.00,'Semanal'),(2,'01/02/2016',400.00,'Mensual');
/*!40000 ALTER TABLE `presupuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiendas`
--

DROP TABLE IF EXISTS `tiendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiendas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiendas`
--

LOCK TABLES `tiendas` WRITE;
/*!40000 ALTER TABLE `tiendas` DISABLE KEYS */;
INSERT INTO `tiendas` VALUES (1,'Eroski'),(2,'Okela'),(3,'Sustrai'),(4,'Ogiberri'),(5,'Vincen'),(6,'Dia'),(7,'Otro'),(8,'Garin');
/*!40000 ALTER TABLE `tiendas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-29 14:19:22
