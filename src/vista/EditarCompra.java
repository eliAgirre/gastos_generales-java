package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import com.toedter.calendar.JDateChooser;
import java.sql.*;
import java.text.*;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import modelo.*;
import utilidades.BD;
import utilidades.Utilidades;


@SuppressWarnings("serial")
public class EditarCompra extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtID;
	private Tipo_compra tipoCompra;
	private JComboBox<Tipo_compra> cbTipoCompra;
	private int codigoCompra;
	private int idTipoCompra;
	private String tipo;
	private Tipo_tienda tipoTienda;
	private JComboBox<Tipo_tienda> cbTipoTienda;
	private int codigoTienda;
	private int idTipoTienda;
	private String tipoTiendaS;
	private Tienda tienda;
	private JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private static JTextField txtProducto;
	private static JTextField txtCoste;
	private FormaPago formaPago;
	private JComboBox<FormaPago> cbFormaPago;
	private int codigoPago;
	private int idFormaPago;
	private String tipoPago;
	private Tipo_tarjeta tipoTarjeta;
	private JComboBox<Tipo_tarjeta> cbTarjeta;
	private int idTarjeta;
	private String nombreTarjeta;
	private int contTiendaCompra=0;
	private int contTipoTienda=0;
	private JDateChooser dateChooser;
	private JButton btnVolver;
	private JButton btnEditar;
	private JButton btnBorrar;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		private int id;
		private String resultFecha;
		private String resultTipoCompra;
		private String resultTipoTienda;
		private String resultTienda;
		private String resultProducto;
		private Double resultCoste;
		private String resultFormaPago;
		private String resultTarjeta;
	
	public EditarCompra(String idCompra, String fecha, String tipoCompraFrame, String tipoTiendaFrame, String tiendaFrame, String producto, String coste, String formaPagoFrame, String tarjetaFrame){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 638, 370); // tama�o
		setTitle("Editar compra"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			
			// Establece los textos de JTextFields + JComboBox + Radio buttons
			txtID.setText(idCompra);
			// formato de la fecha string
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			// convierte la fecha string en Date con el formato yyyy-MM-dd
			Date fechaDate=sdf.parse(fecha);
			// Muestra la fecha en dateChooser
			dateChooser.setDate(fechaDate);
			txtProducto.setText(producto);
			int pos=coste.indexOf(" �");
			txtCoste.setText(coste.substring(0,pos));
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_compra WHERE tipo='"+tipoCompraFrame+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoCompra=rs.getInt("idCompra");
				tipo=rs.getString("tipo");
				
				// Se instancia la clase Tipo_compra
				tipoCompra=new Tipo_compra(idTipoCompra,tipo);
				
				//Se a�ade cada objeto al JComboBox
				cbTipoCompra.addItem(tipoCompra);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_tienda WHERE tipo='"+tipoTiendaFrame+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoTienda=rs.getInt("idTipo");
				tipoTiendaS=rs.getString("tipo");
				
				// Se instancia la clase Tipo_tienda
				tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoTienda.addItem(tipoTienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			// Sentencia SELECT
			sql = "SELECT * FROM tiendas WHERE nombre='"+tiendaFrame+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos
				idTienda=rs.getInt("idTienda");
				nombreTienda=rs.getString("nombre");
				
				// Se instancia la clase Tipo_tienda
				tienda = new Tienda (idTienda,nombreTienda); 
				
				//Se a�ade cada objeto al JComboBox
				cbTienda.addItem(tienda);
				
			} // Cierre de while					
			rs.close(); //Cierre de la consulta
			
			// Sentencia SELECT
			sql = "SELECT * FROM forma_pago WHERE tipo_pago='"+formaPagoFrame+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos
				idFormaPago=rs.getInt("idFormaPago");
				tipoPago=rs.getString("tipo_pago");
				
				// Se instancia la clase 
				formaPago = new FormaPago (idFormaPago,tipoPago); 
				
				//Se a�ade cada objeto al JComboBox
				cbFormaPago.addItem(formaPago);
				
			} // Cierre de while					
			rs.close(); //Cierre de la consulta
			
			// Sentencia SELECT
			sql = "SELECT * FROM tipo_tarjetas WHERE nombre='"+tarjetaFrame+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos
				idTarjeta=rs.getInt("idTarjeta");
				nombreTarjeta=rs.getString("nombre");

				// Se instancia la clase Tipo_tienda
				tipoTarjeta = new Tipo_tarjeta (idTarjeta,nombreTarjeta); 
				
				//Se a�ade cada objeto al JComboBox
				cbTarjeta.addItem(tipoTarjeta);
				
			} // Cierre de while					
			rs.close(); //Cierre de la consulta
		}
		catch (Exception e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 316, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Modifique algun dato si es necesario.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblID = new JLabel("ID:");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblID.setForeground(new Color(0, 139, 139));
		lblID.setBounds(337, 36, 48, 14);
		contentPane.add(lblID);
		
		txtID = new JTextField();
		txtID.setEnabled(false);
		txtID.setColumns(10);
		txtID.setBounds(444, 33, 164, 20);
		contentPane.add(txtID);
		
		JLabel lblFecha = new JLabel("Fecha Compra:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(48, 81, 88, 14);
		contentPane.add(lblFecha);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(159, 78, 126, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		JLabel lblTipoCompra = new JLabel("Tipo compra:");
		lblTipoCompra.setForeground(new Color(0, 139, 139));
		lblTipoCompra.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoCompra.setBounds(48, 125, 88, 14);
		contentPane.add(lblTipoCompra);
		
		cbTipoCompra = new JComboBox<Tipo_compra>(); // Muestra la lista de la clase Tipo compra desde la BD
		cbTipoCompra.addActionListener((ActionListener)this);
		cbTipoCompra.setBounds(159, 125, 126, 20);
		contentPane.add(cbTipoCompra);
		
		JLabel lblTipoTienda = new JLabel("Tipo tienda:");
		lblTipoTienda.setForeground(new Color(0, 139, 139));
		lblTipoTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoTienda.setBounds(48, 172, 88, 14);
		contentPane.add(lblTipoTienda);
		
		cbTipoTienda = new JComboBox<Tipo_tienda>();
		cbTipoTienda.addActionListener((ActionListener)this);
		cbTipoTienda.setBounds(159, 169, 126, 20);
		contentPane.add(cbTipoTienda);
		
		JLabel lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(48, 215, 88, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tienda>();
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(159, 212, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblProducto = new JLabel("Producto:");
		lblProducto.setForeground(new Color(0, 139, 139));
		lblProducto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProducto.setBounds(337, 81, 95, 14);
		contentPane.add(lblProducto);
		
		txtProducto = new JTextField();
		txtProducto.addActionListener((ActionListener)this);
		txtProducto.setColumns(10);
		txtProducto.setBounds(444, 78, 164, 20);
		contentPane.add(txtProducto);
		
		JLabel lblCoste = new JLabel("Coste:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(337, 125, 95, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.addActionListener((ActionListener)this);
		txtCoste.setColumns(10);
		txtCoste.setBounds(444, 122, 164, 20);
		contentPane.add(txtCoste);
		
		JLabel lblFormaPago = new JLabel("Forma pago:");
		lblFormaPago.setForeground(new Color(0, 139, 139));
		lblFormaPago.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFormaPago.setBounds(337, 172, 95, 14);
		contentPane.add(lblFormaPago);
		
		cbFormaPago = new JComboBox<FormaPago>();
		cbFormaPago.addActionListener((ActionListener)this);
		cbFormaPago.setBounds(444, 169, 164, 20);
		contentPane.add(cbFormaPago);
		
		JLabel lblTarjeta = new JLabel("Tarjeta:");
		lblTarjeta.setForeground(new Color(0, 139, 139));
		lblTarjeta.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTarjeta.setBounds(337, 215, 95, 14);
		contentPane.add(lblTarjeta);
		
		cbTarjeta = new JComboBox<Tipo_tarjeta>();
		cbTarjeta.addActionListener((ActionListener)this);
		cbTarjeta.setBounds(444, 215, 164, 20);
		contentPane.add(cbTarjeta);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setBounds(126, 274, 108, 41);
		btnVolver.setBackground(new Color(184, 231, 255));
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/back.png")));
		contentPane.add(btnVolver);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setBounds(261, 274, 108, 41);
		btnEditar.setBackground(new Color(184, 231, 255));
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/edit2.png")));
		contentPane.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setBackground(new Color(184, 231, 255));
		btnBorrar.setBounds(402, 274, 111, 41);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/borrar.png")));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado en "Tipo Compra"
		if(evento.getSource()==cbTipoCompra){
			if(cbTipoCompra.isValid()==true){
				contTiendaCompra++;
				if(contTiendaCompra==1){
					// Limpia los datos de la lista
					cbTipoCompra.removeAllItems();
				}
				if(cbTipoCompra.getSelectedItem()==null){ // Si la seleccion item es igual a null
					try{
						// Sentencia SQL SELECT
						sql = "SELECT * FROM tipo_compra;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos, idCompra + tipo
							idTipoCompra=rs.getInt("idCompra");
							tipo=rs.getString("tipo");
							
							// Se instancia la clase Tipo_compra
							tipoCompra=new Tipo_compra(idTipoCompra,tipo);
							
							//Se a�ade cada objeto al JComboBox
							cbTipoCompra.addItem(tipoCompra);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
				}
				else{
					try{
						// Se guarda el item seleccionado
						resultTipoCompra=cbTipoCompra.getSelectedItem().toString();
						cbFormaPago.removeAllItems();
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_compra WHERE tipo='"+resultTipoCompra+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo de tipo de compra
							codigoCompra = rs.getInt("idCompra");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM forma_pago WHERE idTipoCompra="+codigoCompra+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idFormaPago=rs.getInt("idFormaPago");
							tipoPago=rs.getString("tipo_pago");
							
							// Se instancia la clase 
							formaPago = new FormaPago (idFormaPago,tipoPago); 
							
							//Se a�ade cada objeto al JComboBox
							cbFormaPago.addItem(formaPago);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						// Guarda el valor de item seleccionado "Forma de pago"
						resultFormaPago=cbFormaPago.getSelectedItem().toString();
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tarjetas WHERE idTipoTarjeta='"+resultFormaPago+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTarjeta=rs.getInt("idTarjeta");
							nombreTarjeta=rs.getString("nombre");
	
							// Se instancia la clase Tipo_tienda
							tipoTarjeta = new Tipo_tarjeta (idTarjeta,nombreTarjeta); 
							
							//Se a�ade cada objeto al JComboBox
							cbTarjeta.addItem(tipoTarjeta);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Guarda el valor de item seleccionado "Forma de pago"
						resultFormaPago=cbFormaPago.getSelectedItem().toString();
						// Limpia la lista de ComboBox
						cbTarjeta.removeAllItems();
						// Sentencia SELECT
						sql = "SELECT * FROM forma_pago WHERE tipo_pago='"+resultFormaPago+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo
							codigoPago = rs.getInt("idFormaPago");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tarjetas WHERE idTipoTarjeta="+codigoPago+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTarjeta=rs.getInt("idTarjeta");
							nombreTarjeta=rs.getString("nombre");

							// Se instancia la clase Tipo_tienda
							tipoTarjeta = new Tipo_tarjeta (idTarjeta,nombreTarjeta); 
							
							//Se a�ade cada objeto al JComboBox
							cbTarjeta.addItem(tipoTarjeta);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
				}
			}
		}
		// Si selecciona la "Forma de Pago"
		if(evento.getSource()==cbFormaPago){
			if(cbFormaPago.isValid()==true){
				if(cbTipoCompra.getSelectedItem()!=null){ // Si la seleccion item no es null
					try{
						// Guarda el valor de item seleccionado "Forma de pago"
						resultFormaPago=cbFormaPago.getSelectedItem().toString();
						// Limpia la lista de ComboBox
						cbTarjeta.removeAllItems();
						// Sentencia SELECT
						sql = "SELECT * FROM forma_pago WHERE tipo_pago='"+resultFormaPago+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo
							codigoPago = rs.getInt("idFormaPago");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tarjetas WHERE idTipoTarjeta="+codigoPago+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTarjeta=rs.getInt("idTarjeta");
							nombreTarjeta=rs.getString("nombre");
	
							// Se instancia la clase Tipo_tienda
							tipoTarjeta = new Tipo_tarjeta (idTarjeta,nombreTarjeta); 
							
							//Se a�ade cada objeto al JComboBox
							cbTarjeta.addItem(tipoTarjeta);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
				}
			}
		}
		// Si ha seleccionado en "Tipo Tienda"
		if(evento.getSource()==cbTipoTienda){
			if(cbTipoTienda.isValid()==true){
				contTipoTienda++;
				if(contTipoTienda==1){
					// Limpia los datos de la lista
					cbTipoTienda.removeAllItems();
				}
				if(cbTipoTienda.getSelectedItem()==null){ // Si la seleccion item es igual a null
					try{
						// Sentencia SQL SELECT
						sql = "SELECT * FROM tipo_tienda;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos, idCompra + tipo
							idTipoTienda=rs.getInt("idTipo");
							tipoTiendaS=rs.getString("tipo");
							
							// Se instancia la clase Tipo_tienda
							tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
							
							//Se a�ade cada objeto al JComboBox
							cbTipoTienda.addItem(tipoTienda);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
				}
				else{
					try{
						// Se guarda el item seleccionado
						resultTipoTienda=cbTipoTienda.getSelectedItem().toString();
						cbTienda.removeAllItems();
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tienda WHERE tipo='"+resultTipoTienda+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo de tipo de tienda
							codigoTienda = rs.getInt("idTipo");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM tiendas WHERE idTipo="+codigoTienda+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTienda=rs.getInt("idTienda");
							nombreTienda=rs.getString("nombre");
							
							// Se instancia la clase Tipo_tienda
							tienda = new Tienda (idTienda,nombreTienda); 
							
							//Se a�ade cada objeto al JComboBox
							cbTienda.addItem(tienda);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
				}
			}
		}
		// Si ha seleccionado "Tienda"
		if(evento.getSource()==cbTienda){
			if(cbTienda.isValid()==true){
				if(cbTienda.getSelectedItem()!=null){ // Si la seleccion item no es null
					// Se guarda el item seleccionado
					resultTienda=cbTienda.getSelectedItem().toString();
				}				
			}			
		}
		// Si ha seleccionado "Tarjeta"
		if(evento.getSource()==cbTarjeta){
			if(cbTarjeta.isValid()==true){
				if(cbTarjeta.getSelectedItem()!=null){ // Si la seleccion item no es null
					// Se guarda el item seleccionado
					resultTarjeta=cbTarjeta.getSelectedItem().toString();
				}				
			}			
		}
		// Si hace clic en Volver y vuelve a la ventana TablaGastos
		if(evento.getSource()==btnVolver){
			// Se instancia la ventana correspondiente
			TablaCompras ventanaTablaCompras=new TablaCompras();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaCompras.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaCompras.setVisible(true);
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnEditar){ 
			
			boolean validar=false;			
			try{
				validar=Utilidades.validarCamposCompra(dateChooser.getDate().toString(),cbTipoCompra.getSelectedItem().toString(),cbTipoTienda.getSelectedItem().toString(),cbTienda.getSelectedItem().toString(),txtProducto.getText(),txtCoste.getText(),cbFormaPago.getSelectedItem().toString(),cbTarjeta.getSelectedItem().toString(),getTxtProducto(),getTxtCoste());
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{
				// Obtener los datos de cada componente de la ventana
				id=Integer.parseInt(txtID.getText());
				resultTipoCompra=cbTipoCompra.getSelectedItem().toString();
				resultTipoTienda=cbTipoTienda.getSelectedItem().toString();
				resultTienda=cbTienda.getSelectedItem().toString();
				resultProducto=txtProducto.getText().trim();
				resultCoste=Double.parseDouble(txtCoste.getText());
				resultFormaPago=cbFormaPago.getSelectedItem().toString();
				resultTarjeta=cbTarjeta.getSelectedItem().toString();
				//Sentencia UPDATE
				sql = "UPDATE compras SET idCompra="+id+", fecha='"+resultFecha+"', tipo_compra='"+resultTipoCompra+"', tipo_tienda='"+resultTipoTienda+"', tienda='"+resultTienda+"', producto='"+resultProducto+"', coste="+resultCoste+", forma_pago= '"+resultFormaPago+"', tarjeta='"+resultTarjeta+"' WHERE idCompra='"+id+"'";
				// Ejecuta la actualizacion
				if(BD.actualizar(sql)==true){
					// Se guardan los datos en fichero log
					Utilidades.comprasLog("Editar",id,resultFecha,resultTipoTienda,resultTienda,Double.toString(resultCoste),resultFormaPago,resultProducto);
					// Muestra un aviso al usuario
					JOptionPane.showMessageDialog(null, "La compra se ha editado correctamente.");
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece esta ventana
			  		setVisible(false);
				}
			}
		}
		if(evento.getSource()==btnBorrar){ 
			
			// Obtener el id de la ventana
			id=Integer.parseInt(txtID.getText());
			// Sentencia DELETE
			sql="DELETE FROM compras WHERE idCompra=?;";
			if(BD.borrarRegistro(sql,id)==true){ // Si elimina el registro
				// Se guardan los datos en fichero log
				Utilidades.comprasLog("Borrar",id,"","","","","","");
				// Muestra aviso al usuario
				JOptionPane.showMessageDialog(null, "La compra ha sido borrado.");
				// Se instancia el controlador Principal
		  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
		  		// Desparece la ventana editar Gastos de Tienda
		  		setVisible(false);
			}			
		} // Cierre del btnBorrar
		
	} // Cierre del metodo actionPerformed
	
	// getters
	public static JTextField getTxtProducto() {
		return txtProducto;
	}

	public static JTextField getTxtCoste() {
		return txtCoste;
	}
		
} //Cierre clase