package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import utilidades.BD;
import utilidades.Utilidades;

import java.awt.Font;
import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class AnadirIngreso extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JDateChooser dateChooser;
	private JTextField txtCantidad;
	private int id;
	private JTextField txtConcepto;
	private JButton btnMenu;
	private JButton btnGuardar;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
	
		// Atributos del resultados user
		private String resultFecha;
		private String resultConcepto;
		private Double resultCantidad;
	
	public AnadirIngreso(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 313, 357); // tama�o
		setTitle("A�adir ingreso"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		componentes();
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 303, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setBounds(10, 26, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel label = new JLabel("Fecha:");
		label.setForeground(new Color(0, 139, 139));
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setBounds(31, 89, 46, 14);
		contentPane.add(label);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(104, 89, 126, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("dd/MM/yyyy").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		JLabel lblConcepto = new JLabel("Concepto:");
		lblConcepto.setForeground(new Color(0, 139, 139));
		lblConcepto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblConcepto.setBounds(31, 141, 95, 14);
		contentPane.add(lblConcepto);
		
		txtConcepto = new JTextField();
		txtConcepto.setText("");
		txtConcepto.setColumns(10);
		txtConcepto.setBounds(104, 138, 126, 20);
		contentPane.add(txtConcepto);
		
		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setForeground(new Color(0, 139, 139));
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantidad.setBounds(31, 188, 95, 14);
		contentPane.add(lblCantidad);
		
		txtCantidad = new JTextField();
		txtCantidad.setText("");
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(104, 185, 126, 20);
		contentPane.add(txtCantidad);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(31, 241, 112, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);	
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(165, 241, 112, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Va a la ventana principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			// Desparece la ventana a�adir Gastos de Tienda
			setVisible(false);
		}
		// Si hace clic en Guardar se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnGuardar){ 
			try{
				boolean validar=false;			
				try{
					validar=Utilidades.validarCamposIngreso(dateChooser.getDate().toString(), txtConcepto.getText(), txtCantidad.getText(), getDateChooser(), getTxtConcepto(), getTxtCantidad());
				}catch(NullPointerException npe){ // Muestra un error
					JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
				}
				
				if(validar==false){}
				else{
					// Conexion BD
					BD.conectar();
					// Establece conexion devolviendo un resultado
					stmt=BD.conexion();	
					// Sentencia SQL SELECT
					sql = "SELECT * FROM ingresos;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Se a�aden los datos de cada componente al arraylist
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana
						resultConcepto=txtConcepto.getText();
						if(Utilidades.esNumero(txtCantidad.getText())==false){
							JOptionPane.showMessageDialog(null, "La 'Cantidad' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtCantidad.getText())==true){
							resultCantidad=Double.parseDouble(txtCantidad.getText());
							// Sentencia INSERT
							sql="INSERT INTO ingresos VALUES ("+id+",'"+resultFecha+"','"+resultConcepto+"',"+resultCantidad+");";
							//Ejecuta la actualizacion
							if(BD.actualizar(sql)==true){
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "El ingreso se ha guardado correctamente.");
								// Se instancia el controlador Principal
						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
						  		// Desparece esta ventana
						  		setVisible(false);
							}
						}
					}
					else{
						// Sentencia SQL SELECT
						sql = "SELECT MAX(idIngreso) FROM ingresos;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Si hay datos
						if (rs.next()){
							id=rs.getInt(1);
				        }
						id++;
						// Obtener los datos de cada componente de la ventana
						resultConcepto=txtConcepto.getText();
						if(Utilidades.esNumero(txtCantidad.getText())==false){
							JOptionPane.showMessageDialog(null, "La 'Cantidad' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtCantidad.getText())==true){
							resultCantidad=Double.parseDouble(txtCantidad.getText());
							// Sentencia INSERT
							sql="INSERT INTO ingresos VALUES ("+id+",'"+resultFecha+"','"+resultConcepto+"',"+resultCantidad+");";
							//Ejecuta la actualizacion
							if(BD.actualizar(sql)==true){
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "El ingreso se ha guardado correctamente.");
								// Se instancia el controlador Principal
						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
						  		// Desparece esta ventana
						  		setVisible(false);
							}
						}						
					} // Cierre else
				}				
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}// Cierre evento btnMenu
	} // Cierre del metodo actionPerformed
	
	// Getters
	public JDateChooser getDateChooser() {
		return dateChooser;
	}
	public JTextField getTxtCantidad() {
		return txtCantidad;
	}
	public JTextField getTxtConcepto() {
		return txtConcepto;
	}
} // Cierre clase