package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import modelo.ComboFijos;
//import modelo.Fijos;
//import gestor.GastosFijos;
import utilidades.BD;
import utilidades.Utilidades;

import java.awt.Font;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class AnadirGastoFijo extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private static JTextField txtImporte;
	private static JTextField txtNumFactura;
	private ComboFijos gastosFijos;
	private JComboBox<ComboFijos> cbTipoGasto;
	private int idGastoFijo;
	private JDateChooser dateFract;
	private JDateChooser dateInicio;
	private JDateChooser dateFin;
	private String nombreFijo;
	private static JTextField txtDuracion;
	private String duracion;
	private int id;
	private JButton btnMenu;
	private JButton btnGuardar;
	//private JButton btnOtroMas;
	//private int cont=0;
	//private Fijos fijos; // Objeto para el arraylist
	//private GastosFijos gestorGastosFijos=new GastosFijos(); //arraylist
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
		
		// Atributos del resultados user
		private String resultFechaFact;
		private String resultNumFact;
		private String resultTipoGasto;
		private String resultFechaInicio;
		private String resultFechaFin;
		private String resultDuracion;
		private Double resultImporte;
		
	
	public AnadirGastoFijo(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 605, 351); // tama�o
		setTitle("A�adir gasto fijo"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();		
			// Sentencia SQL SELECT
			sql = "SELECT * FROM fijos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idGastoFijo=rs.getInt("idFijos");
				nombreFijo=rs.getString("nombre");
				
				// Se instancia la clase Tipo_compra
				gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoGasto.addItem(gastosFijos);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 301, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setBounds(36, 31, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFechaFract = new JLabel("Fecha doc:");
		lblFechaFract.setForeground(new Color(0, 139, 139));
		lblFechaFract.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaFract.setBounds(36, 67, 105, 14);
		contentPane.add(lblFechaFract);
		
		dateFract = new JDateChooser();
		dateFract.setDateFormatString("dd/MM/yyyy");
		dateFract.setBounds(137, 67, 126, 20);
		dateFract.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaFact = new SimpleDateFormat("dd/MM/yyyy").format(dateFract.getDate());
	                }
				}
		    });
		contentPane.add(dateFract);
		
		JLabel lblNumFactura = new JLabel("N� Documento:");
		lblNumFactura.setForeground(new Color(0, 139, 139));
		lblNumFactura.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNumFactura.setBounds(320, 64, 105, 14);
		contentPane.add(lblNumFactura);
		
		txtNumFactura = new JTextField();
		txtNumFactura.setText("");
		txtNumFactura.setColumns(10);
		txtNumFactura.setBounds(421, 61, 126, 20);
		contentPane.add(txtNumFactura);
		
		JLabel lblTipoGasto = new JLabel("Tipo gasto:");
		lblTipoGasto.setForeground(new Color(0, 139, 139));
		lblTipoGasto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoGasto.setBounds(36, 164, 75, 14);
		contentPane.add(lblTipoGasto);
		
		ComboFijos fijo=new ComboFijos("");		
		cbTipoGasto = new JComboBox<ComboFijos>();
		cbTipoGasto.addItem(fijo);
		cbTipoGasto.addActionListener((ActionListener)this);
		cbTipoGasto.setBounds(137, 161, 126, 20);
		contentPane.add(cbTipoGasto);
		
		JLabel lblFechaInicio = new JLabel("Fecha inicio:");
		lblFechaInicio.setForeground(new Color(0, 139, 139));
		lblFechaInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaInicio.setBounds(36, 113, 105, 14);
		contentPane.add(lblFechaInicio);
		
		dateInicio = new JDateChooser();
		dateInicio.setDateFormatString("dd/MM/yyyy");
		dateInicio.setBounds(137, 113, 126, 20);
		dateInicio.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evento) {
						if (evento.getPropertyName().equals("date")) {
							// Se le da formato a la fecha obtenida
							resultFechaInicio = new SimpleDateFormat("dd/MM/yyyy").format(dateInicio.getDate());
		                }
					}
			    });
		contentPane.add(dateInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setForeground(new Color(0, 139, 139));
		lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaFin.setBounds(320, 119, 105, 14);
		contentPane.add(lblFechaFin);
		
		dateFin = new JDateChooser();
		dateFin.setDateFormatString("dd/MM/yyyy");
		dateFin.setBounds(421, 119, 126, 20);
		dateFin.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evento) {
						if (evento.getPropertyName().equals("date")) {
							// Se le da formato a la fecha obtenida
							resultFechaFin = new SimpleDateFormat("dd/MM/yyyy").format(dateFin.getDate());
		                }
					}
			    });
		contentPane.add(dateFin);
		
		JLabel lblDuracion = new JLabel("Duracion:");
		lblDuracion.setForeground(new Color(0, 139, 139));
		lblDuracion.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDuracion.setBounds(320, 167, 75, 14);
		contentPane.add(lblDuracion);
		
		txtDuracion = new JTextField();
		txtDuracion.setEditable(false);
		txtDuracion.setText("");
		txtDuracion.setColumns(10);
		txtDuracion.setBounds(421, 164, 126, 20);
		contentPane.add(txtDuracion);
		
		JLabel lblImporte = new JLabel("Importe:");
		lblImporte.setForeground(new Color(0, 139, 139));
		lblImporte.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblImporte.setBounds(320, 210, 75, 14);
		contentPane.add(lblImporte);
		
		txtImporte = new JTextField();
		txtImporte.setText("");
		txtImporte.setColumns(10);
		txtImporte.setBounds(421, 207, 126, 20);
		contentPane.add(txtImporte);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(168, 254, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(320, 254, 115, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
		/*btnOtroMas = new JButton("Otro");
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setBackground(new Color(184, 231, 255));
		btnOtroMas.setBounds(414, 254, 115, 41);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		contentPane.add(btnOtroMas);*/
		
	} // Cierre componentes
	
	// Limpia los datos de los componentes
	/*private void limpiar(){
		try{
			dateFract.setDate(null);
		}catch(NullPointerException npe){
			
		}
		cbTipoGasto.setSelectedItem(null);
		try{
			dateInicio.setDate(null);
		}catch(NullPointerException npe){
			
		}
		try{
			dateFin.setDate(null);
		}catch(NullPointerException npe){
			
		}
		txtNumFactura.setText("");
		txtDuracion.setText("");
		txtImporte.setText("");
	}*/ // Cierre limpiar
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Tipo gasto"
		if(evento.getSource()==cbTipoGasto){
			if(cbTipoGasto.isValid()==true){ // Se valida JComboBox
				if(cbTipoGasto.getSelectedItem()!=null){ // Si lo seleccionado no es nulo
					try{
						// Se guarda lo seleccionado en una variable
						resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
						//Sentencia SELECT para obtener la duracion del gasto fijo
						sql="SELECT * FROM fijos WHERE nombre='"+resultTipoGasto+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							// Se obtiene el dato
							if(rs.getInt(3)<=1){
								duracion=Integer.toString(rs.getInt(3))+" mes";
							}
							else{
								duracion=Integer.toString(rs.getInt(3))+" meses";
							}
							
							txtDuracion.setText(duracion);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}
			}
		}
		// Si hace clic en Otro, se van a�adiendo registros al array
		/*if(evento.getSource()==btnOtroMas){
			boolean validar=false;			
			try{
				validar=validarCampos(dateFract.getDateFormatString(),txtNumFactura.getText(),dateInicio.getDateFormatString(),dateFin.getDateFormatString(),cbTipoGasto.getSelectedItem().toString(),txtDuracion.getText(),txtImporte.getText());
				
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{
				try {	
			
					// Sentencia SQL SELECT
					sql = "SELECT * FROM fijos;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Se a�aden los datos de cada componente al arraylist
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana
						resultNumFact=txtNumFactura.getText();
						resultDuracion=txtDuracion.getText();
						if(Utilidades.esNumero(txtImporte.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtImporte.getText())==true){
							resultImporte=Double.parseDouble(txtImporte.getText());	
							// Se guardan los datos en fichero log
							Utilidades.fijosLog("A�adir",id,resultFechaFact,resultTipoGasto,txtImporte.getText(),resultFechaInicio,resultFechaFin,resultNumFact);
							//Se crea un nuevo objeto
							fijos=new Fijos(id,resultFechaFact,resultNumFact,resultTipoGasto,resultFechaInicio,resultFechaFin,resultDuracion,resultImporte);					
							// Se a�ade cada objeto al arraylist
							gestorGastosFijos.anadir(fijos);
							// Se limpian los componentes
							limpiar();
						}
					}
					else{
						cont++;
						// Cuando cont sea igual a 1 se obtiene la ID de la BD
						if(cont==1){
							// Se obtiene el ultimo ID desde la BD
							id=gestorGastosFijos.ultimoID();
						}					
						id++;
						// Obtener los datos de cada componente de la ventana
						resultNumFact=txtNumFactura.getText();
						resultDuracion=txtDuracion.getText();
						if(Utilidades.esNumero(txtImporte.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtImporte.getText())==true){
							resultImporte=Double.parseDouble(txtImporte.getText());	
							// Se guardan los datos en fichero log
							Utilidades.fijosLog("A�adir",id,resultFechaFact,resultTipoGasto,txtImporte.getText(),resultFechaInicio,resultFechaFin,resultNumFact);
							//Se crea un nuevo objeto
							fijos=new Fijos(id,resultFechaFact,resultNumFact,resultTipoGasto,resultFechaInicio,resultFechaFin,resultDuracion,resultImporte);					
							// Se a�ade cada objeto al arraylist
							gestorGastosFijos.anadir(fijos);
							// Se limpian los componentes
							limpiar();
						}
					}
				}catch (SQLException e) {
					// Muestra un error
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
				}
			}
		}*/
		// Va a la ventana principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			// Desparece la ventana a�adir Gastos de Tienda
			setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnGuardar){
			try {					
				boolean validar=false;				
				try{
					validar=Utilidades.validarCamposGastosFijos(dateFract.getDate().toString(),txtNumFactura.getText(),dateInicio.getDate().toString(),dateFin.getDate().toString(),cbTipoGasto.getSelectedItem().toString(),txtDuracion.getText(),txtImporte.getText(),getTxtImporte(),getTxtNumFactura(),getTxtDuracion());
					
				}catch(NullPointerException npe){ // Muestra un error
					JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
				}
				
				if(validar==false){}
				else{
					// Sentencia SQL SELECT
					sql = "SELECT * FROM fijos;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Se a�aden los datos de cada componente al arraylist
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana
						resultNumFact=txtNumFactura.getText();
						resultDuracion=txtDuracion.getText();
						if(Utilidades.esNumero(txtImporte.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtImporte.getText())==true){
							resultImporte=Double.parseDouble(txtImporte.getText());
							// Sentencia INSERT
							sql="INSERT INTO gastos_fijos VALUES ("+id+",'"+resultFechaFact+"','"+resultNumFact+"','"+resultTipoGasto+"','"+resultFechaInicio+"','"+resultFechaFin+"','"+resultDuracion+"',"+resultImporte+");";
							//Ejecuta la actualizacion
							if(BD.actualizar(sql)==true){
								// Se guardan los datos en fichero log
								Utilidades.fijosLog("A�adir",id,resultFechaFact,resultTipoGasto,txtImporte.getText(),resultFechaInicio,resultFechaFin,resultNumFact);	
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
								// Se instancia el controlador Principal
								ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
								// Desparece la ventana a�adir Gastos de Tienda
								setVisible(false);
							}
						}
					}
					else{
						// Sentencia SQL SELECT
						sql = "SELECT MAX(idGasto) FROM gastos_fijos;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Si hay datos
						if (rs.next()){
							id=rs.getInt(1);
				        }
						id++;
						// Obtener los datos de cada componente de la ventana
						resultNumFact=txtNumFactura.getText();
						resultDuracion=txtDuracion.getText();
						if(Utilidades.esNumero(txtImporte.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtImporte.getText())==true){
							resultImporte=Double.parseDouble(txtImporte.getText());	
							// Sentencia INSERT
							sql="INSERT INTO gastos_fijos VALUES ("+id+",'"+resultFechaFact+"','"+resultNumFact+"','"+resultTipoGasto+"','"+resultFechaInicio+"','"+resultFechaFin+"','"+resultDuracion+"',"+resultImporte+");";
							//Ejecuta la actualizacion
							if(BD.actualizar(sql)==true){
								// Se guardan los datos en fichero log
								Utilidades.fijosLog("A�adir",id,resultFechaFact,resultTipoGasto,txtImporte.getText(),resultFechaInicio,resultFechaFin,resultNumFact);
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
								// Se instancia el controlador Principal
								ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
								// Desparece la ventana a�adir Gastos de Tienda
								setVisible(false);
							}									
						}
					}
				} // Cierre del else
			}
			catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}
		}// Cierre evento btnMenu		
	} // Cierre actionPerformed

	// getters
	public static JTextField getTxtImporte() {
		return txtImporte;
	}

	public static JTextField getTxtNumFactura() {
		return txtNumFactura;
	}

	public static JTextField getTxtDuracion() {
		return txtDuracion;
	}
} // Cierre clase