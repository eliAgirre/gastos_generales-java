package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import modelo.ComboFijos;
import utilidades.BD;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class TablaGastosFijos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private ComboFijos gastosFijos;
	private JComboBox<ComboFijos> cbTipoGasto;
	private int idGastoFijo;
	private String nombreFijo;
	private String id;
	private String fechaDoc;
	private String doc;
	private String gastoFijo;
	private String fechaInicio;
	private String fechaFin;
	private String duracion;
	private String importe;
	private JButton btnBuscar;
	private JButton btnMenu;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"ID","Fecha doc","N� Documento","Gasto fijo","Fecha inicio","Fecha fin","Duracion","Importe"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[8];
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		private String resultTipoGasto;
	
	public TablaGastosFijos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 777, 621); // tama�o
		setTitle("Gastos a editar"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();		
			// Sentencia SQL SELECT
			sql = "SELECT * FROM gastos_fijos ORDER BY fecha_factura ASC, idGasto ASC;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan en el array los datos de BD
				datosBD[0]=rs.getString(1); //ID
				datosBD[1]=rs.getString(2); //fecha factura
				datosBD[2]=rs.getString(3); //numero factura
				datosBD[3]=rs.getString(4); //tipo_gasto
				datosBD[4]=rs.getString(5); //fecha_inicio
				datosBD[5]=rs.getString(6); //fecha_fin
				datosBD[6]=rs.getString(7); //duracion
				datosBD[7]=Double.toString(rs.getDouble(8))+" �"; //coste
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
			
			} // Cierre de while
				
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM fijos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idGastoFijo=rs.getInt("idFijos");
				nombreFijo=rs.getString("nombre");
				
				// Se instancia la clase Tipo_compra
				gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoGasto.addItem(gastosFijos);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 153, 732, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se establece una anchura de las columnas
		tabla.getColumn("ID").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(false);
		// Se a�ade un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
			public void mouseClicked(MouseEvent evento) {
		    	
		    	String nl = System.getProperty("line.separator"); // Salto de linea
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
		        id=(String)modelo.getValueAt(fila,columna);
		        fechaDoc=(String) modelo.getValueAt(fila,1);
		        doc=(String) modelo.getValueAt(fila,2);
		        gastoFijo=(String) modelo.getValueAt(fila,3);
		        fechaInicio=(String) modelo.getValueAt(fila,4);
		        fechaFin=(String) modelo.getValueAt(fila,5);
		        duracion=(String) modelo.getValueAt(fila,6);
		        importe=(String) modelo.getValueAt(fila,7);
		        JOptionPane.showMessageDialog(null, "Compra a editar: "+nl+nl+"   Fecha doc: "+fechaDoc+nl+"   Gasto: "+gastoFijo+nl+"   Duracion: "+duracion+nl+"   Importe: "+importe+nl+" ");
		        EditarGastosFijos editarGastosFijos = new EditarGastosFijos(id,fechaDoc,doc,gastoFijo,fechaInicio,fechaFin,duracion,importe);
				// Coloca la ventana en el centro de la pantalla
		        editarGastosFijos.setLocationRelativeTo(null);
				// Hace visible la ventana
		        editarGastosFijos.setVisible(true);
		        // Desaparece esta ventana
				setVisible(false);
				 
		    } // Cierre del mouseClicked
		});	
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 567, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Seleccione un gasto a editar, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(24, 36, 254, 14);
		contentPane.add(lblAviso);
		
		cbTipoGasto = new JComboBox<ComboFijos>();
		cbTipoGasto.addActionListener((ActionListener)this);
		cbTipoGasto.setBounds(305, 78, 141, 20);
		contentPane.add(cbTipoGasto);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener((ActionListener)this);
		btnBuscar.setBackground(new Color(184, 231, 255));
		btnBuscar.setBounds(496, 68, 110, 41);
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/search.png")));
		contentPane.add(btnBuscar);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(334, 540, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en boton "Buscar"
		if(evento.getSource()==btnBuscar){
			Utilidades.limpiarTabla(modelo);
			if(cbTipoGasto.isValid()==true){ // Se valida JComboBox
				if(cbTipoGasto.getSelectedItem()!=null){ // Si lo seleccionado no es nulo
					try{
						// Se guarda lo seleccionado en una variable
						resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
						//Sentencia SELECT para obtener la duracion del gasto fijo
						sql="SELECT * FROM gastos_fijos WHERE tipo_gasto='"+resultTipoGasto+"' ORDER BY fecha_factura ASC, idGasto ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							// Se guardan en el array los datos de BD
							datosBD[0]=rs.getString(1); //ID
							datosBD[1]=rs.getString(2); //fecha factura
							datosBD[2]=rs.getString(3); //numero factura
							datosBD[3]=rs.getString(4); //tipo_gasto
							datosBD[4]=rs.getString(5); //fecha_inicio
							datosBD[5]=rs.getString(6); //fecha_fin
							datosBD[6]=rs.getString(7); //duracion
							datosBD[7]=Double.toString(rs.getDouble(8))+" �"; //coste
							// Se a�ade cada fila al modelo de tabla
							modelo.addRow(datosBD);
						
						} // Cierre de while
							
						rs.close(); //Cierre de la consulta
						cbTipoGasto.setSelectedIndex(0);
						resultTipoGasto="";
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}
			}
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase