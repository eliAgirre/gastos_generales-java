package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.BD;
import utilidades.Utilidades;

import java.awt.Font;

@SuppressWarnings("serial")
public class BuscarProducto extends JFrame implements ActionListener,KeyListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtProducto;
	private JButton btnBuscar;
	private JButton btnMenu;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"ID","Fecha","Compra","Tipo","Tienda","Producto","Coste","Pago", "Tarjeta"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[9];
		
		// Atributos del resultados user
		private String resultProducto;
	
	public BuscarProducto(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 673, 621); // tama�o
		setTitle("Buscar producto"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/search.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);	
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 144, 628, 369);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se establece una anchura de las columnas
		tabla.getColumn("ID").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Fecha").setPreferredWidth(40); 
		//tabla.getColumn("Compra").setPreferredWidth(30);
		tabla.getColumn("Compra").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setPreferredWidth(0); // Se oculta la columna
		//tabla.getColumn("Tipo").setPreferredWidth(30);
		tabla.getColumn("Tipo").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Tienda").setPreferredWidth(40); 
		tabla.getColumn("Producto").setPreferredWidth(200);
		tabla.getColumn("Coste").setPreferredWidth(20);
		tabla.getColumn("Pago").setPreferredWidth(30);
		//tabla.getColumn("Tarjeta").setPreferredWidth(20);
		tabla.getColumn("Tarjeta").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(false);
		tabla.setBorder(null);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 567, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca el nombre del producto:");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(24, 36, 254, 14);
		contentPane.add(lblAviso);
		
		txtProducto = new JTextField();
		txtProducto.addKeyListener((KeyListener)this);
		txtProducto.setColumns(10);
		txtProducto.setBounds(41, 78, 312, 20);
		contentPane.add(txtProducto);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener((ActionListener)this);
		btnBuscar.setBackground(new Color(184, 231, 255));
		btnBuscar.setBounds(380, 69, 112, 41);
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/search.png")));
		contentPane.add(btnBuscar);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		btnMenu.setBounds(287, 540, 95, 41);
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@Override
	public void keyPressed(KeyEvent evento) {
		// Cuando presiona la tecla "Enter"
		if(evento.getKeyCode()==KeyEvent.VK_ENTER){
			// Se guarda el texto del producto
			resultProducto=txtProducto.getText();
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			// Se hace la conexion con la BD
			try {				
				// Conexion BD
				BD.conectar();
				// Establece conexion devolviendo un resultado
				stmt=BD.conexion();
				// Sentencia SQL SELECT
				sql = "SELECT * FROM compras WHERE producto LIKE '%"+resultProducto+"%';";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan en el array los datos de BD
					datosBD[0]=Integer.toString(rs.getInt(1)); //id
					datosBD[1]=rs.getString(2); //fecha
					datosBD[2]=rs.getString(3); //tipo_compra
					datosBD[3]=rs.getString(4); //tipo_tienda
					datosBD[4]=rs.getString(5); //tienda
					datosBD[5]=rs.getString(6); //producto
					datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
					datosBD[7]=rs.getString(8); //forma_pago
					datosBD[8]=rs.getString(9); //tarjeta
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
				
				} // Cierre de while
					
				rs.close(); //Cierre de la consulta
				
				// Limpian los componentes
				txtProducto.setText("");
				resultProducto="";
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
	}

	@Override
	public void keyReleased(KeyEvent evento) {}

	@Override
	public void keyTyped(KeyEvent evento) {}
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Buscar"
		if(evento.getSource()==btnBuscar){
			// Se guarda el texto del producto
			resultProducto=txtProducto.getText();
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			// Se hace la conexion con la BD
			try {				
				// Conexion BD
				BD.conectar();
				// Establece conexion devolviendo un resultado
				stmt=BD.conexion();
				// Sentencia SQL SELECT
				sql = "SELECT * FROM compras WHERE producto LIKE '%"+resultProducto+"%';";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan en el array los datos de BD
					datosBD[0]=Integer.toString(rs.getInt(1)); //id
					datosBD[1]=rs.getString(2); //fecha
					datosBD[2]=rs.getString(3); //tipo_compra
					datosBD[3]=rs.getString(4); //tipo_tienda
					datosBD[4]=rs.getString(5); //tienda
					datosBD[5]=rs.getString(6); //producto
					datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
					datosBD[7]=rs.getString(8); //forma_pago
					datosBD[8]=rs.getString(9); //tarjeta
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
				
				} // Cierre de while
					
				rs.close(); //Cierre de la consulta
				
				// Limpian los componentes
				txtProducto.setText("");
				resultProducto="";
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase