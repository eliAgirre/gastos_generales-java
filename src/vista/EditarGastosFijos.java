package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import controlador.ControladorPrincipal;
import modelo.ComboFijos;
import utilidades.BD;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class EditarGastosFijos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtID;
	private JDateChooser dateDoc;
	private static JTextField txtNumFactura;
	private ComboFijos gastosFijos;
	private JComboBox<ComboFijos> cbTipoGasto;
	private int idGastoFijo;
	private String nombreFijo;
	private JDateChooser dateInicio;
	private JDateChooser dateFin;
	private static JTextField txtDuracion;
	private static JTextField txtImporte;
	private int contTipoGasto=0;
	private JButton btnVolver;
	private JButton btnEditar;
	private JButton btnBorrar;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
	
		// Atributos del resultados user	
		private int id;
		private String resultFechaDoc;
		private String resultNumDoc;
		private String resultTipoGasto;
		private String resultFechaInicio;
		private String resultFechaFin;
		private String resultDuracion;
		private double resultImporte=0.0;
		
	
	public EditarGastosFijos(String idGasto, String fechaDoc, String documento, String gastoFijo, String fechaInicio, String fechaFin, String duracion, String importe){
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 638, 370); // tama�o
		setTitle("Editar gasto fijo"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			
			// Establece los textos de JTextFields + JComboBox + DateChooser
			txtID.setText(idGasto);
			txtNumFactura.setText(documento);
			txtDuracion.setText(duracion);
			int pos=importe.indexOf(" �");
			txtImporte.setText(importe.substring(0,pos));
			// formato de la fecha string
			SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy");
			// convierte la fecha string en Date con el formato anterior
			Date dateDocument = (Date) formatoDate.parse(fechaDoc);
			Date dateInit = (Date) formatoDate.parse(fechaInicio);
			Date dateFinish = (Date) formatoDate.parse(fechaFin);
			// Muestra la fecha en dateChooser
			dateDoc.setDate(dateDocument);
			dateInicio.setDate(dateInit);
			dateFin.setDate(dateFinish);
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();		
			// Sentencia SQL SELECT
			sql = "SELECT * FROM fijos WHERE nombre='"+gastoFijo+"';";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idGastoFijo=rs.getInt("idFijos");
				nombreFijo=rs.getString("nombre");
				
				// Se instancia la clase Tipo_compra
				gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoGasto.addItem(gastosFijos);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (Exception e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
				
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 316, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Modifique algun dato si es necesario.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblID = new JLabel("ID:");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblID.setForeground(new Color(0, 139, 139));
		lblID.setBounds(320, 36, 48, 14);
		contentPane.add(lblID);
		
		txtID = new JTextField();
		txtID.setEnabled(false);
		txtID.setColumns(10);
		txtID.setBounds(421, 33, 126, 20);
		contentPane.add(txtID);
		
		JLabel lblFecha = new JLabel("Fecha doc:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(48, 81, 95, 14);
		contentPane.add(lblFecha);
		
		dateDoc = new JDateChooser();
		dateDoc.setDateFormatString("dd/MM/yyyy");
		dateDoc.setBounds(159, 78, 126, 20);
		dateDoc.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaDoc = new SimpleDateFormat("yyyy-MM-dd").format(dateDoc.getDate());
	                }
				}
		    });
		contentPane.add(dateDoc);
		
		JLabel lblDocumento = new JLabel("N� Documento:");
		lblDocumento.setForeground(new Color(0, 139, 139));
		lblDocumento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDocumento.setBounds(48, 122, 105, 14);
		contentPane.add(lblDocumento);
		
		txtNumFactura = new JTextField();
		txtNumFactura.setText("");
		txtNumFactura.setColumns(10);
		txtNumFactura.setBounds(159, 119, 126, 20);
		contentPane.add(txtNumFactura);
		
		JLabel lblTipoGasto = new JLabel("Tipo gasto:");
		lblTipoGasto.setForeground(new Color(0, 139, 139));
		lblTipoGasto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoGasto.setBounds(43, 164, 75, 14);
		contentPane.add(lblTipoGasto);
		
		cbTipoGasto = new JComboBox<ComboFijos>();
		cbTipoGasto.addActionListener((ActionListener)this);
		cbTipoGasto.setBounds(159, 161, 126, 20);
		contentPane.add(cbTipoGasto);
		
		JLabel lblFechaInicio = new JLabel("Fecha inicio:");
		lblFechaInicio.setForeground(new Color(0, 139, 139));
		lblFechaInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaInicio.setBounds(320, 81, 105, 14);
		contentPane.add(lblFechaInicio);
		
		dateInicio = new JDateChooser();
		dateInicio.setDateFormatString("dd/MM/yyyy");
		dateInicio.setBounds(421, 75, 126, 20);
		dateInicio.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(dateInicio.getDate());
	                }
				}
		    });
		contentPane.add(dateInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setForeground(new Color(0, 139, 139));
		lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaFin.setBounds(320, 122, 105, 14);
		contentPane.add(lblFechaFin);
		
		dateFin = new JDateChooser();
		dateFin.setDateFormatString("dd/MM/yyyy");
		dateFin.setBounds(421, 119, 126, 20);
		dateFin.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaFin = new SimpleDateFormat("yyyy-MM-dd").format(dateFin.getDate());
	                }
				}
		    });
		contentPane.add(dateFin);
		
		JLabel lblDuracion = new JLabel("Duracion:");
		lblDuracion.setForeground(new Color(0, 139, 139));
		lblDuracion.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDuracion.setBounds(320, 167, 75, 14);
		contentPane.add(lblDuracion);
		
		txtDuracion = new JTextField();
		txtDuracion.setEditable(false);
		txtDuracion.setText("");
		txtDuracion.setColumns(10);
		txtDuracion.setBounds(421, 164, 126, 20);
		contentPane.add(txtDuracion);
		
		JLabel lblImporte = new JLabel("Importe:");
		lblImporte.setForeground(new Color(0, 139, 139));
		lblImporte.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblImporte.setBounds(320, 210, 75, 14);
		contentPane.add(lblImporte);
		
		txtImporte = new JTextField();
		txtImporte.setText("");
		txtImporte.setColumns(10);
		txtImporte.setBounds(421, 207, 126, 20);
		contentPane.add(txtImporte);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setBounds(126, 274, 108, 41);
		btnVolver.setBackground(new Color(184, 231, 255));
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/back.png")));
		contentPane.add(btnVolver);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setBounds(261, 274, 108, 41);
		btnEditar.setBackground(new Color(184, 231, 255));
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/edit2.png")));
		contentPane.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setBackground(new Color(184, 231, 255));
		btnBorrar.setBounds(402, 274, 111, 41);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/borrar.png")));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Tipo gasto"
		if(evento.getSource()==cbTipoGasto){
			if(cbTipoGasto.isValid()==true){ // Se valida JComboBox
				contTipoGasto++;
				if(contTipoGasto==1){
					// Limpia los datos de la lista
					cbTipoGasto.removeAllItems();
				}
				if(cbTipoGasto.getSelectedItem()==null){ // Si la seleccion item es igual a null
					try{
						// Sentencia SQL SELECT
						sql = "SELECT * FROM fijos;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos, idCompra + tipo
							idGastoFijo=rs.getInt("idFijos");
							nombreFijo=rs.getString("nombre");
							
							// Se instancia la clase Tipo_compra
							gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
							
							//Se a�ade cada objeto al JComboBox
							cbTipoGasto.addItem(gastosFijos);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (Exception e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());				
					} // Cierre excepcion
					
				}
				else{
					// Se guarda el item seleccionado
					resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
				}
			}
		}
		// Si hace clic en Volver y vuelve a la ventana de tabla
		if(evento.getSource()==btnVolver){
			// Se instancia la ventana correspondiente
			TablaGastosFijos ventanaTablaIngresos=new TablaGastosFijos();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaIngresos.setVisible(true);
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnEditar){ 
			
			boolean validar=false;				
			try{
				validar=Utilidades.validarCamposGastosFijos(dateDoc.getDate().toString(),txtNumFactura.getText(),dateInicio.getDateFormatString(),dateFin.getDateFormatString(),cbTipoGasto.getSelectedItem().toString(),txtDuracion.getText(),txtImporte.getText(),getTxtImporte(),getTxtNumFactura(),getTxtDuracion());
				
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{
				// Obtener los datos de cada componente de la ventana
				id=Integer.parseInt(txtID.getText());
				resultNumDoc=txtNumFactura.getText();
				resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
				resultDuracion=txtDuracion.getText();
				if(Utilidades.esNumero(txtImporte.getText())==false){
					JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
				}
				else if(Utilidades.esNumero(txtImporte.getText())==true){
					resultImporte=Double.parseDouble(txtImporte.getText());
					// Se guardan los datos en fichero log
					Utilidades.fijosLog("Editar",id,resultFechaDoc,resultTipoGasto,txtImporte.getText(),resultFechaInicio,resultFechaFin,resultNumDoc);
					//Sentencia UPDATE
					sql="UPDATE gastos_fijos SET idGasto="+id+", fecha_factura='"+resultFechaDoc+"', num_factura='"+resultNumDoc+"', tipo_gasto='"+resultTipoGasto+"', fecha_inicio='"+resultFechaInicio+"', fecha_fin='"+resultFechaFin+"', duracion='"+resultDuracion+"', importe='"+resultImporte+"' WHERE idGasto='"+id+"'";
					// Ejecuta la actualizacion
					if(BD.actualizar(sql)==true){
						// Muestra un aviso al usuario
						JOptionPane.showMessageDialog(null, "El gasto se ha editado correctamente.");
						// Se instancia el controlador Principal
				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
				  		// Desparece esta ventana
				  		setVisible(false);
					}
				} // si el importe es numero				
			}			
		}// Cierre evento btnMenu
		if(evento.getSource()==btnBorrar){ 
			
			// Obtener el id de la ventana
			id=Integer.parseInt(txtID.getText());
			// Se guardan los datos en fichero log
			Utilidades.fijosLog("Borrar",id,"","","","","","");
			// Sentencia DELETE
			sql="DELETE FROM gastos_fijos WHERE idGasto=?;";
			// Elimina el registro
			if(BD.borrarRegistro(sql,id)==true){
				// Muestra aviso al usuario
				JOptionPane.showMessageDialog(null, "El gasto ha sido borrado.");
				// Se instancia el controlador Principal
		  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
		  		// Desparece la ventana editar Gastos de Tienda
		  		setVisible(false);
			}			
		} // Cierre del btnBorrar
		
	} // Cierre del metodo actionPerformed
	
	// getters
	public static JTextField getTxtImporte() {
		return txtImporte;
	}

	public static JTextField getTxtNumFactura() {
		return txtNumFactura;
	}

	public static JTextField getTxtDuracion() {
		return txtDuracion;
	}
} // Cierre clase