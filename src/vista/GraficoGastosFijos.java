package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import controlador.ControladorPrincipal;
import modelo.GastoFijo;
import utilidades.BD;
import modelo.ComboFijos;

@SuppressWarnings("serial")
public class GraficoGastosFijos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel MainPanel;
	private JPanel TopPanel;
	private ComboFijos gastosFijos;
	private JComboBox<ComboFijos> cbTipoGasto;
	private int idGastoFijo;
	private String nombreFijo;
	private DefaultCategoryDataset dataset=new DefaultCategoryDataset();
	private GastoFijo gastoFijo;
	private ArrayList <GastoFijo> valores=new ArrayList <GastoFijo>();
	private double importe;
	private String fechaDoc;	
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private JButton btnVer;
	private JButton btnMenu;
	private JButton btnGuardar;
	private JButton btnEnviar;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		private String resultTipoGasto;
		
	public GraficoGastosFijos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 691, 609); // tama�o
		setTitle("Grafico de gastos fijos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/chart.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
			// Sentencia SQL SELECT
			sql = "SELECT * FROM fijos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idGastoFijo=rs.getInt("idFijos");
				nombreFijo=rs.getString("nombre");
				
				// Se instancia la clase Tipo_compra
				gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoGasto.addItem(gastosFijos);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layouts
		MainPanel = new JPanel();
		MainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));		
		setContentPane(MainPanel);
		MainPanel.setLayout(null);
		
		TopPanel = new JPanel();
		TopPanel.setBounds(0, 84, 685, 432);
		TopPanel.setPreferredSize(new Dimension(600, 250));
		TopPanel.setBackground(Color.GRAY);
		MainPanel.add(TopPanel);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 555, 95, 14);
		MainPanel.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Elige el gasto fijo a visualizar.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 26, 254, 14);
		MainPanel.add(lblAviso);
		
		cbTipoGasto = new JComboBox<ComboFijos>();
		cbTipoGasto.addActionListener((ActionListener)this);
		cbTipoGasto.setBounds(300, 36, 141, 20);
		MainPanel.add(cbTipoGasto);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(524, 20, 126, 53);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/chart.png")));
		MainPanel.add(btnVer);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(308, 527, 126, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		MainPanel.add(btnMenu);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(159, 528, 126, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		MainPanel.add(btnGuardar);
		
		btnEnviar = new JButton("Eviar");
		btnEnviar.addActionListener((ActionListener)this);
		btnEnviar.setBackground(new Color(184, 231, 255));
		btnEnviar.setBounds(457, 528, 126, 41);
		btnEnviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/send.png")));
		MainPanel.add(btnEnviar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en boton "Ver"
		if(evento.getSource()==btnVer){
			if(cbTipoGasto.isValid()==true){ // Se valida JComboBox
				if(cbTipoGasto.getSelectedItem()!=null){ // Si lo seleccionado no es nulo
					try{	
						// Se guarda lo seleccionado en una variable
						resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
						// Sentencia SELECT
						sql="SELECT importe,fecha_factura FROM gastos_fijos WHERE tipo_gasto='"+resultTipoGasto+"' ORDER BY fecha_factura ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) {
							
							// Obtener los datos BD y guardar en los arrys
							importe=rs.getDouble(1);
							fechaDoc=rs.getString(2);
							// Se crea el objeto
							gastoFijo=new GastoFijo(importe,fechaDoc);
							// Se a�ade al arraylist
							valores.add(gastoFijo);
														
						}// Cierre de while			
						rs.close(); //Cierre de la consulta
						
						// Se recorre el arraylist
						for(int i=0;i<valores.size();i++){
							String numeros=Double.toString(valores.get(i).getImporte())+" �";
							dataset.addValue(valores.get(i).getImporte(),valores.get(i).getFechaDoc(),resultTipoGasto+" "+numeros);
						}
						
						// Establece los datos de la tabla
						chart=ChartFactory.createBarChart("Gastos fijos", "", "", dataset,PlotOrientation.VERTICAL,true,true,false);
						
						// Category for bar chart
						CategoryPlot catPlot=chart.getCategoryPlot();
						catPlot.setRangeGridlinePaint(Color.BLACK);
						
						// Agrega chart al panel
						chartPanel=new ChartPanel(chart);
						TopPanel.removeAll();
						TopPanel.add(chartPanel, BorderLayout.CENTER);
						TopPanel.validate();
						// Limpia los valores
						resultTipoGasto="";
						cbTipoGasto.setSelectedIndex(0);
						valores.clear();
						for(int i=0;i<valores.size();i++){
							valores.remove(i);			
							valores.get(i).setImporte(0.0);
							valores.get(i).setFechaDoc("");
						}
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}
			}
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		// Si hace clic en "Guardar"
		if(evento.getSource()==btnGuardar){ 
			try {
				// Obtiene informacion a renderizar
				final ChartRenderingInfo info=new ChartRenderingInfo(new StandardEntityCollection());
				//final File file=new File("src/archivos/grafico_gastosFijos.png");
				// Se crea un archivo de tipo png
				final File file=new File("archivos/grafico_gastosFijos.png");
				// Se guarda el grafico como PNG
				ChartUtilities.saveChartAsPNG(file, chart, chartPanel.getMaximumDrawWidth(), chartPanel.getMaximumDrawHeight());
				// Muestra un mensaje al usuario
				JOptionPane.showMessageDialog(null,"El grafico se ha guardado.");
			}
			catch (IOException ioe) {
				// Muestra error
				JOptionPane.showMessageDialog(null, ioe.getMessage());
			}
		}
		// Si hace clic en "Enviar"
		if(evento.getSource()==btnEnviar){
			try {
				
				// Se instancia la clase Properties
				Properties props=new Properties();
				// Se establece el nombre del host de correo, es smtp.gmail.com
				props.setProperty("mail.smtp.host", "smtp.gmail.com");
				// TLS disponible
				props.setProperty("mail.smtp.starttls.enable", "true");
				// Puerto de gmail para envio de correos
				props.setProperty("mail.smtp.port","587");
				// Nombre del usuario de gmail
				props.setProperty("mail.smtp.user", "economia.casa.gastos@gmail.com");
				// Requiere usuario y password para conectarse
				props.setProperty("mail.smtp.auth", "true");
				
				// Se obtiene la session
				Session session = Session.getDefaultInstance(props);
				session.setDebug(true);
				
				// Crea la parte del texto
				BodyPart texto = new MimeBodyPart();		
				texto.setText("Grafico de comida");
				// Crea la parte adjunta (imagen)
				BodyPart adjunto = new MimeBodyPart();
				//adjunto.setDataHandler(new DataHandler(new FileDataSource("src/archivos/grafico_comida.png")));
				adjunto.setDataHandler(new DataHandler(new FileDataSource("archivos/grafico_comida.png")));
				adjunto.setFileName("grafico_comida.png");
				// Crea un mime multi para juntarlo
				MimeMultipart multiParte = new MimeMultipart();
				multiParte.addBodyPart(texto);
				multiParte.addBodyPart(adjunto);			

				// Crear objeto mensaje con la session obtenida
				MimeMessage message = new MimeMessage(session);
				// Quien envia el correo
				message.setFrom(new InternetAddress("economia.casa.gastos@gmail.com"));
				// A quien va dirigido
				//message.addRecipient(Message.RecipientType.TO, new InternetAddress("eli91ai@gmail.com"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress("amaia_7_imaz@hotmail.es"));
				// Asunto
				message.setSubject("Grafico de comida");
				// Se mete el texto y la foto adjunta
				message.setContent(multiParte);
				
				// Se instancia la clase Transport para enviar mensaje
				Transport transport = session.getTransport("smtp");
				// Establece conexion
				transport.connect("economia.casa.gastos@gmail.com","3M@il91eco");
				// Envia mensaje
				transport.sendMessage(message,message.getAllRecipients());
				// Cierre conexion
				transport.close();
				// Muestra un mensaje al usuario
				JOptionPane.showMessageDialog(null,"Se ha enviado el correo.");
			}
			catch (AddressException ae) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, ae.getMessage());
			}
			catch (NoSuchProviderException nspe) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, nspe.getMessage());
			}
			catch (MessagingException me) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, me.getMessage());
			}
		}
	} // Cierre del metodo actionPerformed
} // Cierre clase