package vista;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Principal extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private static String version="6.6";
	private JPanel contentPane;
	
		// items del menu
		private JMenuItem itemPresu;
		private JMenuItem itemIngreso;
		private JMenuItem itemEditarPresu;
		private JMenuItem itemEditarIngreso;
		private JMenuItem itemEditarGastosFijos;
		private JMenuItem itemProducto;
		private JMenuItem itemPresupuestos;
		private JMenuItem itemIngresos;
		private JMenuItem itemVerGastos;
		private JMenuItem itemConcretos;
		private JMenuItem itemVerGastosFijos;
		private JMenuItem itemSemanal;
		private JMenuItem itemComprasLog;
		private JMenuItem itemFijosLog;
		private JMenuItem itemGraficoComida;
		private JMenuItem itemGraficoOnline;
		private JMenuItem itemGraficoGastoFijo;
		private JMenuItem itemCrearMail;
		private JMenuItem itemRecibirMail;
		
		// accesos directos - botones
		private JButton btnCompra;
		private JButton btnFijo;
		private JButton btnEditarCompras;
		//private JButton btnDiario;		
		private JButton btnFechas;
		private JButton btnFijos;
		private JButton btnAhorroMensual;		
		
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Principal(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 574, 326); // tama�o
		setTitle("Contabilidad"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/coins.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // La equis de la ventana detiene la aplicacion
		
		// Llama al metodo componentes
		componentes();
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Principal.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Version label
		JLabel lblVersion = new JLabel("Version: "+version);
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 272, 79, 14);
		contentPane.add(lblVersion);
		
		//contenedor de menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 581, 21);
		contentPane.add(menuBar);
		
		// menu + items del menu
		JMenu mnAnadir = new JMenu("A�adir"); 
		menuBar.add(mnAnadir);
		
		itemPresu = new JMenuItem("Presupuesto");
		itemPresu.addActionListener((ActionListener)this);
		mnAnadir.add(itemPresu);
		
		itemIngreso = new JMenuItem("Ingreso");
		itemIngreso.addActionListener((ActionListener)this);
		mnAnadir.add(itemIngreso);
		
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		itemEditarPresu = new JMenuItem("Presupuestos");
		itemEditarPresu.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarPresu);
		
		itemEditarIngreso = new JMenuItem("Ingresos");
		itemEditarIngreso.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarIngreso);
		
		itemEditarGastosFijos = new JMenuItem("Gastos Fijos");
		itemEditarGastosFijos.addActionListener((ActionListener)this);
		mnEditar.add(itemEditarGastosFijos);
		
		
		
		JMenu mnBuscar = new JMenu("Buscar");
		menuBar.add(mnBuscar);
		
		itemProducto = new JMenuItem("Producto");
		itemProducto.addActionListener((ActionListener)this);
		mnBuscar.add(itemProducto);
		
		
		JMenu mnVer = new JMenu("Ver");
		menuBar.add(mnVer);
		
		itemPresupuestos = new JMenuItem("Presupuestos");
		itemPresupuestos.addActionListener((ActionListener)this);
		mnVer.add(itemPresupuestos);
		
		itemIngresos = new JMenuItem("Ingresos");
		itemIngresos.addActionListener((ActionListener)this);
		mnVer.add(itemIngresos);
		
		itemVerGastos = new JMenuItem("Gastos diarios");
		itemVerGastos.addActionListener((ActionListener)this);
		mnVer.add(itemVerGastos);
		
		itemConcretos = new JMenuItem("Gastos concretos");
		itemConcretos.addActionListener((ActionListener)this);
		mnVer.add(itemConcretos);
		
		itemVerGastosFijos = new JMenuItem("Gastos fijos");
		itemVerGastosFijos.addActionListener((ActionListener)this);
		mnVer.add(itemVerGastosFijos);
		
		
		JMenu mnAhorro = new JMenu("Ahorro");
		menuBar.add(mnAhorro);
		
		itemSemanal = new JMenuItem("Semanal");
		itemSemanal.addActionListener((ActionListener)this);
		mnAhorro.add(itemSemanal);
		
		
		JMenu mnFichero = new JMenu("Fichero");
		menuBar.add(mnFichero);
		
		itemComprasLog = new JMenuItem("Compras log");
		itemComprasLog.addActionListener((ActionListener)this);
		mnFichero.add(itemComprasLog);
		
		itemFijosLog = new JMenuItem("Fijos log");
		itemFijosLog.addActionListener((ActionListener)this);
		mnFichero.add(itemFijosLog);
		
				
		JMenu mnGrafico = new JMenu("Grafico");
		menuBar.add(mnGrafico);
		
		itemGraficoComida = new JMenuItem("Comida");
		itemGraficoComida.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoComida);
		
		itemGraficoOnline = new JMenuItem("Tarjeta NET");
		itemGraficoOnline.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoOnline);		
		
		itemGraficoGastoFijo = new JMenuItem("Gasto fijo");
		itemGraficoGastoFijo.addActionListener((ActionListener)this);
		mnGrafico.add(itemGraficoGastoFijo);
		
				
		JMenu mnEmail = new JMenu("Email");
		menuBar.add(mnEmail);
		
		itemCrearMail = new JMenuItem("Enviar");
		itemCrearMail.addActionListener((ActionListener)this);
		mnEmail.add(itemCrearMail);
		
		itemRecibirMail = new JMenuItem("Recibir");
		itemRecibirMail.addActionListener((ActionListener)this);
		mnEmail.add(itemRecibirMail);
		
		
		
		// Accesos directos - botones
		btnCompra = new JButton("Compra");
		btnCompra.addActionListener((ActionListener)this);
		btnCompra.setBackground(new Color(184, 231, 255));
		btnCompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		btnCompra.setBounds(10, 75, 152, 76);
		contentPane.add(btnCompra);	
		
		btnFijo = new JButton("Fijo");
		btnFijo.addActionListener((ActionListener)this);
		btnFijo.setBackground(new Color(184, 231, 255));
		btnFijo.setBounds(202, 75, 152, 76);
		btnFijo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		contentPane.add(btnFijo);
		
		btnEditarCompras = new JButton("Compras");
		btnEditarCompras.addActionListener((ActionListener)this);
		btnEditarCompras.setBackground(new Color(184, 231, 255));
		btnEditarCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/edit.png")));
		btnEditarCompras.setBounds(392, 75, 152, 76);
		contentPane.add(btnEditarCompras);
		
		/*btnDiario = new JButton("Diario");
		btnDiario.addActionListener((ActionListener)this);
		btnDiario.setBackground(new Color(184, 231, 255));
		btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		btnDiario.setBounds(10, 174, 152, 76);
		contentPane.add(btnDiario);*/
		
		btnFechas = new JButton("Fechas");
		btnFechas.addActionListener((ActionListener)this);
		btnFechas.setBackground(new Color(184, 231, 255));
		btnFechas.setBounds(10, 174, 152, 76);
		btnFechas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnFechas);
		
		btnFijos = new JButton("Fijos");
		btnFijos.addActionListener((ActionListener)this);
		btnFijos.setBackground(new Color(184, 231, 255));
		btnFijos.setBounds(202, 174, 152, 76);
		btnFijos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnFijos);
		
		btnAhorroMensual = new JButton("Ahorro");
		btnAhorroMensual.addActionListener((ActionListener)this);
		btnAhorroMensual.setBackground(new Color(184, 231, 255));
		btnAhorroMensual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/coins.png")));
		btnAhorroMensual.setBounds(392, 174, 152, 76);
		contentPane.add(btnAhorroMensual);
		
	} // Cierre componentes
	
	/**
     * Metodo que devuelve la version de la app para mostrar en labels.
     *  
     */
	public static String version(){
		
		return version;
	} // Cierre de version
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en menu A�adir => "Presupuesto"
		if(evento.getSource()==itemPresu ){ 
			// Se instancia la ventana correspondiente
			AnadirPresupuesto ventanaPresupuesto=new AnadirPresupuesto();
			// Coloca la ventana en el centro de la pantalla
			ventanaPresupuesto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaPresupuesto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu A�adir => "Ingreso"
		if(evento.getSource()==itemIngreso){
			// Se instancia la ventana correspondiente
			AnadirIngreso ventanaAnadirIngreso=new AnadirIngreso();
			// Coloca la ventana en el centro de la pantalla
			ventanaAnadirIngreso.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaAnadirIngreso.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton "Compra"
		if(evento.getSource()==btnCompra){ 
			// Se instancia la ventana correspondiente
			AnadirCompra ventanaCompra=new AnadirCompra();
			// Coloca la ventana en el centro de la pantalla
			ventanaCompra.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCompra.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton "Fijo"
		if(evento.getSource()==btnFijo){ 
			// Se instancia la ventana correspondiente
			AnadirGastoFijo ventanaGastoFijo=new AnadirGastoFijo();
			// Coloca la ventana en el centro de la pantalla
			ventanaGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}		
		// Si hace clic en boton "Compras"
		if(evento.getSource()==btnEditarCompras){
			// Se instancia la ventana correspondiente
			TablaCompras ventanaTablaCompras=new TablaCompras();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaCompras.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaCompras.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar => "Presupuestos", visualizara la tabla de presupuestos
		if(evento.getSource()==itemEditarPresu){
			// Se instancia la ventana correspondiente
			TablaPresupuestos ventanaTablaPresupuestos=new TablaPresupuestos();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaPresupuestos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaPresupuestos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar => "Ingresos", visualizara la tabla de ingresos
		if(evento.getSource()==itemEditarIngreso){
			// Se instancia la ventana correspondiente
			TablaIngresos ventanaTablaIngresos=new TablaIngresos();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaIngresos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en  menu Editar => "Gastos fijos", visualizara la tabla de compras
		if(evento.getSource()==itemEditarGastosFijos){
			// Se instancia la ventana correspondiente
			TablaGastosFijos ventanaTablaGastosFijos=new TablaGastosFijos();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaGastosFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaGastosFijos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Buscar => "Producto"
		if(evento.getSource()==itemProducto){
			// Se instancia la ventana correspondiente
			BuscarProducto ventanaBuscarProducto=new BuscarProducto();
			// Coloca la ventana en el centro de la pantalla
			ventanaBuscarProducto.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaBuscarProducto.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ver => "Presupuestos", visualizara los presupuestos
		if(evento.getSource()==itemPresupuestos ){ 
			// Se instancia la ventana correspondiente
			VerPresupuestos ventanaPresupuestos=new VerPresupuestos();
			// Coloca la ventana en el centro de la pantalla
			ventanaPresupuestos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaPresupuestos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ver => "Ingresos"
		if(evento.getSource()==itemIngresos){
			// Se instancia la ventana correspondiente
			VerIngresos ventanaIngresos=new VerIngresos();
			// Coloca la ventana en el centro de la pantalla
			ventanaIngresos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaIngresos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton "Diario" o en menu Ver => "Gastos diarios"
		if(/*evento.getSource()==btnDiario ||*/ evento.getSource()==itemVerGastos){ 
			// Se instancia la ventana correspondiente
			VerDiario ventanaDiario=new VerDiario();
			// Coloca la ventana en el centro de la pantalla
			ventanaDiario.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaDiario.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton "Fechas" o en menu Ver => "Gastos concretos"
		if(evento.getSource()==btnFechas ||evento.getSource()==itemConcretos){ 
			// Se instancia la ventana correspondiente
			VerConcretos ventanaConcretos=new VerConcretos();
			// Coloca la ventana en el centro de la pantalla
			ventanaConcretos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaConcretos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en boton "Fijos" o en menu Ver => "Gastos fijos"
		if(evento.getSource()==btnFijos || evento.getSource()==itemVerGastosFijos){
			// Se instancia la ventana correspondiente
			VerGastosFijos ventanaVerGastosFijo=new VerGastosFijos();
			// Coloca la ventana en el centro de la pantalla
			ventanaVerGastosFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaVerGastosFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Ahorro => Semanal 
		if(evento.getSource()==itemSemanal){ 
			// Se instancia la ventana correspondiente
			VerSemanal ventanaSemanal=new VerSemanal();
			// Coloca la ventana en el centro de la pantalla
			ventanaSemanal.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaSemanal.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en el boton "Ahorro"
		if(evento.getSource()==btnAhorroMensual){ 
			// Se instancia la ventana correspondiente
			VerMensual ventanaMensual=new VerMensual();
			// Coloca la ventana en el centro de la pantalla
			ventanaMensual.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaMensual.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}		
		// Si hace clic en menu Fichero => "Compras log"
		if(evento.getSource()==itemComprasLog){
			// Se instancia la ventana correspondiente
			LogCompras ventanaLogCompras=new LogCompras();
			// Coloca la ventana en el centro de la pantalla
			ventanaLogCompras.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaLogCompras.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Fichero => "Fijos log"
		if(evento.getSource()==itemFijosLog){
			// Se instancia la ventana correspondiente
			LogGastosFijos ventanaLogFijos=new LogGastosFijos();
			// Coloca la ventana en el centro de la pantalla
			ventanaLogFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaLogFijos.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico => "Comida"
		if(evento.getSource()==itemGraficoComida){
			// Se instancia la ventana correspondiente
			GraficoComida ventanaGraficoComida=new GraficoComida();
			// Coloca la ventana en el centro de la pantalla
			ventanaGraficoComida.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoComida.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico => "Tarjeta NET"
		if(evento.getSource()==itemGraficoOnline){
			// Se instancia la ventana correspondiente
			GraficoOnline ventanaGraficoOnline=new GraficoOnline();
			// Coloca la ventana en el centro de la pantalla
			ventanaGraficoOnline.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoOnline.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Grafico => "Gasto Fijo"
		if(evento.getSource()==itemGraficoGastoFijo){
			// Se instancia la ventana correspondiente
			GraficoGastosFijos ventanaGraficoGastoFijo=new GraficoGastosFijos();
			// Coloca la ventana en el centro de la pantalla
			ventanaGraficoGastoFijo.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaGraficoGastoFijo.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Email => "Enviar"
		if(evento.getSource()==itemCrearMail){
			// Se instancia la ventana correspondiente
			EnviarMail ventanaCrearMail=new EnviarMail();
			// Coloca la ventana en el centro de la pantalla
			ventanaCrearMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCrearMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en menu Email => "Recibir"
		if(evento.getSource()==itemRecibirMail){
			// Se instancia la ventana correspondiente
			RecibirMail ventanaRecibirMail=new RecibirMail();
			// Coloca la ventana en el centro de la pantalla
			ventanaRecibirMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaRecibirMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
	} // Cierre actionPerfomed
} // Cierre de clase