package vista;

import java.awt.*;
import java.io.*;
import java.util.*;

import javax.imageio.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.*;

import org.jdesktop.swingx.*;

public class CargadorMensaje {
	
	// Atributos de la clase
	private PanelBandeja pb;
	private Store store;
	private Folder folder;
	private int indice=0;
	private int maximo=15;
	
	public CargadorMensaje(PanelBandeja panelBandeja){
		
		this.pb=panelBandeja;
		
	} // Constructor
	
	public void run(){
		
		Properties props = System.getProperties();
		props.setProperty("mail.pop3.starttls.enable", "false");
		props.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.pop3.socketFactory.fallback", "false");
		props.setProperty("mail.pop3.port", "995");
		props.setProperty("mail.pop3.socketFactory.port", "995");
		
		Session sesion = Session.getDefaultInstance(props,null);
		
		try {
			store = sesion.getStore("pop3s");
		    store.connect("pop.gmail.com", "economia.casa.gastos@gmail.com", "3M@il91eco");
			if(store.isConnected()){
				// Se obtiene la carpeta "INBOX"
				folder = store.getFolder("INBOX");
				folder.open(Folder.READ_ONLY);
				int count=folder.getMessageCount();
				if(count==0){
					JOptionPane.showMessageDialog(null, "No tienes mensaje para leer.");
				}
				while(indice<maximo){
					if(indice<count){
						listarMensajes(folder.getMessages()[indice]);
						indice++;
						pb.getContenedor().repaint();
						pb.getRecibirMail().repaint();
					}else{
						break;
					}
				}
				folder.close(false);
				store.close();
			}
		} catch (MessagingException me) {
			JOptionPane.showMessageDialog(null, "Error, intentalo otra vez");
		
		} catch (SecurityException se){
			JOptionPane.showMessageDialog(null, "Error, intentalo otra vez");
			
		}
		
	} // Run
	
	/**
	 * @wbp.parser.entryPoint
	 */
	@SuppressWarnings("deprecation")
	public void listarMensajes(Message mensaje){
		try{
			JXTaskPane msg=new JXTaskPane();
			msg.setAnimated(true);
			msg.setPreferredSize(new Dimension(350,400));
			JEditorPane visulizadormensaje=new JEditorPane();
			visulizadormensaje.setEditable(false);
			visulizadormensaje.setPreferredSize(new Dimension(350,400));
			Object o = mensaje.getContent();
			if (o instanceof String) {
				if (mensaje.getContentType().indexOf("text/html") != -1) {
					visulizadormensaje.setContentType("text/html");
					visulizadormensaje.setText(""+mensaje.getContent());
				}
				else{
					visulizadormensaje.setContentType("text/plain");
					visulizadormensaje.setText(""+mensaje.getContent());
				}
			}
			else{
				if (o instanceof Multipart) {
					Multipart mp = (Multipart)o;
					int numPart = mp.getCount();
					for (int j=0; j < numPart; j++) {
						Part part = mp.getBodyPart(j);
						String disposition = part.getDisposition();
						if (disposition == null) {
							if (part.isMimeType("multipart/*") || part.isMimeType("text/*")) {
								String cuerpoMensaje;
								if (part.isMimeType("multipart/*")) {	
									
									Multipart mp2 = (Multipart) part.getContent();
									Part part2 = mp2.getBodyPart(0);
									cuerpoMensaje = (String)part2.getContent();
									visulizadormensaje.setText(""+cuerpoMensaje);
									
								}
								else{
									// Si es texto
									if (part.isMimeType("text/plain")){
										
										cuerpoMensaje = (String)part.getContent();
										visulizadormensaje.setText(""+cuerpoMensaje);
									}
								}
							}
							else {
								if (part.getContentType().indexOf("text/html") != -1) {
									MimeBodyPart mbp = (MimeBodyPart)part;
									visulizadormensaje.setContentType("text/html");
									visulizadormensaje.setText(""+mbp.getContent());
								}
							}
						}
						else{
							if ((disposition != null) && disposition.equalsIgnoreCase(Part.ATTACHMENT) || (disposition.equalsIgnoreCase(Part.INLINE))) {
								String nombrePart = part.getFileName();
								if (nombrePart == null){
									//nombrePart = "adjunto" + i;
									MimeBodyPart mbp = (MimeBodyPart)part;
									visulizadormensaje.setText(""+mbp.getContent());
								}
							}
						}
					} // Cierre for
				} 
			}
        	Address[] fromAddresses = mensaje.getFrom();
        	String fromAdress=fromAddresses[0].toString();
			msg.setTitle(""+mensaje.getSubject()+" "+mensaje.getSentDate().getDate()+"/"+mensaje.getSentDate().getMonth()+"/"+(mensaje.getSentDate().getYear()+1900)+"    "+fromAdress);
			JScrollPane scroll=new JScrollPane(visulizadormensaje);
			scroll.setPreferredSize(new Dimension(350,400));
			msg.add(scroll);
			pb.getContenedor().add(msg);	
		}
		catch(MessagingException me){
			JOptionPane.showMessageDialog(null, me.getMessage());				
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());			
		}
	} // Cierre enlistarMensajes
	
    @SuppressWarnings("unused")
	private static void muestraImagen(Part unaParte)throws IOException, MessagingException {
    	
            JFrame frame = new JFrame();
            ImageIcon icono = new ImageIcon(ImageIO.read(unaParte.getInputStream()));
            JLabel l = new JLabel(icono);
            frame.getContentPane().add(l);
            frame.pack();
            frame.setVisible(true);
            
    } // MuestraImagen
    
	@SuppressWarnings({ "resource", "unused" })
	private static void guardaImagen(Part unaParte)throws FileNotFoundException, MessagingException, IOException{
	        FileOutputStream fichero = new FileOutputStream("d:/" + unaParte.getFileName());
	        InputStream imagen = unaParte.getInputStream();
	        byte[] bytes = new byte[1000];
	        int leidos = 0;
	        while ((leidos = imagen.read(bytes)) > 0){
	            fichero.write(bytes, 0, leidos);
	        }
	} // GuardaImagen

} // Clase