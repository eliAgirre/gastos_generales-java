package vista;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import modelo.*;
import utilidades.BD;
import utilidades.Utilidades;

@SuppressWarnings("serial")
public class VerDiario extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private Tipo_compra tipoCompra;
	private JComboBox<Tipo_compra> cbTipoCompra;
	private int idTipoCompra;
	private String tipo;
	private Tipo_tienda tipoTienda;
	private JComboBox<Tipo_tienda> cbTipoTienda;
	private int idTipoTienda;
	private String tipoTiendaS;
	private Tienda tienda;
	private JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private int codigoTienda;
	private FormaPago formaPago;
	private JComboBox<FormaPago> cbFormaPago;
	private int codigoCompra;
	private int idFormaPago;
	private String tipoPago;
	private JLabel lblCosteTotal;
	private JLabel lblCantidad;
	private JButton btnVer;
	private JButton btnDiario;
	private JButton btnMenu;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
		
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"Fecha","Compra","Tipo","Tienda","Producto","Coste","Pago", "Tarjeta"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[8];
		
		// Atributos del resultados user
		private String resultTipoCompra;
		private String resultTipoTienda;
		private String resultTienda;
		private String resultFormaPago;
	
	
	public VerDiario(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 678, 609); // tama�o
		setTitle("Gastos diarios"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
			// Sentencia SQL SELECT
			sql = "SELECT * FROM compras ORDER BY fecha ASC, idCompra ASC;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan en el array los datos de BD
				datosBD[0]=rs.getString(2); //fecha
				datosBD[1]=rs.getString(3); //tipo_compra
				datosBD[2]=rs.getString(4); //tipo_tienda
				datosBD[3]=rs.getString(5); //tienda
				datosBD[4]=rs.getString(6); //producto
				datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
				datosBD[6]=rs.getString(8); //forma_pago
				datosBD[7]=rs.getString(9); //tarjeta
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
			
			} // Cierre de while
				
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_compra;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoCompra=rs.getInt("idCompra");
				tipo=rs.getString("tipo");
				
				// Se instancia la clase Tipo_compra
				tipoCompra = new Tipo_compra (idTipoCompra,tipo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoCompra.addItem(tipoCompra);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta

			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoTienda=rs.getInt("idTipo");
				tipoTiendaS=rs.getString("tipo");
				
				// Se instancia la clase Tipo_tienda
				tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoTienda.addItem(tipoTienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			lblCosteTotal.setVisible(true);
			lblCantidad.setVisible(true);
			sql="SELECT sum(coste) FROM compras;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			// Mientras que haya datos
			while (rs.next()) { 						
				lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			resultTipoTienda="";
			resultTienda="";
			
			limpiar();
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 153, 617, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se establece una anchura de las columnas
		tabla.getColumn("Fecha").setPreferredWidth(40); 
		//tabla.getColumn("Compra").setPreferredWidth(30);
		tabla.getColumn("Compra").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setPreferredWidth(0); // Se oculta la columna
		//tabla.getColumn("Tipo").setPreferredWidth(30);
		tabla.getColumn("Tipo").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Tienda").setPreferredWidth(40); 
		tabla.getColumn("Producto").setPreferredWidth(200);
		tabla.getColumn("Coste").setPreferredWidth(20);
		tabla.getColumn("Pago").setPreferredWidth(30);
		//tabla.getColumn("Tarjeta").setPreferredWidth(20);
		tabla.getColumn("Tarjeta").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(false);
		tabla.setBorder(null);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 554, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Puede seleccionar de las siguientes listas:");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setBounds(10, 26, 323, 14);
		contentPane.add(lblAviso);
		
		JLabel lblTipoCompra = new JLabel("Tipo compra:");
		lblTipoCompra.setForeground(new Color(0, 139, 139));
		lblTipoCompra.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoCompra.setBounds(24, 63, 88, 14);
		contentPane.add(lblTipoCompra);
		
		cbTipoCompra = new JComboBox<Tipo_compra>();
		cbTipoCompra.addActionListener((ActionListener)this);
		cbTipoCompra.setBounds(112, 60, 126, 20);
		contentPane.add(cbTipoCompra);
		
		JLabel lblTipoTienda = new JLabel("Tipo tienda:");
		lblTipoTienda.setForeground(new Color(0, 139, 139));
		lblTipoTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoTienda.setBounds(24, 99, 88, 14);
		contentPane.add(lblTipoTienda);
		
		cbTipoTienda = new JComboBox<Tipo_tienda>();
		cbTipoTienda.addActionListener((ActionListener)this);
		cbTipoTienda.setBounds(112, 96, 126, 20);
		contentPane.add(cbTipoTienda);
		
		JLabel lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(257, 96, 88, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tienda>();		
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(349, 93, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblFormaPago = new JLabel("Forma pago:");
		lblFormaPago.setForeground(new Color(0, 139, 139));
		lblFormaPago.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFormaPago.setBounds(257, 60, 95, 14);
		contentPane.add(lblFormaPago);
		
		cbFormaPago = new JComboBox<FormaPago>();
		cbFormaPago.addActionListener((ActionListener)this);
		cbFormaPago.setBounds(350, 57, 126, 20);
		contentPane.add(cbFormaPago);
		
		lblCosteTotal = new JLabel("Total:");
		lblCosteTotal.setVisible(false);
		lblCosteTotal.setForeground(new Color(0, 139, 139));
		lblCosteTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCosteTotal.setBounds(422, 481, 50, 14);
		contentPane.add(lblCosteTotal);
		
		lblCantidad= new JLabel();
		lblCantidad.setVisible(false);
		lblCantidad.setForeground(Color.BLACK);
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantidad.setBounds(470, 481, 78, 14);
		contentPane.add(lblCantidad);
		
		btnVer = new JButton("Consultar");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(500, 54, 140, 59);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		btnDiario = new JButton("Diario");
		btnDiario.addActionListener((ActionListener)this);
		btnDiario.setBackground(new Color(184, 231, 255));
		btnDiario.setBounds(365, 528, 95, 41);
		btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/diary.png")));
		contentPane.add(btnDiario);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(220, 528, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	// Limpia los datos de los componentes
	private void limpiar(){
		cbTipoCompra.setSelectedItem(null);
		cbTipoTienda.setSelectedItem(null);
		cbTienda.setSelectedItem(null);
		cbFormaPago.setSelectedItem(null);
	} // Cierre limpiar
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado alguna opcion de "Tipo compra"
		if(evento.getSource()==cbTipoCompra){
			if(cbTipoCompra.isValid()==true){
				// Guarda el valor de item seleccionado "Tipo compra"
				resultTipoCompra=cbTipoCompra.getSelectedItem().toString();
				// Limpia los anteriores items de la lista desplegable
				cbFormaPago.removeAllItems();
				try {
					// Sentencia SELECT
					sql = "SELECT * FROM tipo_compra WHERE tipo='"+resultTipoCompra+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						// Se obtiene el codigo de tipo de compra
						codigoCompra = rs.getInt("idCompra");
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					
					// Sentencia SELECT
					sql = "SELECT * FROM forma_pago WHERE idTipoCompra="+codigoCompra+";";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						
						//Obtienen los datos
						idFormaPago=rs.getInt("idFormaPago");
						tipoPago=rs.getString("tipo_pago");
						
						// Se instancia la clase 
						formaPago = new FormaPago (idFormaPago,tipoPago); 
						
						//Se a�ade cada objeto al JComboBox
						cbFormaPago.addItem(formaPago);
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
			}
		}
		// Si ha seleccionado alguna opcion de "Tipo tienda"
		if(evento.getSource()==cbTipoTienda){
			if(cbTipoTienda.isValid()==true){
				// Guarda el valor de item seleccionado "Tipo tienda"
				resultTipoTienda=cbTipoTienda.getSelectedItem().toString();
				// Limpia los anteriores items de la lista desplegable
				cbTienda.removeAllItems();
				try {
					// Sentencia SELECT
					sql = "SELECT * FROM tipo_tienda WHERE tipo='"+resultTipoTienda+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						// Se obtiene el codigo de tipo de tienda
						codigoTienda = rs.getInt("idTipo");
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					
					// Sentencia SELECT
					sql = "SELECT * FROM tiendas WHERE idTipo="+codigoTienda+";";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						
						//Obtienen los datos
						idTienda=rs.getInt("idTienda");
						nombreTienda=rs.getString("nombre");
						
						// Se instancia la clase Tipo_tienda
						tienda = new Tienda (idTienda,nombreTienda); 
						
						//Se a�ade cada objeto al JComboBox
						cbTienda.addItem(tienda);
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					cbTienda.setSelectedItem(null);
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
			}
		} 
		if(evento.getSource()==cbTienda){
			if(cbTienda.isValid()==true){
				// Guarda el valor de item seleccionado
				resultTienda=cbTienda.getSelectedItem().toString();	
			}
		}
		// Si ha seleccionado alguna opcion de "Forma de pago"
		if(evento.getSource()==cbFormaPago){
			if(cbFormaPago.isValid()==true){ //si es v�lido JComboBox
				// Guarda el valor de item seleccionado "Forma de pago"
				resultFormaPago=cbFormaPago.getSelectedItem().toString();
			}		
		}
		// Si hace clic en "Consultar" podr� ver dependiendo de las opciones que haya elegido
		if(evento.getSource()==btnVer){
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			try{
				if(cbFormaPago.getSelectedItem()==null){
					if(cbTienda.getSelectedItem()==null){
						//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoTienda);
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE tipo_tienda='"+resultTipoTienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=rs.getString(2); //fecha
								datosBD[1]=rs.getString(3); //tipo_compra
								datosBD[2]=rs.getString(4); //tipo_tienda
								datosBD[3]=rs.getString(5); //tienda
								datosBD[4]=rs.getString(6); //producto
								datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[6]=rs.getString(8); //forma_pago
								datosBD[7]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE tipo_tienda='"+resultTipoTienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							resultTipoTienda="";
							resultTienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						cbTipoTienda.setSelectedIndex(0);
						
					}
					else{
						//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoTienda+" y tienda: "+resultTienda);
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE tipo_tienda='"+resultTipoTienda+"' and tienda='"+resultTienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=rs.getString(2); //fecha
								datosBD[1]=rs.getString(3); //tipo_compra
								datosBD[2]=rs.getString(4); //tipo_tienda
								datosBD[3]=rs.getString(5); //tienda
								datosBD[4]=rs.getString(6); //producto
								datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[6]=rs.getString(8); //forma_pago
								datosBD[7]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE tipo_tienda='"+resultTipoTienda+"' and tienda='"+resultTienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							resultTipoTienda="";
							resultTienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						
						// Limpia los componentes de la ventana
						resultTienda=null;
						cbTipoTienda.setSelectedIndex(0);
						cbTienda.setSelectedItem(null);						
					}
				}
				else{
					if(resultFormaPago==null){
						//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoCompra);
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE tipo_compra='"+resultTipoCompra+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=rs.getString(2); //fecha
								datosBD[1]=rs.getString(3); //tipo_compra
								datosBD[2]=rs.getString(4); //tipo_tienda
								datosBD[3]=rs.getString(5); //tienda
								datosBD[4]=rs.getString(6); //producto
								datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[6]=rs.getString(8); //forma_pago
								datosBD[7]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE tipo_compra='"+resultTipoCompra+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							resultTipoTienda="";
							resultTienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						
						// Limpia los componentes de la ventana
						cbTipoCompra.setSelectedIndex(0);
						cbFormaPago.setSelectedItem(null);
					}
					else{
						//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoCompra+" y tienda: "+resultFormaPago);
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE tipo_compra='"+resultTipoCompra+"' and forma_pago='"+resultFormaPago+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=rs.getString(2); //fecha
								datosBD[1]=rs.getString(3); //tipo_compra
								datosBD[2]=rs.getString(4); //tipo_tienda
								datosBD[3]=rs.getString(5); //tienda
								datosBD[4]=rs.getString(6); //producto
								datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[6]=rs.getString(8); //forma_pago
								datosBD[7]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE tipo_compra='"+resultTipoCompra+"' and forma_pago='"+resultFormaPago+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							resultTipoTienda="";
							resultTienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						
						// Limpia los componentes de la ventana
						resultFormaPago=null;
						cbTipoCompra.setSelectedIndex(0);
						cbFormaPago.setSelectedItem(null);	
					}
				}
				
			}
			catch(NullPointerException npe){}
		}
		// Si hace clic en "Diario" aparece los datos sin filtros
		if(evento.getSource()==btnDiario){
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			try{
				// Sentencia SQL SELECT
				sql = "SELECT * FROM compras;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan en el array los datos de BD
					datosBD[0]=rs.getString(2); //fecha
					datosBD[1]=rs.getString(3); //tipo_compra
					datosBD[2]=rs.getString(4); //tipo_tienda
					datosBD[3]=rs.getString(5); //tienda
					datosBD[4]=rs.getString(6); //producto
					datosBD[5]=Double.toString(rs.getDouble(7))+" �"; //coste
					datosBD[6]=rs.getString(8); //forma_pago
					datosBD[7]=rs.getString(9); //tarjeta
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
				
				} // Cierre de while
					
				rs.close(); //Cierre de la consulta
				
				lblCosteTotal.setVisible(true);
				lblCantidad.setVisible(true);
				sql="SELECT sum(coste) FROM compras;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) { 						
					lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
				resultTipoTienda="";
				resultTienda="";
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en "Menu" se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){
		
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		
	} // Cierre actionPerformed
} // Cierre de clase