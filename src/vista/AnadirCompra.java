package vista;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import gestor.Compras;
import modelo.*;
import utilidades.BD;
import utilidades.Utilidades;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class AnadirCompra extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private Tipo_compra tipoCompra;
	private JLabel lblTipoCompra;
	private JComboBox<Tipo_compra> cbTipoCompra;
	private int idTipoCompra;
	private String tipo;
	private Tipo_tienda tipoTienda;
	private static JComboBox<Tipo_tienda> cbTipoTienda;
	private int idTipoTienda;
	private String tipoTiendaS;
	private int codigoTienda;
	private Tienda tienda;
	private static JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private FormaPago formaPago;
	private static JComboBox<FormaPago> cbFormaPago;
	private int codigoCompra;
	private int idFormaPago;
	private String tipoPago;
	private JLabel lblTarjeta;
	private Tipo_tarjeta tipoTarjeta;
	private static JComboBox<Tipo_tarjeta> cbTarjeta;
	private int codigoPago;
	private int idTarjeta;
	private String nombreTarjeta;
	private JLabel lblTienda;		
	private JLabel lblProducto;
	private static JTextField txtProducto;
	private static JTextField txtCoste;
	private JButton btnMenu;
	private JButton btnGuardar;
	private JButton btnOtroMas;
	private int cont=0;
	private int id;
	private static JDateChooser dateChooser;
	private Compra compra; // Objeto para el arraylist
	private Compras gestorCompras=new Compras(); //arraylist
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;	
		
		// Atributos del resultados user
		private String resultFecha;
		private String resultTipoCompra;
		private String resultTipoTienda;
		private String resultTienda;
		private String resultProducto;
		private double resultCoste;
		private String resultFormaPago;
		private String resultTarjeta;
	
	public AnadirCompra(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 699, 370); // tama�o
		setTitle("A�adir compra"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_compra;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoCompra=rs.getInt("idCompra");
				tipo=rs.getString("tipo");
				
				// Se instancia la clase Tipo_compra
				tipoCompra = new Tipo_compra (idTipoCompra,tipo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoCompra.addItem(tipoCompra);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoTienda=rs.getInt("idTipo");
				tipoTiendaS=rs.getString("tipo");
				
				// Se instancia la clase Tipo_tienda
				tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoTienda.addItem(tipoTienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			limpiar();
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 316, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha compra:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(48, 81, 115, 14);
		contentPane.add(lblFecha);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(159, 81, 164, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		lblTipoCompra = new JLabel("Forma compra:");
		lblTipoCompra.setForeground(new Color(0, 139, 139));
		lblTipoCompra.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoCompra.setBounds(364, 81, 126, 14);
		contentPane.add(lblTipoCompra);
		
		Tipo_compra tipo=new Tipo_compra("");	
		cbTipoCompra = new JComboBox<Tipo_compra>(); // Muestra la lista de la clase Tipo compra desde la BD
		cbTipoCompra.addItem(tipo);
		cbTipoCompra.addActionListener((ActionListener)this);
		cbTipoCompra.setBounds(463, 81, 164, 20);
		contentPane.add(cbTipoCompra);
		
		JLabel lblTipoTienda = new JLabel("Tipo tienda:");
		lblTipoTienda.setForeground(new Color(0, 139, 139));
		lblTipoTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoTienda.setBounds(48, 126, 88, 14);
		contentPane.add(lblTipoTienda);
		
		Tipo_tienda tipoTienda=new Tipo_tienda(" ");	
		cbTipoTienda = new JComboBox<Tipo_tienda>();
		cbTipoTienda.addItem(tipoTienda);		
		cbTipoTienda.addActionListener((ActionListener)this);
		cbTipoTienda.setBounds(159, 122, 164, 20);
		contentPane.add(cbTipoTienda);
		
		lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(363, 126, 88, 14);
		contentPane.add(lblTienda);
		
		Tienda tienda=new Tienda(" ");	
		cbTienda = new JComboBox<Tienda>();
		cbTienda.addItem(tienda);
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(463, 122, 164, 20);
		contentPane.add(cbTienda);
		
		lblProducto = new JLabel("Producto:");
		lblProducto.setForeground(new Color(0, 139, 139));
		lblProducto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProducto.setBounds(48, 175, 95, 14);
		contentPane.add(lblProducto);
		
		txtProducto = new JTextField();
		txtProducto.addActionListener((ActionListener)this);
		txtProducto.setColumns(10);
		txtProducto.setBounds(159, 171, 164, 20);
		contentPane.add(txtProducto);
		
		JLabel lblCoste = new JLabel("Importe:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(365, 175, 95, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.addActionListener((ActionListener)this);
		txtCoste.setColumns(10);
		txtCoste.setBounds(463, 171, 164, 20);
		contentPane.add(txtCoste);
		
		JLabel lblFormaPago = new JLabel("Forma pago:");
		lblFormaPago.setForeground(new Color(0, 139, 139));
		lblFormaPago.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFormaPago.setBounds(48, 216, 95, 14);
		contentPane.add(lblFormaPago);
		
		FormaPago formaPago=new FormaPago(" ");	
		cbFormaPago = new JComboBox<FormaPago>();
		cbFormaPago.addItem(formaPago);
		cbFormaPago.addActionListener((ActionListener)this);
		cbFormaPago.setBounds(159, 216, 164, 20);
		contentPane.add(cbFormaPago);
		
		lblTarjeta = new JLabel("Tarjeta:");
		lblTarjeta.setForeground(new Color(0, 139, 139));
		lblTarjeta.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTarjeta.setBounds(365, 216, 95, 14);
		contentPane.add(lblTarjeta);
		
		Tipo_tarjeta tarjeta=new Tipo_tarjeta(" ");	
		cbTarjeta = new JComboBox<Tipo_tarjeta>();
		cbTarjeta.addItem(tarjeta);
		cbTarjeta.addActionListener((ActionListener)this);
		cbTarjeta.setBounds(463, 212, 164, 20);
		contentPane.add(cbTarjeta);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(177, 274, 126, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);	
		
		btnOtroMas = new JButton("Otro");
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setBackground(new Color(184, 231, 255));
		btnOtroMas.setBounds(475, 274, 115, 41);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		contentPane.add(btnOtroMas);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(330, 274, 115, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
	} // Cierre componentes
	
	// Limpia los datos de los componentes
	private void limpiar(){
		cbTipoCompra.setSelectedItem(0);
		cbTipoTienda.setSelectedItem(0);
		cbTienda.setSelectedItem(null);
		txtProducto.setText("");
		txtCoste.setText("");
		cbFormaPago.setSelectedItem(null);
		cbTarjeta.setSelectedItem(null);
	} // Cierre limpiar
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado alguna opcion de "Tipo compra"
		if(evento.getSource()==cbTipoCompra){
			if(cbTipoCompra.isValid()==true){
				// Guarda el valor de item seleccionado "Tipo compra"
				resultTipoCompra=cbTipoCompra.getSelectedItem().toString();
				// Limpia los anteriores items de la lista desplegable
				cbFormaPago.removeAllItems();
				cbTarjeta.removeAllItems();			
				try {
					// Sentencia SELECT
					sql = "SELECT * FROM tipo_compra WHERE tipo='"+resultTipoCompra+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						// Se obtiene el codigo de tipo de compra
						codigoCompra = rs.getInt("idCompra");
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					
					// Sentencia SELECT
					sql = "SELECT * FROM forma_pago WHERE idTipoCompra="+codigoCompra+";";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						
						//Obtienen los datos
						idFormaPago=rs.getInt("idFormaPago");
						tipoPago=rs.getString("tipo_pago");
						
						// Se instancia la clase 
						formaPago = new FormaPago (idFormaPago,tipoPago); 
						
						//Se a�ade cada objeto al JComboBox
						cbFormaPago.addItem(formaPago);
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
			}
		}
		// Si ha seleccionado alguna opcion de "Tipo tienda"
		if(evento.getSource()==cbTipoTienda){
			if(cbTipoTienda.isValid()==true){
				// Guarda el valor de item seleccionado "Tipo tienda"
				resultTipoTienda=cbTipoTienda.getSelectedItem().toString();
				// Limpia los anteriores items de la lista desplegable
				cbTienda.removeAllItems();
				try {
					// Sentencia SELECT
					sql = "SELECT * FROM tipo_tienda WHERE tipo='"+resultTipoTienda+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						// Se obtiene el codigo de tipo de tienda
						codigoTienda = rs.getInt("idTipo");
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					
					// Sentencia SELECT
					sql = "SELECT * FROM tiendas WHERE idTipo="+codigoTienda+";";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						
						//Obtienen los datos
						idTienda=rs.getInt("idTienda");
						nombreTienda=rs.getString("nombre");
						
						// Se instancia la clase Tipo_tienda
						tienda = new Tienda (idTienda,nombreTienda); 
						
						//Se a�ade cada objeto al JComboBox
						cbTienda.addItem(tienda);
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					resultTienda=cbTienda.getSelectedItem().toString();
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
				
			}
		}
		// Si ha seleccionado alguna opcion de "Forma de pago"
		if(evento.getSource()==cbFormaPago){
			if(cbFormaPago.isValid()==true){ //si es v�lido JComboBox
				if(cbFormaPago.getSelectedItem()!=null){
					cbTarjeta.removeAllItems();
					try {
						// Guarda el valor de item seleccionado "Forma de pago"
						resultFormaPago=cbFormaPago.getSelectedItem().toString();
						// Sentencia SELECT
						sql = "SELECT * FROM forma_pago WHERE tipo_pago='"+resultFormaPago+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo
							codigoPago = rs.getInt("idFormaPago");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tarjetas WHERE idTipoTarjeta="+codigoPago+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTarjeta=rs.getInt("idTarjeta");
							nombreTarjeta=rs.getString("nombre");

							// Se instancia la clase Tipo_tienda
							tipoTarjeta = new Tipo_tarjeta (idTarjeta,nombreTarjeta); 
							
							//Se a�ade cada objeto al JComboBox
							cbTarjeta.addItem(tipoTarjeta);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}	
				// Limpia los anteriores items de la lista desplegable
				//cbTarjeta.removeAllItems();
			} // Cierre if			
		}
		// Si hace clic en Otro, se van a�adiendo registros al array hasta que da al Menu
		if(evento.getSource()==btnOtroMas){
			
			boolean validar=false;			
			try{
				validar=Utilidades.validarCamposCompra(dateChooser.getDate().toString(),cbTipoCompra.getSelectedItem().toString(),cbTipoTienda.getSelectedItem().toString(),cbTienda.getSelectedItem().toString(),txtProducto.getText(),txtCoste.getText(),cbFormaPago.getSelectedItem().toString(),cbTarjeta.getSelectedItem().toString(),getTxtProducto(),getTxtCoste());
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{				
				try {				
					// Sentencia SQL SELECT
					sql = "SELECT * FROM compras;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Se a�aden los datos de cada componente al arraylist
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana
						resultTienda=cbTienda.getSelectedItem().toString().trim();
						resultProducto=txtProducto.getText().trim();
						if(Utilidades.esNumero(txtCoste.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtCoste.getText())==true){
							resultCoste=Double.parseDouble(txtCoste.getText());
							resultTarjeta=cbTarjeta.getSelectedItem().toString().trim();
							// Se guardan los datos en fichero log
							Utilidades.comprasLog("A�adir",id,resultFecha,resultTipoTienda,resultTienda,txtCoste.getText(),resultFormaPago,resultProducto);
							//Se crea un nuevo objeto
							compra=new Compra(id,resultFecha,resultTipoCompra,resultTipoTienda,resultTienda,resultProducto,resultCoste,resultFormaPago,resultTarjeta);					
							// Se a�ade cada objeto al arraylist
							gestorCompras.anadir(compra);
							// Se limpian los componentes
							limpiar();
						}
					}
					else{
						cont++;
						// Cuando cont sea igual a 1 se obtiene la ID de la BD
						if(cont==1){
							// Se obtiene el ultimo ID desde la BD
							id=gestorCompras.ultimoID();
						}					
						id++;
						// Obtener los datos de cada componente de la ventana
						resultTienda=cbTienda.getSelectedItem().toString().trim();
						resultProducto=txtProducto.getText().trim();
						if(Utilidades.esNumero(txtCoste.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtCoste.getText())==true){
							resultCoste=Double.parseDouble(txtCoste.getText());
							resultTarjeta=cbTarjeta.getSelectedItem().toString().trim();
							// Se guardan los datos en fichero log
							Utilidades.comprasLog("A�adir",id,resultFecha,resultTipoTienda,resultTienda,txtCoste.getText(),resultFormaPago,resultProducto);
							//Se crea un nuevo objeto
							compra=new Compra(id,resultFecha,resultTipoCompra,resultTipoTienda,resultTienda,resultProducto,resultCoste,resultFormaPago,resultTarjeta);					
							// Se a�ade cada objeto al arraylist
							gestorCompras.anadir(compra);
							// Se limpian los componentes
							limpiar();
						}
						
					} // Cierre del else				
				}
				catch (SQLException e) {
					// Muestra un error
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		// Va a la ventana principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			// Desparece la ventana a�adir Gastos de Tienda
			setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnGuardar){
			
			boolean validar=false;			
			try{
				validar=Utilidades.validarCamposCompra(dateChooser.getDate().toString(),cbTipoCompra.getSelectedItem().toString(),cbTipoTienda.getSelectedItem().toString(),cbTienda.getSelectedItem().toString(),txtProducto.getText(),txtCoste.getText(),cbFormaPago.getSelectedItem().toString(),cbTarjeta.getSelectedItem().toString(),txtProducto, txtCoste);
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{
				try {					
					cont++;
					// Cuando cont sea igual a 1 se obtiene la ID de la BD
					if(cont==1){
						// Se obtiene el ultimo ID desde la BD
						id=gestorCompras.ultimoID();
					}
					id++;
					// Obtener los datos de cada componente de la ventana
					resultTienda=cbTienda.getSelectedItem().toString().trim();
					resultProducto=txtProducto.getText().trim();
					if(Utilidades.esNumero(txtCoste.getText())==false){
						JOptionPane.showMessageDialog(null, "El 'Importe' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
					}
					else if(Utilidades.esNumero(txtCoste.getText())==true){
						resultCoste=Double.parseDouble(txtCoste.getText());
						resultTarjeta=cbTarjeta.getSelectedItem().toString().trim();
						if(formaPago==null){
							JOptionPane.showMessageDialog(null, "Seleccione la forma de pago");
						}					
						//Se crea un nuevo objeto
						compra=new Compra(id,resultFecha,resultTipoCompra,resultTipoTienda,resultTienda,resultProducto,resultCoste,resultFormaPago,resultTarjeta);				
						// Se a�ade cada objeto al arraylist
						gestorCompras.anadir(compra);
						// Inserta los datos del array en la BD
						if(gestorCompras.insertarBD()==true){
							// Se guardan los datos en fichero log
							Utilidades.comprasLog("A�adir",id,resultFecha,resultTipoTienda,resultTienda,txtCoste.getText(),resultFormaPago, resultProducto);
							// Muestra un aviso para el usuario
							JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
							// Se instancia el controlador Principal
							ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
							// Desparece la ventana a�adir Gastos de Tienda
							setVisible(false);
						}				
					}
				}
				catch (SQLException e) {
					// Muestra un error
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
				}
			} // else
		}
		
	} // Cierre actionPerformed
	
	// getters
	public static JTextField getTxtProducto() {
		return txtProducto;
	}

	public static JTextField getTxtCoste() {
		return txtCoste;
	}
	
} // Cierre de clase