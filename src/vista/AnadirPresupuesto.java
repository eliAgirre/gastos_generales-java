package vista;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import utilidades.BD;
import utilidades.Utilidades;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class AnadirPresupuesto extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JDateChooser dateChooser;
	private JTextField txtPresu;
	private JButton btnMenu;
	private JRadioButton rbSemanal;
	private JRadioButton rbMensual;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private int id=0;
	private String fecha;
	private double presupuesto;
	private String tipo;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
		private boolean tablaVacia;
		private JButton btnGuardar;
	
	public AnadirPresupuesto(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 300, 381); // tama�o
		setTitle("A�adir presupuesto"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 327, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 182, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(31, 81, 63, 14);
		contentPane.add(lblFecha);
		
		JLabel lblPresu = new JLabel("Presupuesto:");
		lblPresu.setForeground(new Color(0, 139, 139));
		lblPresu.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPresu.setBounds(31, 133, 89, 14);
		contentPane.add(lblPresu);
		
		txtPresu = new JTextField();
		txtPresu.setColumns(10);
		txtPresu.setBounds(130, 133, 126, 20);
		contentPane.add(txtPresu);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setForeground(new Color(0, 139, 139));
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipo.setBounds(31, 196, 89, 14);
		contentPane.add(lblTipo);
		
		rbSemanal = new JRadioButton("Semanal");
		rbSemanal.setSelected(true);
		rbSemanal.addActionListener((ActionListener)this);
		rbSemanal.setBounds(130, 180, 109, 23);
		buttonGroup.add(rbSemanal);
		contentPane.add(rbSemanal);
		
		rbMensual = new JRadioButton("Mensual");
		rbMensual.addActionListener((ActionListener)this);
		rbMensual.setBounds(130, 216, 109, 23);
		buttonGroup.add(rbMensual);
		contentPane.add(rbMensual);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(35, 271, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);	
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(130, 81, 126, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						fecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(153, 271, 103, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado radio "Semanal"
		if(evento.getSource()==rbSemanal){
			// Se establece el valor del texto de la radio
			tipo=rbSemanal.getText();			
		}
		// Si ha seleccionado radio "Mensual"
		if(evento.getSource()==rbMensual){
			// Se establece el valor del texto de la radio
			tipo=rbMensual.getText();			
		}
		
		// Va a la ventana principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
			ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			// Desparece la ventana a�adir Gastos de Tienda
			setVisible(false);
		}
		// Si hace clic en Guardar se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnGuardar){
			try {
				boolean validar=false;			
				try{
					validar=Utilidades.validarCamposPresu(dateChooser.getDate().toString(), txtPresu.getText(), getDateChooser(), getTxtPresu());
				}catch(NullPointerException npe){ // Muestra un error
					JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
				}
				
				if(validar==false){}
				else{
					// Sentencia SQL SELECT
					sql = "SELECT * FROM presupuestos;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Si la tabla esta vacia
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana y se comprueba
						if(Utilidades.esNumero(txtPresu.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Presupuesto' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtPresu.getText())==true){
							presupuesto=Double.parseDouble(txtPresu.getText().trim());
							// Si el usuario  ha dejado por defecto el radio button
							if(tipo==null){
								// Se establece "Semanal" el valor de la variable
								tipo="Semanal";
							}
							//Sentencia INSERT
							sql = "INSERT INTO presupuestos VALUES ("+id+",'"+fecha+"',"+presupuesto+",'"+tipo+"');";
							// Inserta los datos en la BD
							if(BD.actualizar(sql)==true){
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
								// Se instancia el controlador Principal
						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
						  		// Desparece la ventana a�adir Gastos de Tienda
						  		setVisible(false);
							}
						}
					}
					else{ // Si la tabla contiene datos				
						// Sentencia SQL SELECT
						sql = "SELECT MAX(idPresu) FROM presupuestos;";
						// Ejecuta la consulta
						rs = BD.consulta(stmt, sql);
						// Si hay datos
						if (rs.next()){
							// Se obtiene el ultimo ID desde la BD
							id=rs.getInt(1);
				        }		
						rs.close(); //Cierre de la consulta
						id++;
						// Si el usuario  ha dejado por defecto el radio button
						if(tipo==null){
							// Se establece "Semanal" el valor de la variable
							tipo="Semanal";
						}
						// Obtener los datos de cada componente de la ventana y se comprueba
						if(Utilidades.esNumero(txtPresu.getText())==false){
							JOptionPane.showMessageDialog(null, "El 'Presupuesto' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
						}
						else if(Utilidades.esNumero(txtPresu.getText())==true){
							presupuesto=Double.parseDouble(txtPresu.getText().trim());
							//Sentencia INSERT
							sql = "INSERT INTO presupuestos VALUES ("+id+",'"+fecha+"',"+presupuesto+",'"+tipo+"');";
							// Inserta los datos en la BD
							if(BD.actualizar(sql)==true){
								// Muestra un aviso para el usuario
								JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
								// Se instancia el controlador Principal
						  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
						  		// Desparece la ventana a�adir Gastos de Tienda
						  		setVisible(false);
							}
						}
					} // Cierre del else
				}
			}
		  	catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}	
		}// Cierre evento btnMenu
	  		
	} // Cierre actionPerformed

	// Getters
	public JDateChooser getDateChooser() {
		return dateChooser;
	}
	public JTextField getTxtPresu() {
		return txtPresu;
	}
} // Cierre la clase