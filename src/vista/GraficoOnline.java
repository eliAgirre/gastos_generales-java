package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import com.toedter.calendar.JDateChooser;

import controlador.ControladorPrincipal;
import modelo.CosteTienda;
import utilidades.BD;

@SuppressWarnings("serial")
public class GraficoOnline extends JFrame implements ActionListener{

	// Atributos de la clase
	private JPanel MainPanel;
	private JPanel TopPanel;
	private DefaultCategoryDataset dataset=new DefaultCategoryDataset();
	private ArrayList <CosteTienda> valores=new ArrayList <CosteTienda>();
	private CosteTienda costeTienda;
	private double costeTotal;
	private String nombreTienda;
	private JDateChooser dateInicio;
	private JDateChooser dateFin;
	private JButton btnVer;
	private JButton btnMenu;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		private String resultFechaInicio;
		private String resultFechaFin;
		
	public GraficoOnline(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 691, 609); // tama�o
		setTitle("Grafico de Tarjeta NET"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/chart.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventan
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
	} // Constructor 
	
	private void componentes(){
		
		// Layouts
		MainPanel = new JPanel();
		MainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));		
		setContentPane(MainPanel);
		MainPanel.setLayout(null);
		
		TopPanel = new JPanel();
		TopPanel.setBounds(0, 84, 685, 432);
		TopPanel.setPreferredSize(new Dimension(600, 250));
		TopPanel.setBackground(Color.GRAY);
		MainPanel.add(TopPanel);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 555, 95, 14);
		MainPanel.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca las fechas, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 26, 254, 14);
		MainPanel.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(288, 27, 103, 14);
		MainPanel.add(lblFecha);
		
		dateInicio = new JDateChooser();
		dateInicio.setDateFormatString("dd/MM/yyyy");
		dateInicio.setBounds(372, 20, 126, 20);
		dateInicio.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(dateInicio.getDate());
	                }
				}
		    });
		MainPanel.add(dateInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaFin.setForeground(new Color(0, 139, 139));
		lblFechaFin.setBounds(288, 59, 95, 14);
		MainPanel.add(lblFechaFin);
		
		dateFin = new JDateChooser();
		dateFin.setDateFormatString("dd/MM/yyyy");
		dateFin.setBounds(372, 52, 126, 20);
		dateFin.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaFin = new SimpleDateFormat("yyyy-MM-dd").format(dateFin.getDate());
	                }
				}
		    });
		MainPanel.add(dateFin);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(524, 20, 126, 53);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/chart.png")));
		MainPanel.add(btnVer);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(288, 528, 126, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		MainPanel.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Ver" visualizara el grafico
		if(evento.getSource()==btnVer){
			try{
				// Sentencia SQL SELECT
				sql = "SELECT sum(coste),tienda FROM compras WHERE fecha BETWEEN '"+resultFechaInicio+"' AND '"+resultFechaFin+"' AND tarjeta='NET' group by tienda;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) {
					
					// Obtener los datos BD y guardar en los arrys
					costeTotal=rs.getDouble(1);
					nombreTienda=rs.getString(2);
					// Se crea el objeto
					costeTienda=new CosteTienda(costeTotal,nombreTienda);
					// Se a�ade al arraylist
					valores.add(costeTienda);
					
				} // Cierre de while			
				rs.close(); //Cierre de la consulta
				
				// Se recorre el arraylist
				for(int i=0;i<valores.size();i++){
					String numeros=Double.toString(valores.get(i).getCosteTotal())+" �";
					dataset.addValue(valores.get(i).getCosteTotal(),valores.get(i).getNombreTienda(),numeros);
				}
				// Establece los datos de la tabla
				JFreeChart chart=ChartFactory.createBarChart("Online", "Del "+resultFechaInicio+" al "+resultFechaFin, "", dataset,PlotOrientation.VERTICAL,true,true,false);
				// Category for bar chart
				CategoryPlot catPlot=chart.getCategoryPlot();
				catPlot.setRangeGridlinePaint(Color.BLACK);
				
				// Agrega chart al panel
				ChartPanel chartPanel=new ChartPanel(chart);
				TopPanel.removeAll();
				TopPanel.add(chartPanel, BorderLayout.CENTER);
				TopPanel.validate();
				resultFechaInicio="";
				resultFechaFin="";
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
			try{
				dateInicio.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
			try{
				dateFin.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
	} // Cierre del metodo actionPerformed	
} // Cierre clase