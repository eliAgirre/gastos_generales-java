package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import modelo.ComboFijos;
import utilidades.BD;

import java.awt.Font;

@SuppressWarnings("serial")
public class VerGastosFijos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private ComboFijos gastosFijos;
	private JComboBox<ComboFijos> cbTipoGasto;
	private int idGastoFijo;
	private String nombreFijo;
	private JButton btnVer;
	private JLabel lblCosteTotal;
	private JLabel lblCantidad;	
	private JButton btnMenu;
	private JButton btnTodos;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"Fecha doc","N� Documento","Gasto fijo","Fecha inicio","Fecha fin","Duracion","Importe"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[7];
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
	
		// Atributos del resultados user
		private String resultTipoGasto;
		
	public VerGastosFijos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 789, 609); // tama�o
		setTitle("Gastos fijos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();		
			// Sentencia SQL SELECT
			sql = "SELECT * FROM gastos_fijos ORDER BY fecha_factura ASC, idGasto ASC;";
			// Comprueba si la tabla esta vacia
			tablaVacia=BD.tablaVacia(stmt,sql);
			// Se a�aden los datos de cada componente al arraylist
			if(tablaVacia==true){
				cbTipoGasto.setEnabled(false);
				btnVer.setEnabled(false);
				btnTodos.setEnabled(false);
				JOptionPane.showMessageDialog(null,	"La tabla de gastos fijos esta vacio.");
			}
			else{
				cbTipoGasto.setEnabled(true);
				btnVer.setEnabled(true);
				btnTodos.setEnabled(true);
			}
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan en el array los datos de BD
				datosBD[0]=rs.getString(2); //fecha factura
				datosBD[1]=rs.getString(3); //numero factura
				datosBD[2]=rs.getString(4); //tipo_gasto
				datosBD[3]=rs.getString(5); //fecha_inicio
				datosBD[4]=rs.getString(6); //fecha_fin
				datosBD[5]=rs.getString(7); //duracion
				datosBD[6]=Double.toString(rs.getDouble(8))+" �"; //coste
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
			
			} // Cierre de while
				
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM fijos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idGastoFijo=rs.getInt("idFijos");
				nombreFijo=rs.getString("nombre");
				
				// Se instancia la clase Tipo_compra
				gastosFijos = new ComboFijos (idGastoFijo,nombreFijo); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoGasto.addItem(gastosFijos);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 153, 732, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 554, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Puede seleccionar para ver algun gasto fijo:");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setBounds(32, 30, 268, 20);
		contentPane.add(lblAviso);
		
		cbTipoGasto = new JComboBox<ComboFijos>();
		cbTipoGasto.addActionListener((ActionListener)this);
		cbTipoGasto.setBounds(247, 78, 141, 20);
		contentPane.add(cbTipoGasto);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(496, 68, 110, 41);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		btnTodos = new JButton("Todos");
		btnTodos.addActionListener((ActionListener)this);
		btnTodos.setBackground(new Color(184, 231, 255));
		btnTodos.setBounds(647, 68, 109, 41);
		btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/all.png")));
		contentPane.add(btnTodos);
		
		lblCosteTotal = new JLabel("Total:");
		lblCosteTotal.setVisible(false);
		lblCosteTotal.setForeground(new Color(0, 139, 139));
		lblCosteTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCosteTotal.setBounds(630, 481, 50, 14);
		contentPane.add(lblCosteTotal);
		
		lblCantidad= new JLabel();
		lblCantidad.setVisible(false);
		lblCantidad.setForeground(Color.BLACK);
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantidad.setBounds(678, 481, 78, 14);
		contentPane.add(lblCantidad);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(344, 528, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);

		
	} // Cierre componentes
	
	// Metodo para limpiar los datos de la tabla.
	public void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en boton "Ver"
		if(evento.getSource()==btnVer){
			limpiarTabla();
			if(cbTipoGasto.isValid()==true){ // Se valida JComboBox
				if(cbTipoGasto.getSelectedItem()!=null){ // Si lo seleccionado no es nulo
					try{
						// Se guarda lo seleccionado en una variable
						resultTipoGasto=cbTipoGasto.getSelectedItem().toString();
						//Sentencia SELECT para obtener la duracion del gasto fijo
						sql="SELECT * FROM gastos_fijos WHERE tipo_gasto='"+resultTipoGasto+"' ORDER BY fecha_factura ASC, idGasto ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							// Se guardan en el array los datos de BD
							datosBD[0]=rs.getString(2); //fecha factura
							datosBD[1]=rs.getString(3); //numero factura
							datosBD[2]=rs.getString(4); //tipo_gasto
							datosBD[3]=rs.getString(5); //fecha_inicio
							datosBD[4]=rs.getString(6); //fecha_fin
							datosBD[5]=rs.getString(7); //duracion
							datosBD[6]=Double.toString(rs.getDouble(8))+" �"; //coste
							// Se a�ade cada fila al modelo de tabla
							modelo.addRow(datosBD);
						
						} // Cierre de while
							
						rs.close(); //Cierre de la consulta
						cbTipoGasto.setSelectedIndex(0);
						
						lblCosteTotal.setVisible(true);
						lblCantidad.setVisible(true);
						sql="SELECT sum(importe) FROM gastos_fijos WHERE tipo_gasto='"+resultTipoGasto+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 						
							lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
						} // Cierre de while
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}
			}
		}
		// Si hace clic en boton "Todos"
		if(evento.getSource()==btnTodos){
			lblCosteTotal.setVisible(false);
			lblCantidad.setVisible(false);
			lblCantidad.setText("");
			limpiarTabla();
			try{
				//Sentencia SELECT para obtener la duracion del gasto fijo
				sql="SELECT * FROM gastos_fijos ORDER BY fecha_factura ASC, idGasto ASC;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan en el array los datos de BD
					datosBD[0]=rs.getString(2); //fecha factura
					datosBD[1]=rs.getString(3); //numero factura
					datosBD[2]=rs.getString(4); //tipo_gasto
					datosBD[3]=rs.getString(5); //fecha_inicio
					datosBD[4]=rs.getString(6); //fecha_fin
					datosBD[5]=rs.getString(7); //duracion
					datosBD[6]=Double.toString(rs.getDouble(8))+" �"; //coste
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
				
				} // Cierre de while
					
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en "Menu" se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){		
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		
	} // Cierre actionPerformed
} // Cierre clase