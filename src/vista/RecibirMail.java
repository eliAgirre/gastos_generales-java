package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;

@SuppressWarnings("serial")
public class RecibirMail extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JMenuItem btnRevisarCorreo;
	private JMenuItem btnEnviarCorreo;
	private JMenuItem btnSalir;
	private JTabbedPane tabPanel;
	private ArrayList<PanelBandeja> bandejas=new ArrayList<PanelBandeja>();
	private String hostcorreo="Gmail";	
	
	public RecibirMail(){
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(400, 200, 436, 435); // size
		setTitle("Recibir emails"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/mail.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		componentes();
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//contenedor de menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 24);
		contentPane.add(menuBar);
		
		btnRevisarCorreo = new JMenuItem("Revisar");
		btnRevisarCorreo.addActionListener((ActionListener) this);
		btnRevisarCorreo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/review.png")));
		menuBar.add(btnRevisarCorreo);
		
		btnEnviarCorreo = new JMenuItem("Enviar");
		btnEnviarCorreo.addActionListener((ActionListener) this);
		btnEnviarCorreo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/send-small.png")));
		menuBar.add(btnEnviarCorreo);		
		
		btnSalir = new JMenuItem("Salir");
		btnSalir.addActionListener((ActionListener) this);
		btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/exit.png")));
		menuBar.add(btnSalir);
		
		tabPanel = new JTabbedPane();
		tabPanel.setBounds(0, 24, 434, 384);
		contentPane.add(tabPanel);
		
	} // Componentes

	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Revisar" visualiza los mensajes no leidos
		if(evento.getSource()==btnRevisarCorreo){
			PanelBandeja panelBandeja=new PanelBandeja(RecibirMail.this);
			bandejas.add(panelBandeja);
			tabPanel.addTab(hostcorreo, panelBandeja.getContenedor());
			panelBandeja.getContenedor().repaint();
			repaint();
		}
		// Si hace clic en "Enviar" podra escribir un correo
		if(evento.getSource()==btnEnviarCorreo){
			// Se instancia la ventana correspondiente
			EnviarMail ventanaCrearMail=new EnviarMail();
			// Coloca la ventana en el centro de la pantalla
			ventanaCrearMail.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaCrearMail.setVisible(true);
	  		// Deaparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en "Salir", ira a la ventana principal
		if(evento.getSource()==btnSalir){
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		
	} // actionPerformed
}