package vista;

import java.awt.Color;
//import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import modelo.Ahorro;
import utilidades.BD;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.toedter.calendar.JDateChooser;


@SuppressWarnings("serial")
public class VerSemanal extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private Double presu=0.0;
	private Double gastos=0.0;
	private float ahorro=(float) 0.0;
	private int id=0;
	private JDateChooser dateInicio;
	private JDateChooser dateFin;
	private JButton btnGuardar;
	private JButton btnCalcular;
	private JButton btnConsultar;	
	private JButton btnMenu;
	private JButton btnPDF;
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	private static Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
	private Font avisoFont=new Font();
	private Font avisoFechas=new Font();
	private Ahorro ahorroModel;
	private ArrayList<Ahorro> arrayPDF=new ArrayList<Ahorro>();
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
		
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"Fecha inicio","Fecha fin","Presupuesto","Gasto","Ahorro"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static Object[] datos=new Object[5];
		
		// Atributos del resultados user
		private String resultFechaInicio;
		private String resultFechaFin;
	
	public VerSemanal(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(0, -29, 554, 598); // tama�o
		setTitle("Ahorro semanal"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/coins.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM semanal;";
			// Comprueba si la tabla esta vacia
			tablaVacia=BD.tablaVacia(stmt,sql);
			// Si la tabla esta vacia
			if(tablaVacia==true){
				// No podra consultar
				btnConsultar.setEnabled(false);
			}
			else{
				// Podra consultar los ahorros semanales de la BD
				btnConsultar.setEnabled(true);
			}
			// Se instancia la clase Calendar
			Calendar calendario = Calendar.getInstance();
			// Se obtiene el dia de la semana
			int dayWeek=calendario.get(Calendar.DAY_OF_WEEK);
			// Si el dia de la semana es 1 (domingo)
			if(dayWeek==1){
				// Se permite pulsar el boton
				btnGuardar.setEnabled(true);
			}
			else{
				// No se permite pulsar el boton
				btnGuardar.setEnabled(false);
			}
			// No se permite pulsar el boton
			btnPDF.setEnabled(false);
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre del constructor
	
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(61, 199, 462, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		tabla.setDefaultRenderer(Object.class, new MiRender()); // Para editar el color del texto de la tabla
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 544, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Puedes ver el ahorro semanal.");
		//lblAviso.setFont(new Font("Arial", Font.BOLD, 12));	
		avisoFont.setFamily("Arial");
		avisoFont.setStyle(1);
		avisoFont.setSize(12);
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFechaInicio = new JLabel("Fecha inicio:");
		lblFechaInicio.setForeground(new Color(0, 139, 139));
		//lblFechaInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblFechaInicio.setBounds(61, 82, 111, 20);
		contentPane.add(lblFechaInicio);
		
		dateInicio = new JDateChooser();
		dateInicio.setDateFormatString("dd/MM/yyyy");
		dateInicio.setBounds(159, 82, 126, 20);
		dateInicio.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(dateInicio.getDate());
	                }
				}
		    });
		contentPane.add(dateInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setForeground(new Color(0, 139, 139));
		//lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblFechaFin.setBounds(61, 119, 111, 20);
		contentPane.add(lblFechaFin);
		
		dateFin = new JDateChooser();
		dateFin.setDateFormatString("dd/MM/yyyy");
		dateFin.setBounds(159, 119, 126, 20);
		dateFin.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaFin = new SimpleDateFormat("yyyy-MM-dd").format(dateFin.getDate());
	                }
				}
		    });
		contentPane.add(dateFin);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(325, 49, 137, 33);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener((ActionListener)this);
		btnCalcular.setBackground(new Color(184, 231, 255));
		btnCalcular.setBounds(325, 93, 137, 33);
		btnCalcular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/calc.png")));
		contentPane.add(btnCalcular);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener((ActionListener)this);
		btnConsultar.setBackground(new Color(184, 231, 255));
		btnConsultar.setBounds(325, 137, 137, 33);
		btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnConsultar);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(159, 525, 126, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		btnPDF = new JButton("Crear");
		btnPDF.addActionListener((ActionListener)this);
		btnPDF.setBackground(new Color(184, 231, 255));
		btnPDF.setBounds(325, 525, 137, 33);
		btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/pdf.png")));
		contentPane.add(btnPDF);
		
	} // Cierre componentes
	
	// Metodo para limpiar los datos de la tabla.
	public void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	public static void agregarTitulo(Document documento) throws DocumentException{
		
		// Se crea el objeto Pararagrap
		Paragraph parrafo=new Paragraph();
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se crea una tabla de una celda
		PdfPTable tabla=new PdfPTable(1);
		// Se crea una celda con una frase
		PdfPCell celda = new PdfPCell(new Phrase("Ahorro semanal",titleFont));
		// Se escribe en el centro
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		// No tiene borde
		celda.setBorder(Rectangle.NO_BORDER);
		// Se agrega la celda a la tabla
		tabla.addCell(celda);
		// Se agrega la tabla al documento
		documento.add(tabla);		
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se agregan los parrafos al documento
		documento.add(parrafo);
		
	} // agregarTitulo
	
	public static void agregarTabla(Document documento, String[] header, ArrayList<Ahorro> datos) throws DocumentException{
		
		// Se crea una tabla de 5 columnas
		PdfPTable tabla=new PdfPTable(5);
	    
	    for(int i=0;i<header.length;i++){
	    	// Se crea la celda cabecera
	    	PdfPCell cellHead = new PdfPCell(new Phrase(header[i],headerFont));
	    	// Se escribe en el centro
	    	cellHead.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	// Se agrega la celda a la tabla
		    tabla.addCell(cellHead);
	    }
	    
	    // Se agrega la cabecera a la tabla
	    tabla.setHeaderRows(1);
	    
	    // Se agragan cada celda con algun dato a la tabla
	    for(int i=0;i<datos.size();i++){
	
	    	tabla.addCell(datos.get(i).getFechaInicio()); // fecha_inicio
		    tabla.addCell(datos.get(i).getFechaFin()); // fecha_fin
		    tabla.addCell(Double.toString(datos.get(i).getPresu())); // presu
		    tabla.addCell(Double.toString(datos.get(i).getGasto())); // gasto
		    tabla.addCell(Double.toString(datos.get(i).getAhorro())); // ahorro
	    }
	    
		// Se agrega la tabla al documento
	 	documento.add(tabla);
	    
		
	} // AgregarContenido
	
	private static void agregarSaltoLinea(Paragraph paragraph, int number) {
		
	    for (int i=0;i<number;i++) {
	    	paragraph.add(new Paragraph(" "));
	    }
	} // agregarSaltoLinea
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		if(evento.getSource()==btnGuardar){
			limpiarTabla();
			try {
				// Sentencia SELECT presu
				sql = "SELECT cantidad FROM presupuestos WHERE fecha_presu='"+resultFechaInicio+"' and tipo='Semanal';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					// Se guardan los datos en array
					datos[0]=resultFechaInicio;
					datos[1]=resultFechaFin;
					datos[2]=Double.toString(rs.getDouble(1));
					presu=rs.getDouble(1);
				} // Cierre de while
				rs.close(); //Cierre de la consulta	
				
				// Sentencia SELECT gasto
				sql = "SELECT SUM(coste) FROM compras WHERE fecha BETWEEN '"+resultFechaInicio+"' AND '"+resultFechaFin+"';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					datos[3]=Double.toString(rs.getDouble(1));
					gastos=rs.getDouble(1);
					ahorro=(float)(presu-gastos); 					 
					datos[4]=ahorro;
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datos);
				} // Cierre de while
				rs.close(); //Cierre de la consulta
				
				// Sentencia SQL SELECT
				sql = "SELECT * FROM semanal;";
				// Comprueba si la tabla esta vacia
				tablaVacia=BD.tablaVacia(stmt,sql);
				// Si la tabla esta vacia
				if(tablaVacia==true){
					id++;					
					// Sentencia INSERT
					sql = "INSERT INTO semanal VALUES("+id+",'"+datos[0]+"','"+datos[1]+"',"+datos[2]+","+datos[3]+","+datos[4]+");";
					// Se insertan los datos en la BD
					BD.actualizar(sql);
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
				}
				else{
					
					// Sentencia SQL
					sql = "SELECT MAX(idSemanal) FROM semanal;";
					// Ejecuta la consulta
					rs = BD.consulta(stmt, sql);
					// Si hay datos
					if (rs.next()){
						// Se obtiene el ultimo ID desde la BD
						id=rs.getInt(1);
			        }					
					id++;
					// Sentencia INSERT
					sql = "INSERT INTO semanal VALUES("+id+",'"+datos[0]+"','"+datos[1]+"',"+datos[2]+","+datos[3]+","+datos[4]+");";
					// Se insertan los datos en la BD
					BD.actualizar(sql);
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");					
				} // Cierre del else
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
			// Limpia los componentes de la ventana
			resultFechaInicio="";
			resultFechaFin="";
			try{
				dateInicio.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
			try{
				dateFin.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}			
		}
		// Si hace clic en "Calcular", se obtiene un numero de ahorro
		if(evento.getSource()==btnCalcular){ 
			limpiarTabla();
			try {
				// Sentencia SELECT presu
				sql = "SELECT cantidad FROM presupuestos WHERE fecha_presu='"+resultFechaInicio+"' and tipo='Semanal';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					// Se guardan los datos en array
					datos[0]=resultFechaInicio;
					datos[1]=resultFechaFin;
					datos[2]=Double.toString(rs.getDouble(1));
					presu=rs.getDouble(1);
				} // Cierre de while
				rs.close(); //Cierre de la consulta
				
				// Sentencia SELECT compras
				sql = "SELECT SUM(coste) FROM compras WHERE fecha BETWEEN '"+resultFechaInicio+"' AND '"+resultFechaFin+"';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					datos[3]=Double.toString(rs.getDouble(1));
					gastos=rs.getDouble(1);
					ahorro=(float)(presu-gastos); 					 
					datos[4]=ahorro;
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datos);
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
			// Limpia los componentes de la ventana
			resultFechaInicio="";
			resultFechaFin="";
			try{
				dateInicio.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
			try{
				dateFin.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
		}
		// Si hace clic en "Consultar", consulta los ahorros que estan en la BD
		if(evento.getSource()==btnConsultar){
			limpiarTabla();
			try {
				// Sentencia SELECT
				//sql = "SELECT * FROM semanal WHERE fecha_inicio='"+resultFechaInicio+"' AND fecha_fin='"+resultFechaFin+"';";
				sql = "SELECT * FROM semanal;";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					
					// Se obtienen los datos
					datos[0]=rs.getString(2);
					datos[1]=rs.getString(3);
					datos[2]=Double.toString(rs.getDouble(4));
					datos[3]=Double.toString(rs.getDouble(5));
					ahorro=(float)rs.getDouble(6);
					datos[4]=ahorro;
					
					// Se crea el modelo de Ahorro
					ahorroModel=new Ahorro(rs.getString(2),rs.getString(3),rs.getDouble(4),rs.getDouble(5),rs.getDouble(6));
					
					// Se agrega al array
					arrayPDF.add(ahorroModel);
					
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datos);
					
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
				
				// Se permite pulsar el boton
				btnPDF.setEnabled(true);
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion	
			// Limpia los componentes de la ventana
			resultFechaInicio="";
			resultFechaFin="";
			try{
				dateInicio.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
			try{
				dateFin.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
		}
		// Si hace clic en Menu vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en "Crear" PDF, se crea pdf de ahorro semanal
		if(evento.getSource()==btnPDF){
			try {
				// Se crea el objeto Document
				Document documento=new Document();
				// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
				//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/ahorro_semanal.pdf"));
				PdfWriter.getInstance(documento, new FileOutputStream("archivos/ahorro_semanal.pdf"));
				// Se abre el documneto para despues escribir
				documento.open();
				// Se agrega el titulo
				agregarTitulo(documento);
				// Se agrega la tabla
				agregarTabla(documento,vector,arrayPDF);
				// Se cierra el documento
				documento.close();
				// Se cierra el documento
				documento.close();
				// Muestra al usuario el mensaje
				JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
				
			}
			catch (FileNotFoundException fnfe) {
				JOptionPane.showMessageDialog(null, fnfe.getMessage());
			} 
			catch (DocumentException de) {
				JOptionPane.showMessageDialog(null, de.getMessage());
			}
		}
	} // Cierre actionPerformed
} // Cierre de la clase