package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.BD;

@SuppressWarnings("serial")
public class VerPresupuestos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JRadioButton rbSemanal;
	private JRadioButton rbMensual;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnVer;
	private JButton btnTodos;
	private JButton btnMenu;
	private String tipo;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"Fecha","Cantidad","Presupuesto"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[3];
	
	public VerPresupuestos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 446, 544); // tama�o
		setTitle("Ver presupuestos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM presupuestos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			// Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan los datos en array datosBD
				String fecha=rs.getString(2);
				int pos=fecha.indexOf("-");
				String year=fecha.substring(0,pos);
				String mes=fecha.substring(pos+1,fecha.length());
				int pos2=mes.indexOf("-");
				mes=mes.substring(0,pos2);
				String dia=fecha.substring(pos2+pos+2,fecha.length());
				//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
				//datosBD[1]=rs.getString(2); //fecha
				datosBD[0]=dia+"/"+mes+"/"+year; //fecha
				datosBD[1]=Double.toString(rs.getDouble(3))+" �";
				datosBD[2]=rs.getString(4);
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre constructor
	
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(10, 138, 420, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Label + radio buttons + botones
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 490, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Seleccione una opcion a ver:");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(10, 35, 182, 14);
		contentPane.add(lblAviso);
		
		rbSemanal = new JRadioButton("Semanal");
		rbSemanal.setSelected(true);
		rbSemanal.addActionListener((ActionListener)this);
		rbSemanal.setBounds(144, 58, 109, 23);
		buttonGroup.add(rbSemanal);
		contentPane.add(rbSemanal);
		
		rbMensual = new JRadioButton("Mensual");
		rbMensual.addActionListener((ActionListener)this);
		rbMensual.setBounds(144, 86, 109, 23);
		buttonGroup.add(rbMensual);
		contentPane.add(rbMensual);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBounds(289, 35, 109, 41);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		btnTodos = new JButton("Todos");
		btnTodos.addActionListener((ActionListener)this);		
		btnTodos.setBackground(new Color(184, 231, 255));
		btnTodos.setBounds(289, 86, 109, 41);
		btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/all.png")));
		contentPane.add(btnTodos);
		
		// Boton menu
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(169, 471, 106, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	public void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla

	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si ha seleccionado radio "Semanal"
		if(evento.getSource()==rbSemanal){
			// Se establece el valor del texto de la radio
			tipo=rbSemanal.getText();
			
		}
		// Si ha seleccionado radio "Mensual"
		if(evento.getSource()==rbMensual){
			// Se establece el valor del texto de la radio
			tipo=rbMensual.getText();
			
		}
		// Si hace clic en Ver podra ver los presupuestos que haya seleccionado
		if(evento.getSource()==btnVer){ 
			// Limpia la tabla
			limpiarTabla();
			try {
				// Sentencia SQL SELECT
				sql = "SELECT * FROM presupuestos WHERE tipo='"+tipo+"';";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan los datos en array datosBD
					String fecha=rs.getString(2);
					int pos=fecha.indexOf("-");
					String year=fecha.substring(0,pos);
					String mes=fecha.substring(pos+1,fecha.length());
					int pos2=mes.indexOf("-");
					mes=mes.substring(0,pos2);
					String dia=fecha.substring(pos2+pos+2,fecha.length());
					//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
					//datosBD[1]=rs.getString(2); //fecha
					datosBD[0]=dia+"/"+mes+"/"+year; //fecha
					datosBD[1]=Double.toString(rs.getDouble(3))+" �";
					datosBD[2]=rs.getString(4);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
					
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en "Todos" se visualizan todos los presupuestos
		if(evento.getSource()==btnTodos){
			// Limpia la tabla
			limpiarTabla();
			// Se hace la conexion con la BD
			try {	
				// Sentencia SQL SELECT
				sql = "SELECT * FROM presupuestos;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan los datos en array datosBD
					String fecha=rs.getString(2);
					int pos=fecha.indexOf("-");
					String year=fecha.substring(0,pos);
					String mes=fecha.substring(pos+1,fecha.length());
					int pos2=mes.indexOf("-");
					mes=mes.substring(0,pos2);
					String dia=fecha.substring(pos2+pos+2,fecha.length());
					//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
					//datosBD[1]=rs.getString(2); //fecha
					datosBD[0]=dia+"/"+mes+"/"+year; //fecha
					datosBD[1]=Double.toString(rs.getDouble(3))+" �";
					datosBD[2]=rs.getString(4);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
					
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase