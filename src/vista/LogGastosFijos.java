package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;

@SuppressWarnings("serial")
public class LogGastosFijos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnMenu;
	
	public LogGastosFijos(){
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 862, 536); // tama�o
		setTitle("Fichero de gastos fijos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/file.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();	
		
		// Obtiene el fichero gastos.log
		//File file = new File("src/archivos/gastosFijos.log");
		File file = new File("archivos/gastosFijos.log");
		
		//Abre el fichero
		try{
		
			String texto="";		
			FileReader ficheroR=new FileReader(file);
			BufferedReader bR=new BufferedReader(ficheroR);
			StringBuffer buffer=new StringBuffer();
			texto=bR.readLine();
			
			// Mientras que el fichero contenga texto mostrara
			while(texto!= null){
				
				buffer.append(texto+ "\n"); // Se a�ade una linea de salto
				texto=bR.readLine();

			} // Cierre while
			textArea.append(buffer.toString()); //aparecer el contenido en el textArea
			bR.close();
		
		}
		catch (IOException eoi) {

			JOptionPane.showMessageDialog(null,"El fichero no esta bien.");
		}
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(35, 65, 789, 374);
		contentPane.add(scrollPane);
		
		// Label
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 482, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Todos los movimientos sobre los gastos fijos:");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(35, 22, 343, 14);
		contentPane.add(lblAviso);
		
		// Area de texto
		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		textArea.setFont(new java.awt.Font(null, 0, 12));
		
		// Boton
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		btnMenu.setBounds(385, 455, 95, 41);
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase