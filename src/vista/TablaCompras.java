package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import modelo.Tienda;
import modelo.Tipo_tienda;
import utilidades.BD;
import utilidades.Utilidades;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class TablaCompras extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private Tipo_tienda tipoTienda;
	private JComboBox<Tipo_tienda> cbTipoTienda;
	private int idTipoTienda;
	private String tipoTiendaS;
	private Tienda tienda;
	private JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private int codigoTienda;
	private JDateChooser dateChooser;
	private JButton btnBuscar;
	private JButton btnMenu;
	private JButton btnTodos;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"ID","Fecha","Compra","Tipo","Tienda","Producto","Coste","Pago", "Tarjeta"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[9];
		
		// Atributos para la siguiente ventana
		private String id;
		private String fecha;
		private String tiendaF;
		private String coste;
		private String formaPago;
		private String tarjeta;
		private String tipoCompra;
		private String tipoTiendaF;
		private String producto;
		
		// Atributos del resultados user
		private String resultTipoTienda;
		private String resultTienda;	
		private String resultFecha;
	
	public TablaCompras(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 673, 621); // tama�o
		setTitle("Compras a editar"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
			// Sentencia SQL SELECT
			sql = "SELECT * FROM compras ORDER BY fecha ASC, idCompra ASC;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan en el array los datos de BD
				datosBD[0]=Integer.toString(rs.getInt(1)); //id
				fecha=rs.getString(2);
				int pos=fecha.indexOf("-");
				String year=fecha.substring(0,pos);
				String mes=fecha.substring(pos+1,fecha.length());
				int pos2=mes.indexOf("-");
				mes=mes.substring(0,pos2);
				String dia=fecha.substring(pos2+pos+2,fecha.length());
				//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
				//datosBD[1]=rs.getString(2); //fecha
				datosBD[1]=dia+"/"+mes+"/"+year; //fecha
				datosBD[2]=rs.getString(3); //tipo_compra
				datosBD[3]=rs.getString(4); //tipo_tienda
				datosBD[4]=rs.getString(5); //tienda
				datosBD[5]=rs.getString(6); //producto
				datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
				datosBD[7]=rs.getString(8); //forma_pago
				datosBD[8]=rs.getString(9); //tarjeta
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
			
			} // Cierre de while
				
			rs.close(); //Cierre de la consulta
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoTienda=rs.getInt("idTipo");
				tipoTiendaS=rs.getString("tipo");
				
				// Se instancia la clase Tipo_tienda
				tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoTienda.addItem(tipoTienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 144, 628, 369);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se establece una anchura de las columnas
		tabla.getColumn("ID").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Fecha").setPreferredWidth(40); 
		//tabla.getColumn("Compra").setPreferredWidth(30);
		tabla.getColumn("Compra").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setPreferredWidth(0); // Se oculta la columna
		//tabla.getColumn("Tipo").setPreferredWidth(30);
		tabla.getColumn("Tipo").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Tienda").setPreferredWidth(40); 
		tabla.getColumn("Producto").setPreferredWidth(200);
		tabla.getColumn("Coste").setPreferredWidth(20);
		tabla.getColumn("Pago").setPreferredWidth(30);
		//tabla.getColumn("Tarjeta").setPreferredWidth(20);
		tabla.getColumn("Tarjeta").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(false);
		tabla.setBorder(null);
		// Se a�ade un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
			public void mouseClicked(MouseEvent evento) {
		    	
		    	String nl = System.getProperty("line.separator"); // Salto de linea
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
	        	id=(String)modelo.getValueAt(fila,columna);
		        fecha=(String) modelo.getValueAt(fila,1);
		        int pos=fecha.indexOf("/");
				String dia=fecha.substring(0,pos);
				String mes=fecha.substring(pos+1,fecha.length());
				int pos2=mes.indexOf("/");
				mes=mes.substring(0,pos2);
				String year=fecha.substring(pos2+pos+2,fecha.length());
				//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
				fecha=year+"-"+mes+"-"+dia;
		        tipoCompra=(String) modelo.getValueAt(fila,2);
		        tipoTiendaF=(String) modelo.getValueAt(fila,3);
		        tiendaF=(String) modelo.getValueAt(fila,4);
		        producto=(String) modelo.getValueAt(fila,5);
		        coste=(String) modelo.getValueAt(fila,6);
		        formaPago=(String) modelo.getValueAt(fila,7);
		        tarjeta=(String) modelo.getValueAt(fila,8);
		        JOptionPane.showMessageDialog(null, "Compra a editar: "+nl+nl+"   Dia: "+fecha+nl+"   Tienda: "+tiendaF+nl+"   Producto: "+producto+nl+"   Coste: "+coste+nl+" ");
		        EditarCompra editarCompra = new EditarCompra(id,fecha,tipoCompra, tipoTiendaF,tiendaF,producto,coste,formaPago,tarjeta);
				// Coloca la ventana en el centro de la pantalla
		        editarCompra.setLocationRelativeTo(null);
				// Hace visible la ventana
		        editarCompra.setVisible(true);
				setVisible(false);
				 
		    } // Cierre del mouseClicked
		});	
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 567, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Seleccione una compra a editar, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(24, 36, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblTipoTienda = new JLabel("Tipo tienda:");
		lblTipoTienda.setForeground(new Color(0, 139, 139));
		lblTipoTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipoTienda.setBounds(40, 101, 88, 14);
		contentPane.add(lblTipoTienda);
		
		Tipo_tienda tipo=new Tipo_tienda("");
		cbTipoTienda = new JComboBox<Tipo_tienda>();
		cbTipoTienda.addItem(tipo);
		cbTipoTienda.addActionListener((ActionListener)this);
		cbTipoTienda.setBounds(128, 98, 126, 20);
		contentPane.add(cbTipoTienda);
		
		JLabel lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(286, 98, 64, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tienda>();		
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(353, 98, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel label = new JLabel("Fecha:");
		label.setForeground(new Color(0, 139, 139));
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setBounds(286, 61, 46, 14);
		contentPane.add(label);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(353, 49, 126, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener((ActionListener)this);
		btnBuscar.setBackground(new Color(184, 231, 255));
		btnBuscar.setBounds(508, 82, 126, 41);
		btnBuscar.setBackground(new Color(184, 231, 255));
		btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/search.png")));
		contentPane.add(btnBuscar);
		
		btnTodos = new JButton("Todos");
		btnTodos.addActionListener((ActionListener)this);
		btnTodos.setBackground(new Color(184, 231, 255));
		btnTodos.setBounds(351, 540, 109, 41);
		btnTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/all.png")));
		contentPane.add(btnTodos);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(214, 540, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si ha seleccionado alguna opcion de "Tipo tienda"
		if(evento.getSource()==cbTipoTienda){
			if(cbTipoTienda.isValid()==true){
				// Guarda el valor de item seleccionado "Tipo tienda"
				resultTipoTienda=cbTipoTienda.getSelectedItem().toString();
				// Limpia los anteriores items de la lista desplegable
				cbTienda.removeAllItems();
				try {
					// Sentencia SELECT
					sql = "SELECT * FROM tipo_tienda WHERE tipo='"+resultTipoTienda+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						// Se obtiene el codigo de tipo de tienda
						codigoTienda = rs.getInt("idTipo");
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					
					// Sentencia SELECT
					sql = "SELECT * FROM tiendas WHERE idTipo="+codigoTienda+";";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					//Mientras que haya datos
					while (rs.next()) { 
						
						//Obtienen los datos
						idTienda=rs.getInt("idTienda");
						nombreTienda=rs.getString("nombre");
						
						// Se instancia la clase Tipo_tienda
						tienda = new Tienda (idTienda,nombreTienda); 
						
						//Se a�ade cada objeto al JComboBox
						cbTienda.addItem(tienda);
						
					} // Cierre de while					
					rs.close(); //Cierre de la consulta
					cbTienda.setSelectedItem(null);
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
			}
		}
		// Si hace clic en "Tienda"
		if(evento.getSource()==cbTienda){
			if(cbTienda.isValid()==true){
				// Guarda el valor de item seleccionado
				resultTienda=cbTienda.getSelectedItem().toString();	
			}
		}
		// Si hace clic en "Buscar"
		if(evento.getSource()==btnBuscar){ 
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			if(cbTienda.getSelectedItem()==null){
				//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoTienda);
				// Se hace la conexion con la BD
				if(resultTipoTienda!=null){
					if(resultFecha!=null){
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' and tipo_tienda='"+resultTipoTienda+"' ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=Integer.toString(rs.getInt(1)); //id
								fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								//datosBD[1]=rs.getString(2); //fecha
								datosBD[1]=dia+"/"+mes+"/"+year; //fecha
								//datosBD[1]=rs.getString(2); //fecha
								datosBD[2]=rs.getString(3); //tipo_compra
								datosBD[3]=rs.getString(4); //tipo_tienda
								datosBD[4]=rs.getString(5); //tienda
								datosBD[5]=rs.getString(6); //producto
								datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[7]=rs.getString(8); //forma_pago
								datosBD[8]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						cbTipoTienda.setSelectedIndex(0);
						resultFecha=null;
						try{
							dateChooser.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
					}else{ // Si ha elegido TipoTienda pero no la fecha
						
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE tipo_tienda='"+resultTipoTienda+"' ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								// Se guardan en el array los datos de BD
								datosBD[0]=Integer.toString(rs.getInt(1)); //id
								fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								//datosBD[1]=rs.getString(2); //fecha
								datosBD[1]=dia+"/"+mes+"/"+year; //fecha
								//datosBD[1]=rs.getString(2); //fecha
								datosBD[2]=rs.getString(3); //tipo_compra
								datosBD[3]=rs.getString(4); //tipo_tienda
								datosBD[4]=rs.getString(5); //tienda
								datosBD[5]=rs.getString(6); //producto
								datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
								datosBD[7]=rs.getString(8); //forma_pago
								datosBD[8]=rs.getString(9); //tarjeta
								// Se a�ade cada fila al modelo de tabla
								modelo.addRow(datosBD);
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						cbTipoTienda.setSelectedIndex(0);
						resultFecha=null;
						try{
							dateChooser.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
					}
				}
				else if(resultTipoTienda==null){
					try {		
						// Sentencia SQL SELECT
						sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' ORDER BY fecha ASC, idCompra ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 
							
							// Se guardan en el array los datos de BD
							datosBD[0]=Integer.toString(rs.getInt(1)); //id
							fecha=rs.getString(2);
							int pos=fecha.indexOf("-");
							String year=fecha.substring(0,pos);
							String mes=fecha.substring(pos+1,fecha.length());
							int pos2=mes.indexOf("-");
							mes=mes.substring(0,pos2);
							String dia=fecha.substring(pos2+pos+2,fecha.length());
							//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[1]=dia+"/"+mes+"/"+year; //fecha
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[2]=rs.getString(3); //tipo_compra
							datosBD[3]=rs.getString(4); //tipo_tienda
							datosBD[4]=rs.getString(5); //tienda
							datosBD[5]=rs.getString(6); //producto
							datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
							datosBD[7]=rs.getString(8); //forma_pago
							datosBD[8]=rs.getString(9); //tarjeta
							// Se a�ade cada fila al modelo de tabla
							modelo.addRow(datosBD);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
					// Limpia los componentes de la ventana
					cbTipoTienda.setSelectedIndex(0);
					resultFecha=null;
					try{
						dateChooser.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}
				}
			}
			else{
				if(resultFecha == null){
					// Se hace la conexion con la BD
					try {		
						// Sentencia SQL SELECT
						sql = "SELECT * FROM compras WHERE tipo_tienda='"+resultTipoTienda+"' and tienda='"+resultTienda+"' ORDER BY fecha ASC, idCompra ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 
							
							// Se guardan en el array los datos de BD
							datosBD[0]=Integer.toString(rs.getInt(1)); //id
							fecha=rs.getString(2);
							int pos=fecha.indexOf("-");
							String year=fecha.substring(0,pos);
							String mes=fecha.substring(pos+1,fecha.length());
							int pos2=mes.indexOf("-");
							mes=mes.substring(0,pos2);
							String dia=fecha.substring(pos2+pos+2,fecha.length());
							//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[1]=dia+"/"+mes+"/"+year; //fecha
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[2]=rs.getString(3); //tipo_compra
							datosBD[3]=rs.getString(4); //tipo_tienda
							datosBD[4]=rs.getString(5); //tienda
							datosBD[5]=rs.getString(6); //producto
							datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
							datosBD[7]=rs.getString(8); //forma_pago
							datosBD[8]=rs.getString(9); //tarjeta
							// Se a�ade cada fila al modelo de tabla
							modelo.addRow(datosBD);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
					
					// Limpia los componentes de la ventana
					resultTienda=null;
					cbTipoTienda.setSelectedIndex(0);
					cbTienda.setSelectedItem(null);
				}
				else{
					// Si la fecha no es null
					//JOptionPane.showMessageDialog(null, "tipo tienda: "+resultTipoTienda+" y tienda: "+resultTienda);
					// Se hace la conexion con la BD
					try {		
						// Sentencia SQL SELECT
						sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' and tipo_tienda='"+resultTipoTienda+"' and tienda='"+resultTienda+"' ORDER BY fecha ASC, idCompra ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 
							
							// Se guardan en el array los datos de BD
							datosBD[0]=Integer.toString(rs.getInt(1)); //id
							fecha=rs.getString(2);
							int pos=fecha.indexOf("-");
							String year=fecha.substring(0,pos);
							String mes=fecha.substring(pos+1,fecha.length());
							int pos2=mes.indexOf("-");
							mes=mes.substring(0,pos2);
							String dia=fecha.substring(pos2+pos+2,fecha.length());
							//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[1]=dia+"/"+mes+"/"+year; //fecha
							//datosBD[1]=rs.getString(2); //fecha
							datosBD[2]=rs.getString(3); //tipo_compra
							datosBD[3]=rs.getString(4); //tipo_tienda
							datosBD[4]=rs.getString(5); //tienda
							datosBD[5]=rs.getString(6); //producto
							datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
							datosBD[7]=rs.getString(8); //forma_pago
							datosBD[8]=rs.getString(9); //tarjeta
							// Se a�ade cada fila al modelo de tabla
							modelo.addRow(datosBD);
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
					
					// Limpia los componentes de la ventana
					resultTienda=null;
					cbTipoTienda.setSelectedIndex(0);
					cbTienda.setSelectedItem(null);	
					resultFecha=null;
					try{
						dateChooser.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}
				}
			}
		}
		// Si hace clic en "Todos" aparece los datos sin filtros
		if(evento.getSource()==btnTodos){
			// Limpia la tabla
			Utilidades.limpiarTabla(modelo);
			// Se hace la conexion con la BD
			try {
				// Sentencia SQL SELECT
				sql = "SELECT * FROM compras  ORDER BY fecha ASC, idCompra ASC;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan en el array los datos de BD
					datosBD[0]=Integer.toString(rs.getInt(1)); //id
					fecha=rs.getString(2);
					int pos=fecha.indexOf("-");
					String year=fecha.substring(0,pos);
					String mes=fecha.substring(pos+1,fecha.length());
					int pos2=mes.indexOf("-");
					mes=mes.substring(0,pos2);
					String dia=fecha.substring(pos2+pos+2,fecha.length());
					//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
					//datosBD[1]=rs.getString(2); //fecha
					datosBD[1]=dia+"/"+mes+"/"+year; //fecha
					//datosBD[1]=rs.getString(2); //fecha
					datosBD[2]=rs.getString(3); //tipo_compra
					datosBD[3]=rs.getString(4); //tipo_tienda
					datosBD[4]=rs.getString(5); //tienda
					datosBD[5]=rs.getString(6); //producto
					datosBD[6]=Double.toString(rs.getDouble(7))+" �"; //coste
					datosBD[7]=rs.getString(8); //forma_pago
					datosBD[8]=rs.getString(9); //tarjeta
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
				
				} // Cierre de while
					
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase