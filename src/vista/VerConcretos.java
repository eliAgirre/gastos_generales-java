package vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import controlador.ControladorPrincipal;

//import java.awt.Font;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.toedter.calendar.JDateChooser;

import modelo.*;
import utilidades.BD;

@SuppressWarnings("serial")
public class VerConcretos extends JFrame implements ActionListener {
	
	// Atributos de la clase
	private JPanel contentPane;
	private String[] listaOpciones={"1 Fecha", "Entre fechas","Tiendas"};
	private JComboBox<Object> cbOpciones;
	private JLabel lblFecha;
	private JDateChooser dateChooser1;
	private JLabel lblFechaFin;
	private JDateChooser dateChooser2;
	private Tipo_tienda tipoTienda;
	private JComboBox<Tipo_tienda> cbTipoTienda;
	private int idTipoTienda;
	private String tipoTiendaS;
	private int codigoTienda;
	private Tienda tiendaModel;
	private JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private JLabel lblTipoTienda;
	private JLabel lblTienda;
	private JLabel lblCosteTotal;
	private JLabel lblCantidad;
	private int i=0;
	private boolean check=false;
	private Compra compra;
	private ArrayList<Compra> arrayPDF=new ArrayList<Compra>();
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	private static Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 15,Font.ITALIC);
	private static Font cellHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
	private Font avisoFont=new Font();
	private Font avisoFechas=new Font();
	private String tipo="";
	private String tienda="";
	private JButton btnVer;
	private JButton btnMenu;
	private JButton btnPDF;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"ID","Fecha","Compra","Tipo","Tienda","Producto","Coste","Pago","Tarjeta"}; //cabecera
		private JTable tabla;
		private TableColumn tableColumn;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		private String resultOpcion;
		private static String resultFecha;
		private String resultFechaFin;
		private String resultTipoTienda;
		private String resultTienda;
	
	public VerConcretos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 707, 608); // tama�o
		setTitle("Gastos concretos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tipo_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, idCompra + tipo
				idTipoTienda=rs.getInt("idTipo");
				tipoTiendaS=rs.getString("tipo");
				
				// Se instancia la clase Tipo_tienda
				tipoTienda = new Tipo_tienda (idTipoTienda,tipoTiendaS); 
				
				//Se a�ade cada objeto al JComboBox
				cbTipoTienda.addItem(tipoTienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
			
			try{
				cbTipoTienda.setSelectedItem(null);
			}
			catch(NullPointerException npe){
				
			}
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);	
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 153, 622, 303);
		contentPane.add(scrollPane);
		
		// Table
		tabla = new JTable();		
		
		// Model for Table
		modelo = new DefaultTableModel() {

			public Class<?> getColumnClass(int column) {
				
        		switch (column) {
                case 0:
                    return Boolean.class;
                case 1:
                    return Integer.class;
                case 2:
                	return String.class;
                case 3:
                	return String.class;
                case 4:
                	return String.class;
                case 5:
                	return String.class;
                case 6:
                	return String.class;
                case 7:
                	return Double.class;
                case 8:
                	return String.class;
                case 9:
                	return String.class;
                default:
                	return Boolean.class;
        		}
			}
		};
		tabla.setModel(modelo);
		
		modelo.addColumn("Seleccionar");
		modelo.addColumn("ID");
		modelo.addColumn("Fecha");
		modelo.addColumn("Compra");
		modelo.addColumn("Tipo");
		modelo.addColumn("Tienda");
		modelo.addColumn("Producto");
		modelo.addColumn("Coste");
		modelo.addColumn("Pago");
		modelo.addColumn("Tarjeta");
		
		tableColumn = tabla.getColumnModel().getColumn(0);
	    tableColumn.setCellEditor(tabla.getDefaultEditor(Boolean.class));
	    tableColumn.setCellRenderer(tabla.getDefaultRenderer(Boolean.class));
	    tableColumn.setHeaderRenderer(new CheckBoxHeader(new MyItemListener()));
		
		
		// Se establece una anchura de las columnas
		tabla.getColumn("Seleccionar").setPreferredWidth(40);
		tabla.getColumn("ID").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Fecha").setPreferredWidth(40); 
		//tabla.getColumn("Compra").setPreferredWidth(30);
		tabla.getColumn("Compra").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Compra").setPreferredWidth(0); // Se oculta la columna
		//tabla.getColumn("Tipo").setPreferredWidth(30);
		tabla.getColumn("Tipo").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tipo").setPreferredWidth(0); // Se oculta la columna
		tabla.getColumn("Tienda").setPreferredWidth(40); 
		tabla.getColumn("Producto").setPreferredWidth(200);
		tabla.getColumn("Coste").setPreferredWidth(20);
		tabla.getColumn("Pago").setPreferredWidth(30);
		//tabla.getColumn("Tarjeta").setPreferredWidth(20);
		tabla.getColumn("Tarjeta").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("Tarjeta").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(true);
		tabla.setBorder(null);
		scrollPane.setViewportView(tabla);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 552, 95, 14);
		contentPane.add(lblVersion);	
		
		JLabel lblPuedeSeleccionarDe = new JLabel("Puede seleccionar de la siguiente la opci\u00F3n a ver los gastos:");
		//lblPuedeSeleccionarDe.setFont(new Font("Arial", Font.BOLD, 12));
		avisoFont.setFamily("Arial");
		avisoFont.setStyle(1);
		avisoFont.setSize(12);
		lblPuedeSeleccionarDe.setBounds(10, 24, 363, 20);
		contentPane.add(lblPuedeSeleccionarDe);
		
		cbOpciones = new JComboBox<Object>(listaOpciones);
		cbOpciones.addActionListener((ActionListener)this);
		cbOpciones.setBounds(405, 25, 151, 20);
		contentPane.add(cbOpciones);
		
		lblFecha = new JLabel("Fecha:");
		lblFecha.setVisible(false);
		lblFecha.setForeground(new Color(0, 139, 139));
		//lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblFecha.setBounds(37, 76, 95, 14);
		contentPane.add(lblFecha);
		
		dateChooser1 = new JDateChooser();
		dateChooser1.setVisible(false);
		dateChooser1.setDateFormatString("dd/MM/yyyy");
		dateChooser1.setBounds(126, 70, 126, 20);
		dateChooser1.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser1.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser1);
		
		lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setVisible(false);
		lblFechaFin.setForeground(new Color(0, 139, 139));
		//lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblFechaFin.setBounds(37, 111, 95, 14);
		contentPane.add(lblFechaFin);
		
		dateChooser2 = new JDateChooser();
		dateChooser2.setVisible(false);
		dateChooser2.setDateFormatString("dd/MM/yyyy");
		dateChooser2.setBounds(126, 111, 126, 20);
		dateChooser2.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evento) {
						if (evento.getPropertyName().equals("date")) {
							// Se le da formato a la fecha obtenida
							resultFechaFin = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser2.getDate());
		                }
					}
			    });
		contentPane.add(dateChooser2);
		
		lblTipoTienda = new JLabel("Tipo tienda:");
		lblTipoTienda.setVisible(false);
		lblTipoTienda.setForeground(new Color(0, 139, 139));
		//lblTipoTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblTipoTienda.setBounds(405, 73, 95, 14);
		contentPane.add(lblTipoTienda);
		
		cbTipoTienda = new JComboBox<Tipo_tienda>();
		cbTipoTienda.setVisible(false);
		cbTipoTienda.addActionListener((ActionListener)this);
		cbTipoTienda.setBounds(495, 70, 151, 20);
		contentPane.add(cbTipoTienda);
		
		lblTienda = new JLabel("Tienda:");
		lblTienda.setVisible(false);
		lblTienda.setForeground(new Color(0, 139, 139));
		//lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblTienda.setBounds(405, 114, 95, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tienda>();
		cbTienda.setVisible(false);
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(495, 111, 151, 20);
		contentPane.add(cbTienda);
		
		btnVer = new JButton("Ver");
		btnVer.setVisible(false);
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(273, 70, 110, 61);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		lblCosteTotal = new JLabel("Total:");
		lblCosteTotal.setVisible(false);
		lblCosteTotal.setForeground(new Color(0, 139, 139));
		//lblCosteTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblCosteTotal.setBounds(474, 481, 50, 14);
		contentPane.add(lblCosteTotal);
		
		lblCantidad= new JLabel();
		lblCantidad.setVisible(false);
		lblCantidad.setForeground(Color.BLACK);
		//lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		avisoFechas.setFamily("Tahoma");
		avisoFechas.setStyle(1);
		avisoFechas.setSize(11);
		lblCantidad.setBounds(522, 481, 78, 14);
		contentPane.add(lblCantidad);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(237, 527, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		btnPDF = new JButton("Crear");
		btnPDF.addActionListener((ActionListener)this);
		btnPDF.setBackground(new Color(184, 231, 255));
		btnPDF.setBounds(369, 527, 95, 41);
		btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/pdf.png")));
		contentPane.add(btnPDF);
		
	} // Cierre componentes
	
	// Metodo para limpiar los datos de la tabla.
	private void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento){
		// Si selecciona la lista de "Opciones"
		if(evento.getSource()==cbOpciones){
			if(cbOpciones.isValid()==true){
				if(cbOpciones.getSelectedItem()!=null){
					try{
						dateChooser2.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}					
					// Se guarda el item seleccionado
					resultOpcion=cbOpciones.getSelectedItem().toString();
					// Se limpian los componentes
					limpiarTabla();
					resultFecha="";
					resultFechaFin="";
					resultTipoTienda="";
					resultTienda="";
					lblCosteTotal.setVisible(false);
					lblCantidad.setVisible(false);
					lblCantidad.setText("");
					try{
						dateChooser1.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}
					if(resultOpcion.equals("1 Fecha")){
						//JOptionPane.showMessageDialog(null, "Te aparecer� 1 fecha a filtrar");
						lblFecha.setVisible(true);
						dateChooser1.setVisible(true);
						lblFechaFin.setVisible(false);
						dateChooser2.setVisible(false);
						lblTipoTienda.setVisible(true);
						cbTipoTienda.setVisible(true);
						lblTienda.setVisible(true);
						cbTienda.setVisible(true);
						btnVer.setVisible(true);
					}
					if(resultOpcion.equals("Entre fechas")){
						//JOptionPane.showMessageDialog(null, "Te aparecer� 2 fechas a filtrar");
						lblFecha.setVisible(true);
						dateChooser1.setVisible(true);
						lblFechaFin.setVisible(true);
						dateChooser2.setVisible(true);
						lblTipoTienda.setVisible(true);
						cbTipoTienda.setVisible(true);
						lblTienda.setVisible(true);
						cbTienda.setVisible(true);
						btnVer.setVisible(true);
					}
					if(resultOpcion.equals("Tiendas")){
						//JOptionPane.showMessageDialog(null, "Te aparecer� la lista de TIENDAS");
						lblFecha.setVisible(false);
						dateChooser1.setVisible(false);
						lblFechaFin.setVisible(false);
						dateChooser2.setVisible(false);
						lblTipoTienda.setVisible(true);
						cbTipoTienda.setVisible(true);
						lblTienda.setVisible(true);
						cbTienda.setVisible(true);
						btnVer.setVisible(true);
					}
				}
			}
		}
		// Si ha seleccionado alguna opcion de "Tipo tienda"
		if(evento.getSource()==cbTipoTienda){
			if(cbTipoTienda.isValid()==true){
				if(cbTipoTienda.getSelectedItem()!=null){
					// Guarda el valor de item seleccionado "Tipo tienda"
					tipo=cbTipoTienda.getSelectedItem().toString();
					resultTipoTienda=tipo;
					// Limpia los anteriores items de la lista desplegable
					cbTienda.removeAllItems();
					
					try {
						// Sentencia SELECT
						sql = "SELECT * FROM tipo_tienda WHERE tipo='"+resultTipoTienda+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							// Se obtiene el codigo de tipo de tienda
							codigoTienda = rs.getInt("idTipo");
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
						
						// Sentencia SELECT
						sql = "SELECT * FROM tiendas WHERE idTipo="+codigoTienda+";";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						//Mientras que haya datos
						while (rs.next()) { 
							
							//Obtienen los datos
							idTienda=rs.getInt("idTienda");
							nombreTienda=rs.getString("nombre");
							
							// Se instancia la clase Tipo_tienda
							tiendaModel = new Tienda (idTienda,nombreTienda); 
							
							//Se a�ade cada objeto al JComboBox
							cbTienda.addItem(tiendaModel);
							
						} // Cierre de while					
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion
				}				
			}
		}
		// Si ha seleccionado "Tienda"
		if(evento.getSource()==cbTienda){
			if(cbTienda.isValid()==true){
				if(cbTienda.getSelectedItem()!=null){ // Si lo seleccionado no es igual a null
					// Se guarda el valor del item seleccionado
					tienda=cbTienda.getSelectedItem().toString();
					resultTienda=tienda;
				}
			}
		}
		// Si ha pulsado el boton "Ver"
		if(evento.getSource()==btnVer){
			limpiarTabla();
			if(resultOpcion.equals("1 Fecha")){
				if(cbTipoTienda.getSelectedItem()==null){ // Si la seleccion es igual a null
					//JOptionPane.showMessageDialog(null, "Has seleccionado 1 fecha");
					// Se hace la conexion con la BD
					try {		
						// Sentencia SQL SELECT
						sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' ORDER BY fecha ASC, idCompra ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 
							
							modelo.addRow(new Object[0]);
							modelo.setValueAt(check, i, 0);
							modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
							String fecha=rs.getString(2);
							int pos=fecha.indexOf("-");
							String year=fecha.substring(0,pos);
							String mes=fecha.substring(pos+1,fecha.length());
							int pos2=mes.indexOf("-");
							mes=mes.substring(0,pos2);
							String dia=fecha.substring(pos2+pos+2,fecha.length());
							//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
							modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
							modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
							modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
							modelo.setValueAt(rs.getString(5), i, 5); //tienda
							modelo.setValueAt(rs.getString(6), i, 6); //producto
							modelo.setValueAt(rs.getDouble(7), i, 7); //coste
							modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
							modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
							
							i++;
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
						
						i=0;
						lblCosteTotal.setVisible(true);
						lblCantidad.setVisible(true);
						sql="SELECT sum(coste) FROM compras WHERE fecha='"+resultFecha+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 						
							lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
						} // Cierre de while
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion		
					
					// Limpia los componentes de la ventana
					
					try{
						dateChooser1.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}				
				}
				else{
					if(tienda.equals("")){
						//JOptionPane.showMessageDialog(null, "has seleccionado fecha+tipo tienda");
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' AND tipo_tienda='"+tipo+"' ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								modelo.addRow(new Object[0]);
								modelo.setValueAt(check, i, 0);
								modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
								String fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
								modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
								modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
								modelo.setValueAt(rs.getString(5), i, 5); //tienda
								modelo.setValueAt(rs.getString(6), i, 6); //producto
								modelo.setValueAt(rs.getDouble(7), i, 7); //coste
								modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
								modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
								
								i++;
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							i=0;
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE fecha='"+resultFecha+"' AND tipo_tienda='"+tipo+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						tipo="";
						tienda="";
						try{
							dateChooser1.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTipoTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
					}
					else{						
						//JOptionPane.showMessageDialog(null, "has seleccionado fecha+tienda");
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE fecha='"+resultFecha+"' AND tienda='"+tienda+"' ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								modelo.addRow(new Object[0]);
								modelo.setValueAt(check, i, 0);
								modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
								String fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
								modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
								modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
								modelo.setValueAt(rs.getString(5), i, 5); //tienda
								modelo.setValueAt(rs.getString(6), i, 6); //producto
								modelo.setValueAt(rs.getDouble(7), i, 7); //coste
								modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
								modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
								
								i++;
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							i=0;
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE fecha='"+resultFecha+"' AND tienda='"+tienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						tipo="";
						tienda="";
						// Limpia los componentes de la ventana
						try{
							dateChooser1.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTipoTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
					}

				}
			}
			if(resultOpcion.equals("Entre fechas")){
				//JOptionPane.showMessageDialog(null, "Has seleccionado entre 2 fechas");
				if(cbTipoTienda.getSelectedItem()==null){ // Si la seleccion es igual a null
					// Se hace la conexion con la BD
					try {		
						// Sentencia SQL SELECT
						//sql = "SELECT * FROM compras WHERE fecha>='"+resultFecha+"' and fecha<='"+resultFechaFin+"';";
						sql="SELECT * FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"' ORDER BY fecha ASC, idCompra ASC;";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 
							
							modelo.addRow(new Object[0]);
							modelo.setValueAt(check, i, 0);
							modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
							String fecha=rs.getString(2);
							int pos=fecha.indexOf("-");
							String year=fecha.substring(0,pos);
							String mes=fecha.substring(pos+1,fecha.length());
							int pos2=mes.indexOf("-");
							mes=mes.substring(0,pos2);
							String dia=fecha.substring(pos2+pos+2,fecha.length());
							//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
							modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
							modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
							modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
							modelo.setValueAt(rs.getString(5), i, 5); //tienda
							modelo.setValueAt(rs.getString(6), i, 6); //producto
							modelo.setValueAt(rs.getDouble(7), i, 7); //coste
							modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
							modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
							
							i++;
							
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
						
						i=0;
						lblCosteTotal.setVisible(true);
						lblCantidad.setVisible(true);
						sql="SELECT sum(coste) FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"';";
						// Hace la consulta y devuelve el resultado
						rs = BD.consulta(stmt,sql);
						// Mientras que haya datos
						while (rs.next()) { 						
							lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
						} // Cierre de while
						
						rs.close(); //Cierre de la consulta
					}
					catch (SQLException e) {
						// Muestra error SQL
						JOptionPane.showMessageDialog(null, e.getMessage());
						
					} // Cierre excepcion		
					// Limpia los componentes de la ventana
					try{
						dateChooser1.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}	
					try{
						dateChooser2.setCalendar(null);
					}
					catch(NullPointerException npe){
						
					}
				}
				else{
					if(tienda.equals("")){
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"' AND tipo_tienda='"+tipo+"'  ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								modelo.addRow(new Object[0]);
								modelo.setValueAt(check, i, 0);
								modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
								String fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
								modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
								modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
								modelo.setValueAt(rs.getString(5), i, 5); //tienda
								modelo.setValueAt(rs.getString(6), i, 6); //producto
								modelo.setValueAt(rs.getDouble(7), i, 7); //coste
								modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
								modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
								
								i++;
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							i=0;
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"' AND tipo_tienda='"+tipo+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta	
							tipo="";
							tienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						try{
							cbTipoTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							dateChooser1.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}	
						try{
							dateChooser2.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
					}
					else{
						// Se hace la conexion con la BD
						try {		
							// Sentencia SQL SELECT
							sql = "SELECT * FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"' AND tienda='"+tienda+"' ORDER BY fecha ASC, idCompra ASC;";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 
								
								modelo.addRow(new Object[0]);
								modelo.setValueAt(check, i, 0);
								modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
								String fecha=rs.getString(2);
								int pos=fecha.indexOf("-");
								String year=fecha.substring(0,pos);
								String mes=fecha.substring(pos+1,fecha.length());
								int pos2=mes.indexOf("-");
								mes=mes.substring(0,pos2);
								String dia=fecha.substring(pos2+pos+2,fecha.length());
								//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
								modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
								modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
								modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
								modelo.setValueAt(rs.getString(5), i, 5); //tienda
								modelo.setValueAt(rs.getString(6), i, 6); //producto
								modelo.setValueAt(rs.getDouble(7), i, 7); //coste
								modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
								modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
								
								i++;
								
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta
							
							i=0;
							lblCosteTotal.setVisible(true);
							lblCantidad.setVisible(true);
							sql="SELECT sum(coste) FROM compras WHERE fecha BETWEEN '"+resultFecha+"' AND '"+resultFechaFin+"' AND tienda='"+tienda+"';";
							// Hace la consulta y devuelve el resultado
							rs = BD.consulta(stmt,sql);
							// Mientras que haya datos
							while (rs.next()) { 						
								lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
							} // Cierre de while
							
							rs.close(); //Cierre de la consulta							
							tipo="";
							tienda="";
						}
						catch (SQLException e) {
							// Muestra error SQL
							JOptionPane.showMessageDialog(null, e.getMessage());
							
						} // Cierre excepcion
						// Limpia los componentes de la ventana
						try{
							cbTipoTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							cbTienda.setSelectedItem(null);
						}
						catch(NullPointerException npe){
							
						}
						try{
							dateChooser1.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}	
						try{
							dateChooser2.setCalendar(null);
						}
						catch(NullPointerException npe){
							
						}
					}
				}
			}
			if(resultOpcion.equals("Tiendas")){
				//JOptionPane.showMessageDialog(null, "Has seleccionado tiendas");
				// Se hace la conexion con la BD
				try {		
					// Sentencia SQL SELECT
					sql = "SELECT * FROM compras WHERE tienda='"+tienda+"'  ORDER BY fecha ASC, idCompra ASC;";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					// Mientras que haya datos
					while (rs.next()) { 
						
						modelo.addRow(new Object[0]);
						modelo.setValueAt(check, i, 0);
						modelo.setValueAt(rs.getInt(1), i, 1); // idCompra
						String fecha=rs.getString(2);
						int pos=fecha.indexOf("-");
						String year=fecha.substring(0,pos);
						String mes=fecha.substring(pos+1,fecha.length());
						int pos2=mes.indexOf("-");
						mes=mes.substring(0,pos2);
						String dia=fecha.substring(pos2+pos+2,fecha.length());
						//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
						modelo.setValueAt(dia+"/"+mes+"/"+year, i, 2); //fecha
						modelo.setValueAt(rs.getString(3), i, 3); //tipo_compra
						modelo.setValueAt(rs.getString(4), i, 4); //tipo_tienda
						modelo.setValueAt(rs.getString(5), i, 5); //tienda
						modelo.setValueAt(rs.getString(6), i, 6); //producto
						modelo.setValueAt(rs.getDouble(7), i, 7); //coste
						modelo.setValueAt(rs.getString(8), i, 8); //forma_pago
						modelo.setValueAt(rs.getString(9), i, 9); //tarjeta
						
						i++;
						
					} // Cierre de while
					
					rs.close(); //Cierre de la consulta
					
					i=0;
					lblCosteTotal.setVisible(true);
					lblCantidad.setVisible(true);
					sql="SELECT sum(coste) FROM compras WHERE tienda='"+resultTienda+"';";
					// Hace la consulta y devuelve el resultado
					rs = BD.consulta(stmt,sql);
					// Mientras que haya datos
					while (rs.next()) { 						
						lblCantidad.setText(Double.toString(rs.getDouble(1))+" �");
					} // Cierre de while
					
					rs.close(); //Cierre de la consulta
					tipo="";
					tienda="";
				}
				catch (SQLException e) {
					// Muestra error SQL
					JOptionPane.showMessageDialog(null, e.getMessage());
					
				} // Cierre excepcion
				// Limpia los componentes de la ventana
				try{
					cbTipoTienda.setSelectedItem(null);
				}
				catch(NullPointerException npe){
					
				}
				try{
					cbTienda.setSelectedItem(null);
				}
				catch(NullPointerException npe){
					
				}
			}
		}
		// Si hace clic en "Menu" se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){
		
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en "Crear" PDF, creara un documento de pdf
		if(evento.getSource()==btnPDF){
			for (int i = 0; i < tabla.getRowCount(); i++) {
				
				// Se obtienen los resultados de la tabla
				Boolean checked = Boolean.valueOf(tabla.getValueAt(i, 0).toString());
				int id=(int) tabla.getValueAt(i, 1);
				String fecha=tabla.getValueAt(i, 2).toString();
				String tipoCompra=tabla.getValueAt(i, 3).toString();
				String tipoTienda=tabla.getValueAt(i, 4).toString();
				String tienda=tabla.getValueAt(i, 5).toString();
				String producto=tabla.getValueAt(i, 6).toString();
				double coste=(double) tabla.getValueAt(i, 7);
				String pago=tabla.getValueAt(i, 8).toString();
				String tarjeta=tabla.getValueAt(i, 9).toString();
				// Si esta seleccionado esa fila, se agrega al array
				if (checked) {
					// Se crea un objeto Compra
					compra=new Compra(id,fecha,tipoCompra,tipoTienda,tienda,producto,coste,pago,tarjeta);
					// Se agrega al arraylist
					arrayPDF.add(compra);
				}
			}
			// Si la opcion es con 1 fecha, guardara la fecha tambien
			if(resultOpcion.equals("1 Fecha")){
				if(resultTipoTienda.equals("")){ // Si la seleccion es igual a null
					try {					
						// Se crea el objeto Document    horizontal
						Document documento=new Document(PageSize.A4.rotate());
						// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
						//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+".pdf"));
						PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+".pdf"));
						// Se abre el documneto para despues escribir
						documento.open();
						// Se agrega el titulo
						agregarTitulo(documento);
						// Se agrega la tabla
						agregarTabla(documento,vector,arrayPDF);
						// Se cierra el documento
						documento.close();
						// Se cierra el documento
						documento.close();
						// Muestra al usuario el mensaje
						JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
						
					}
					catch (FileNotFoundException fnfe) {
						JOptionPane.showMessageDialog(null, fnfe.getMessage());
					} 
					catch (DocumentException de) {
						JOptionPane.showMessageDialog(null, de.getMessage());
					}
				}
				else{
					if(resultTienda.equals("")){
						// Fecha + tipoTienda
						try {						
							// Se crea el objeto Document    horizontal
							Document documento=new Document(PageSize.A4.rotate());
							// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
							//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+"_"+resultTipoTienda+".pdf"));
							PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+"_"+resultTienda+".pdf"));
							// Se abre el documneto para despues escribir
							documento.open();
							// Se agrega el titulo
							agregarTitulo(documento);
							// Se agrega la tabla
							agregarTabla(documento,vector,arrayPDF);
							// Se cierra el documento
							documento.close();
							// Se cierra el documento
							documento.close();
							// Muestra al usuario el mensaje
							JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
							resultTipoTienda="";
							resultTienda="";
							
						}
						catch (FileNotFoundException fnfe) {
							JOptionPane.showMessageDialog(null, fnfe.getMessage());
						} 
						catch (DocumentException de) {
							JOptionPane.showMessageDialog(null, de.getMessage());
						}
					}
					else{
						// Fecha + tipoTienda + tienda
						try {						
							// Se crea el objeto Document    horizontal
							Document documento=new Document(PageSize.A4.rotate());
							// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
							//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+"_"+resultTienda+".pdf"));
							PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+"_"+resultTienda+".pdf"));
							// Se abre el documneto para despues escribir
							documento.open();
							// Se agrega el titulo
							agregarTitulo(documento);
							// Se agrega la tabla
							agregarTabla(documento,vector,arrayPDF);
							// Se cierra el documento
							documento.close();
							// Se cierra el documento
							documento.close();
							// Muestra al usuario el mensaje
							JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
							resultTipoTienda="";
							resultTienda="";
							
						}
						catch (FileNotFoundException fnfe) {
							JOptionPane.showMessageDialog(null, fnfe.getMessage());
						} 
						catch (DocumentException de) {
							JOptionPane.showMessageDialog(null, de.getMessage());
						}
					}
				}
			} // cierre if 1 fecha
			if(resultOpcion.equals("Entre fechas")){
				if(resultTipoTienda.equals("")){ // Si la seleccion es igual a null
					// Entre fechas
					try {					
						// Se crea el objeto Document    horizontal
						Document documento=new Document(PageSize.A4.rotate());
						// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
						//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+"_"+resultFechaFin+".pdf"));
						PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+"_"+resultFechaFin+".pdf"));
						// Se abre el documneto para despues escribir
						documento.open();
						// Se agrega el titulo
						agregarTitulo(documento,resultFecha,resultFechaFin);
						// Se agrega la tabla
						agregarTabla(documento,vector,arrayPDF);
						// Se cierra el documento
						documento.close();
						// Se cierra el documento
						documento.close();
						// Muestra al usuario el mensaje
						JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
						
					}
					catch (FileNotFoundException fnfe) {
						JOptionPane.showMessageDialog(null, fnfe.getMessage());
					} 
					catch (DocumentException de) {
						JOptionPane.showMessageDialog(null, de.getMessage());
					}
				}
				else{
					if(resultTienda.equals("")){
						// Entre fechas + tipoTienda
						try {						
							// Se crea el objeto Document    horizontal
							Document documento=new Document(PageSize.A4.rotate());
							// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
							//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+"_"+resultFechaFin+"_"+resultTipoTienda+".pdf"));
							PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+"_"+resultFechaFin+"_"+resultTipoTienda+".pdf"));
							// Se abre el documneto para despues escribir
							documento.open();
							// Se agrega el titulo
							agregarTitulo(documento,resultFecha,resultFechaFin);
							// Se agrega la tabla
							agregarTabla(documento,vector,arrayPDF);
							// Se cierra el documento
							documento.close();
							// Se cierra el documento
							documento.close();
							// Muestra al usuario el mensaje
							JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
							resultTipoTienda="";
							resultTienda="";
							
						}
						catch (FileNotFoundException fnfe) {
							JOptionPane.showMessageDialog(null, fnfe.getMessage());
						} 
						catch (DocumentException de) {
							JOptionPane.showMessageDialog(null, de.getMessage());
						}
					}
					else{
						// Entre fechas + tipoTienda + tienda
						try {						
							// Se crea el objeto Document    horizontal
							Document documento=new Document(PageSize.A4.rotate());
							// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
							//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultFecha+"_"+resultFechaFin+"_"+resultTienda+".pdf"));
							PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultFecha+"_"+resultFechaFin+"_"+resultTienda+".pdf"));
							// Se abre el documneto para despues escribir
							documento.open();
							// Se agrega la tabla
							agregarTitulo(documento,resultFecha,resultFechaFin);
							// Se agrega la tabla
							agregarTabla(documento,vector,arrayPDF);
							// Se cierra el documento
							documento.close();
							// Se cierra el documento
							documento.close();
							// Muestra al usuario el mensaje
							JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
							resultTipoTienda="";
							resultTienda="";
							
						}
						catch (FileNotFoundException fnfe) {
							JOptionPane.showMessageDialog(null, fnfe.getMessage());
						} 
						catch (DocumentException de) {
							JOptionPane.showMessageDialog(null, de.getMessage());
						}
					}
				}
			} // cierre if Entre fechas
			if(resultOpcion.equals("Tiendas")){
				try {						
					// Se crea el objeto Document    horizontal
					Document documento=new Document(PageSize.A4.rotate());
					// Se instancia PdfWriter pasando el documento, y la carpeta y el nombre del documento 
					//PdfWriter.getInstance(documento, new FileOutputStream("src/archivos/compra_"+resultTienda+".pdf"));
					PdfWriter.getInstance(documento, new FileOutputStream("archivos/compra_"+resultTienda+".pdf"));
					// Se abre el documneto para despues escribir
					documento.open();
					// Se agrega la tabla
					agregarTitulo(documento);
					// Se agrega la tabla
					agregarTabla(documento,vector,arrayPDF);
					// Se cierra el documento
					documento.close();
					// Se cierra el documento
					documento.close();
					// Muestra al usuario el mensaje
					JOptionPane.showMessageDialog(null, "Se ha creado el documento PDF");
					resultTipoTienda="";
					resultTienda="";
					
				}
				catch (FileNotFoundException fnfe) {
					JOptionPane.showMessageDialog(null, fnfe.getMessage());
				} 
				catch (DocumentException de) {
					JOptionPane.showMessageDialog(null, de.getMessage());
				}
			} // cierre if Tiendas
		}
	} // Cierre actionPerformed
	
	private static void agregarTitulo(Document documento) throws DocumentException{
		
		// Se crea el objeto Pararagrap
		Paragraph parrafo=new Paragraph();
		// Se agrega la fecha
		documento.add(new Paragraph(resultFecha, headerFont));
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se crea una tabla de una celda
		PdfPTable tabla=new PdfPTable(1);
		// Se crea una celda con una frase
		PdfPCell celda = new PdfPCell(new Phrase("Compra",titleFont));
		// Se escribe en el centro
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		// No tiene borde
		celda.setBorder(Rectangle.NO_BORDER);
		// Se agrega la celda a la tabla
		tabla.addCell(celda);
		// Se agrega la tabla al documento
		documento.add(tabla);		
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se agregan los parrafos al documento
		documento.add(parrafo);
		
	} // agregarTitulo
	
	private static void agregarTitulo(Document documento,String fechaInicio, String fechaFin) throws DocumentException{
		
		// Se crea el objeto Pararagrap
		Paragraph parrafo=new Paragraph();
		// Se agrega la fecha
		documento.add(new Paragraph("Desde "+fechaInicio+" hasta "+fechaFin, headerFont));
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se crea una tabla de una celda
		PdfPTable tabla=new PdfPTable(1);
		// Se crea una celda con una frase
		PdfPCell celda = new PdfPCell(new Phrase("Compra",titleFont));
		// Se escribe en el centro
		celda.setHorizontalAlignment(Element.ALIGN_CENTER);
		// No tiene borde
		celda.setBorder(Rectangle.NO_BORDER);
		// Se agrega la celda a la tabla
		tabla.addCell(celda);
		// Se agrega la tabla al documento
		documento.add(tabla);		
		// Se agrega 1 linea vacia
		agregarSaltoLinea(parrafo,1);
		// Se agregan los parrafos al documento
		documento.add(parrafo);
		
	} // agregarTitulo
	
	private static void agregarTabla(Document documento, String[] header, ArrayList<Compra> datos) throws DocumentException{
		
		// Se crea una tabla de 9 columnas
		PdfPTable tabla=new PdfPTable(9);
		// 100% width of columns table
		//tabla.setWidthPercentage(100);
		tabla.setTotalWidth(documento.getPageSize().getWidth() - 60);
		tabla.setLockedWidth(true);
	    
	    for(int i=0;i<header.length;i++){
	    	// Se crea la celda cabecera
	    	PdfPCell cellHead = new PdfPCell(new Phrase(header[i],cellHeaderFont));
	    	// Se escribe en el centro
	    	cellHead.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	// Se agrega la celda a la tabla
		    tabla.addCell(cellHead);
	    }
	    
	    // Se agrega la cabecera a la tabla
	    tabla.setHeaderRows(1);
	    
	    // Se agragan cada celda con algun dato a la tabla
	    for(int i=0;i<datos.size();i++){
	
	    	tabla.addCell(Integer.toString(datos.get(i).getIdCompra())); // ID
		    tabla.addCell(datos.get(i).getFecha()); // fecha
		    tabla.addCell(datos.get(i).getTipoCompra()); // tipo compra
		    tabla.addCell(datos.get(i).getTipoTienda()); // tipo tienda
		    tabla.addCell(datos.get(i).getTienda()); // tienda
		    tabla.addCell(datos.get(i).getProducto()); // producto
		    tabla.addCell(Double.toString(datos.get(i).getCoste())); // coste
		    tabla.addCell(datos.get(i).getFormaPago()); // forma pago
		    tabla.addCell(datos.get(i).getTarjeta()); // tarjeta

	    }
	    
		// Se agrega la tabla al documento
	 	documento.add(tabla);
	    
		
	} // AgregarContenido
	
	private static void agregarSaltoLinea(Paragraph paragraph, int number) {
		
	    for (int i=0;i<number;i++) {
	    	paragraph.add(new Paragraph(" "));
	    }
	} // agregarSaltoLinea
	
	 class MyItemListener implements ItemListener{
		 
		public void itemStateChanged(ItemEvent e) {
			
			Object source = e.getSource();
			if (source instanceof AbstractButton == false) return;		
			boolean checked = e.getStateChange() == ItemEvent.SELECTED;
			for(int i=0,j=tabla.getRowCount();i<j;i++){
				tabla.setValueAt(new Boolean(checked),i,0);
			}
		}
	} // class MyItemListener
	
} // Cierre de clase Concretos

@SuppressWarnings("serial")
class CheckBoxHeader extends JCheckBox implements TableCellRenderer, MouseListener {
	
	// Atributos
	protected CheckBoxHeader rendererComponent;
	protected int column;
	protected boolean mousePressed = false;
	
	public CheckBoxHeader(ItemListener itemListener) {
		
		rendererComponent = this;
		rendererComponent.addItemListener(itemListener);
		
	} // Constructor
	
	// Getter
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column) {
		
		if (table != null) {
			JTableHeader header = table.getTableHeader();
			
			if (header != null) {
				rendererComponent.setForeground(header.getForeground());
				rendererComponent.setBackground(header.getBackground());
				rendererComponent.setFont(header.getFont());
				header.addMouseListener(rendererComponent);
			}
		}
		
		setColumn(column);
		rendererComponent.setText("Seleccionar");
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		
		return rendererComponent;
    
	}
  
	protected void setColumn(int column) {
		this.column = column;
	}
	
	public int getColumn() {
		return column;
	}
	
	// Methods
	protected void handleClickEvent(MouseEvent e) {
		if (mousePressed){
			
			mousePressed=false;
			
			JTableHeader header = (JTableHeader)(e.getSource());
			JTable tableView = header.getTable();
			TableColumnModel columnModel = tableView.getColumnModel();
			
			int viewColumn = columnModel.getColumnIndexAtX(e.getX());
			int column = tableView.convertColumnIndexToModel(viewColumn);
			
			if (viewColumn == this.column && e.getClickCount() == 1 && column != -1) {
				doClick();
			}
		}
	} // handleClickEvent
  
	public void mouseClicked(MouseEvent e) {
		
		handleClickEvent(e);
		((JTableHeader)e.getSource()).repaint();
		
	} //mouseClicked
	
	public void mousePressed(MouseEvent e) {
		
		mousePressed = true;
		
	} // mousePressed
	
	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}
	
} // class CheckBoxHeader