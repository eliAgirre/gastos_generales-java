package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
/*import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;*/

import controlador.ControladorPrincipal;

@SuppressWarnings("serial")
public class EnviarMail extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextField txtToAdress;
	private JTextField txtSubject;
	private JTextField txtAdjunt;
	private JTextArea areaMensaje;
	private JButton btnAdjuntar;
	private String path;
	private String nombreFile;
	private MimeMessage message;
	private JButton btnEnviar;
	private JButton btnMenu;
	private int resultFichero;
	
	public EnviarMail(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el size
		setBounds(400, 200, 500, 550); // size
		setTitle("Crear emails"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/mail.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		componentes();
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(114, 199, 347, 225);
		contentPane.add(scrollPane);
		
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 496, 95, 14);
		contentPane.add(lblVersion);	
		
		JLabel lblPara = new JLabel("Para:");
		lblPara.setHorizontalAlignment(SwingConstants.LEFT);
		lblPara.setFont(new Font("Arial", Font.BOLD, 12));
		lblPara.setBounds(39, 38, 69, 14);
		contentPane.add(lblPara);
		
		txtToAdress = new JTextField();
		txtToAdress.setBounds(114, 36, 347, 20);
		contentPane.add(txtToAdress);
		txtToAdress.setColumns(10);
		
		JLabel lblAsunto = new JLabel("Asunto:");
		lblAsunto.setHorizontalAlignment(SwingConstants.LEFT);
		lblAsunto.setFont(new Font("Arial", Font.BOLD, 12));
		lblAsunto.setBounds(39, 90, 69, 14);
		contentPane.add(lblAsunto);
		
		txtSubject = new JTextField();
		txtSubject.setColumns(10);
		txtSubject.setBounds(114, 88, 347, 20);
		contentPane.add(txtSubject);
		
		JLabel lblAdjunto = new JLabel("Adjunto:");
		lblAdjunto.setHorizontalAlignment(SwingConstants.LEFT);
		lblAdjunto.setFont(new Font("Arial", Font.BOLD, 12));
		lblAdjunto.setBounds(39, 146, 69, 14);
		contentPane.add(lblAdjunto);
		
		txtAdjunt = new JTextField();
		txtAdjunt.setColumns(10);
		txtAdjunt.setBounds(114, 144, 289, 20);
		contentPane.add(txtAdjunt);
		
		btnAdjuntar = new JButton("");
		btnAdjuntar.setBackground(new Color(184, 231, 255));
		btnAdjuntar.setBounds(426, 133, 35, 34);
		btnAdjuntar.addActionListener((ActionListener) this);
		btnAdjuntar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/attach.png")));
		contentPane.add(btnAdjuntar);
		
		JLabel lblMensaje = new JLabel("Mensaje:");
		lblMensaje.setHorizontalAlignment(SwingConstants.LEFT);
		lblMensaje.setFont(new Font("Arial", Font.BOLD, 12));
		lblMensaje.setBounds(39, 199, 69, 14);
		contentPane.add(lblMensaje);
		
		areaMensaje = new JTextArea();
		areaMensaje.setBounds(114, 199, 347, 225);
		areaMensaje.setLineWrap(true);
		areaMensaje.setForeground(Color.BLACK);
		scrollPane.setViewportView(areaMensaje);
		//Color customColor = new Color(42,91,137);
		//areaMensaje.setForeground(customColor);
		
		
		btnEnviar = new JButton("Enviar");
		btnEnviar.setBackground(new Color(184, 231, 255));
		btnEnviar.setBounds(304, 450, 126, 41);
		btnEnviar.addActionListener((ActionListener) this);
		btnEnviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/send.png")));
		contentPane.add(btnEnviar);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(143, 450, 126, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	private void limpiar(){
		
		txtToAdress.setText("");
		txtSubject.setText("");
		txtAdjunt.setText("");
		areaMensaje.setText("");		
		
	} // Cierre limpiar
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en boton "Adjuntar"
		if(evento.getSource()==btnAdjuntar){
			// Para elegir un fichero
			JFileChooser chooser=new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setMultiSelectionEnabled(true);
			resultFichero=chooser.showOpenDialog(this);
			//Si le da al boton de cancelar vuelve a la aplicacion
			if(resultFichero==JFileChooser.CANCEL_OPTION){
				return;
			}
			//Si ha elegido el fichero
			File ficheroSeleccionado=chooser.getSelectedFile(); //coge la ruta del fichero
			// Establece y obtiene la carpeta
			txtAdjunt.setText(ficheroSeleccionado.getPath());
			path=ficheroSeleccionado.getPath();
			// Obtiene el nombre del fichero
			nombreFile=ficheroSeleccionado.getName();		
		}
		// Si hace clic en "Enviar"
		if(evento.getSource()==btnEnviar){
			try {
			    
				// Se instancia la clase Properties
				Properties props=new Properties();
				// Se establece el nombre del host de correo, es smtp.gmail.com
				props.setProperty("mail.smtp.host", "smtp.gmail.com");
				// TLS disponible
				props.setProperty("mail.smtp.starttls.enable", "true");
				// Puerto de gmail para envio de correos
				props.setProperty("mail.smtp.port","587");
				// Nombre del usuario de gmail
				props.setProperty("mail.smtp.user", "economia.casa.gastos@gmail.com");
				// Requiere usuario y password para conectarse
				props.setProperty("mail.smtp.auth", "true");
				
				// Se obtiene la session
				Session session = Session.getDefaultInstance(props);
				session.setDebug(true);

				if(nombreFile==null){
					//JOptionPane.showMessageDialog(null, "No envias adjunto");
					// Crear objeto mensaje con la session obtenida
					message = new MimeMessage(session);
					// Quien envia el correo
					message.setFrom(new InternetAddress("economia.casa.gastos@gmail.com"));
					// A quien va dirigido
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(txtToAdress.getText()));
					// Asunto
					message.setSubject(txtSubject.getText());
					// El texto del mensaje
					message.setText(areaMensaje.getText());
				}
				else{
					// Crea la parte del texto
					BodyPart texto = new MimeBodyPart();	
					texto.setText(areaMensaje.getText());
					// Crea la parte adjunta (imagen)
					BodyPart adjunto = new MimeBodyPart();
					adjunto.setDataHandler(new DataHandler(new FileDataSource(path)));
					adjunto.setFileName(nombreFile);
					// Crea un mime multi para juntarlo
					MimeMultipart multiParte = new MimeMultipart();
					multiParte.addBodyPart(texto);
					multiParte.addBodyPart(adjunto);
					
					// Crear objeto mensaje con la session obtenida
					message = new MimeMessage(session);
					// Quien envia el correo
					message.setFrom(new InternetAddress("economia.casa.gastos@gmail.com"));
					// A quien va dirigido;
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(txtToAdress.getText()));
					// Asunto
					message.setSubject(txtSubject.getText());
					// Se mete el texto y la foto adjunta
					message.setContent(multiParte);
					//message.setContent(multiParte, "text/*; charset=utf-8");
				}				
				
				// Se instancia la clase Transport para enviar mensaje
				Transport transport = session.getTransport("smtp");
				// Establece conexion
				transport.connect("economia.casa.gastos@gmail.com","3M@il91eco");
				// Envia mensaje
				transport.sendMessage(message,message.getAllRecipients());
				// Cierre conexion
				transport.close();
				// Limpia los componentes de la ventana
				limpiar();
				// Muestra un mensaje al usuario
				JOptionPane.showMessageDialog(null,"Se ha enviado el correo.");
				// Se instancia el controlador Principal
		  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
		  		// Desparece esta ventana
		  		setVisible(false);
			}
			catch (AddressException ae) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, ae.getMessage());
			}
			catch (NoSuchProviderException nspe) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, nspe.getMessage());
			}
			catch (MessagingException me) {
				// Muestra exceopcion
				JOptionPane.showMessageDialog(null, me.getMessage());
			}
		}
		// Si hace clic en "Menu" se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){		
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
	} // Cierre actionPerformed
} // Cierre clase