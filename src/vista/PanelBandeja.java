package vista;

import java.awt.*;

import javax.swing.*;

import org.jdesktop.swingx.*;


public class PanelBandeja {
	
	// Atributos de la clase
	private JXTaskPaneContainer contenedor;
	private RecibirMail recibirMail;
	private CargadorMensaje cm;

	public PanelBandeja(RecibirMail ventanaEnviar){
		
		recibirMail=ventanaEnviar;
		
		contenedor=new JXTaskPaneContainer();
		contenedor.setLayout(new BoxLayout(contenedor,BoxLayout.Y_AXIS));
		contenedor.setAlpha(0.7f);
		
		cm=new CargadorMensaje(this);
		cm.run();
		
		recibirMail.add(contenedor);
		recibirMail.setPreferredSize(new Dimension(400,450));
		
	}
	// Getter
	public JXTaskPaneContainer getContenedor() {
		return contenedor;
	}
	
	public RecibirMail getRecibirMail(){
		return recibirMail;
	}
	
	// Setter
	public void setContenedor(JXTaskPaneContainer contenedor) {
		this.contenedor = contenedor;
	}
	
} // Class