package vista;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import org.jfree.chart.*;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.*;
import org.jfree.data.category.DefaultCategoryDataset;

import com.toedter.calendar.JDateChooser;

import controlador.ControladorPrincipal;
import modelo.CosteTienda;
import modelo.Tienda;
import utilidades.BD;

@SuppressWarnings("serial")
public class GraficoTienda extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel MainPanel;
	private JPanel TopPanel;
	private JDateChooser dateInicio;
	private JDateChooser dateFin;
	private Tienda tienda;
	private JComboBox<Tienda> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private double costeTotal;
	private String fechaCompra;
	private CosteTienda costeTienda;	
	private ArrayList <CosteTienda> valores=new ArrayList <CosteTienda>();
	private DefaultCategoryDataset dataset=new DefaultCategoryDataset();
	private JFreeChart chart;
	private ChartPanel chartPanel;
	private JButton btnVer;
	private JButton btnMenu;
	private JButton btnGuardar;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
	
		// Atributos del resultados user
		private String resultFechaInicio;
		private String resultFechaFin;
		private String resultTienda;
	
	public GraficoTienda(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 754, 609); // tama�o
		setTitle("Grafico de tienda"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/chart.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
			// Sentencia SELECT
			sql="SELECT * FROM tiendas WHERE idTipo=1";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos
				idTienda=rs.getInt("idTienda");
				nombreTienda=rs.getString("nombre");
				
				// Se instancia la clase Tipo_tienda
				tienda = new Tienda (idTienda,nombreTienda); 
				
				//Se a�ade cada objeto al JComboBox
				cbTienda.addItem(tienda);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layouts
		MainPanel = new JPanel();
		MainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));		
		setContentPane(MainPanel);
		MainPanel.setLayout(null);
		
		TopPanel = new JPanel();
		TopPanel.setBounds(0, 84, 748, 432);
		TopPanel.setPreferredSize(new Dimension(600, 250));
		TopPanel.setBackground(Color.GRAY);
		MainPanel.add(TopPanel);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 555, 95, 14);
		MainPanel.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca las fechas y elija la tienda, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 26, 294, 14);
		MainPanel.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(350, 27, 103, 14);
		MainPanel.add(lblFecha);
		
		dateInicio = new JDateChooser();
		dateInicio.setDateFormatString("dd/MM/yyyy");
		dateInicio.setBounds(434, 20, 126, 20);
		dateInicio.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(dateInicio.getDate());
	                }
				}
		    });
		MainPanel.add(dateInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha fin:");
		lblFechaFin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaFin.setForeground(new Color(0, 139, 139));
		lblFechaFin.setBounds(350, 59, 95, 14);
		MainPanel.add(lblFechaFin);
		
		dateFin = new JDateChooser();
		dateFin.setDateFormatString("dd/MM/yyyy");
		dateFin.setBounds(434, 52, 126, 20);
		dateFin.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFechaFin = new SimpleDateFormat("yyyy-MM-dd").format(dateFin.getDate());
	                }
				}
		    });
		MainPanel.add(dateFin);
		
		JLabel lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(102, 56, 88, 14);
		MainPanel.add(lblTienda);
		
		cbTienda = new JComboBox<Tienda>();
		cbTienda.addActionListener((ActionListener)this);
		cbTienda.setBounds(174, 53, 126, 20);
		MainPanel.add(cbTienda);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(586, 20, 126, 53);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/chart.png")));
		MainPanel.add(btnVer);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(203, 527, 126, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		MainPanel.add(btnMenu);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(350, 527, 126, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		MainPanel.add(btnGuardar);
		
	} // Componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en "Tienda", se guarda el nombre de la tienda
		if(evento.getSource()==cbTienda){
			if(cbTienda.isValid()==true){ // Se valida JComboBox
				if(cbTienda.getSelectedItem()!=null){ // Si la seleccion item no es igual a null
					resultTienda=cbTienda.getSelectedItem().toString();
				}
			}
		}
		// Si hace clic en "Ver" visualizara el grafico
		if(evento.getSource()==btnVer){
			try {
				// Sentencia SQL SELECT
				sql = "SELECT sum(coste),fecha FROM compras WHERE fecha BETWEEN '"+resultFechaInicio+"' AND '"+resultFechaFin+"' AND tienda='"+resultTienda+"' GROUP BY fecha ASC;";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				//Mientras que haya datos
				while (rs.next()) {
					
					// Obtener los datos BD y guardar en los arrys
					costeTotal=rs.getDouble(1);
					fechaCompra=rs.getString(2);
					// Se crea el objeto
					costeTienda=new CosteTienda(costeTotal,fechaCompra);
					// Se a�ade al arraylist
					valores.add(costeTienda);
					
				} // Cierre de while			
				rs.close(); //Cierre de la consulta
				
				// Se recorre el arraylist
				for(int i=0;i<valores.size();i++){
					String numeros=Double.toString(valores.get(i).getCosteTotal())+" �";
					dataset.addValue(valores.get(i).getCosteTotal(),valores.get(i).getNombreTienda(),numeros);
				}
				// Establece los datos de la tabla
				chart=ChartFactory.createBarChart("Gastos de tienda "+resultTienda, "Del "+resultFechaInicio+" al "+resultFechaFin, "", dataset,PlotOrientation.VERTICAL,true,true,false);
				
				// Category for bar chart
				CategoryPlot catPlot=chart.getCategoryPlot();
				catPlot.setRangeGridlinePaint(Color.BLACK);
				
				// Agrega chart al panel
				chartPanel=new ChartPanel(chart);
				TopPanel.removeAll();
				TopPanel.add(chartPanel, BorderLayout.CENTER);
				TopPanel.validate();
				
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
			try{
				dateInicio.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
			try{
				dateFin.setCalendar(null);
			}
			catch(NullPointerException npe){
				
			}
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en "Guardar", guarda el grafico
		if(evento.getSource()==btnGuardar){ 
			try {
				// Obtiene informacion a renderizar
				final ChartRenderingInfo info=new ChartRenderingInfo(new StandardEntityCollection());
				//final File file=new File("src/archivos/graficoTienda_"+resultFechaInicio+"_"+resultFechaFin+".png");
				// Se crea un archivo de tipo png
				final File file=new File("archivos/grafico"+resultTienda+"_"+resultFechaInicio+"_"+resultFechaFin+".png");
				resultFechaInicio="";
				resultFechaFin="";
				// Se guarda el grafico como PNG
				ChartUtilities.saveChartAsPNG(file, chart, chartPanel.getMaximumDrawWidth(), chartPanel.getMaximumDrawHeight());
				// Muestra un mensaje al usuario
				JOptionPane.showMessageDialog(null,"El grafico se ha guardado.");
			}
			catch (IOException ioe) {
				// Muestra error
				JOptionPane.showMessageDialog(null, ioe.getMessage());
			}
		}
	} // ActionPerformed
} // Class