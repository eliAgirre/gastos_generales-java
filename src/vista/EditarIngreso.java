package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import utilidades.BD;
import utilidades.Utilidades;

import java.awt.Font;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class EditarIngreso extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtID;
	private JTextField txtCantidad;
	private JDateChooser dateChooser;
	private int id;
	private JTextField txtConcepto;
	private JButton btnEditar;
	private JButton btnVolver;
	private JButton btnBorrar;
	
		// Atributos relacionados con BD
		private String sql;
	
		// Atributos del resultados user
		private String resultFecha;
		private String resultConcepto;
		private Double resultCantidad;
		
	public EditarIngreso(String idIngreso, String fecha, String concepto, String cantidad){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 499, 366); // tama�o
		setTitle("Editar ingreso"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		try {
			// Llama al metodo componentes
			componentes();
			
			// Establece los textos de JTextFields + JComboBox + DateChooser
			txtID.setText(idIngreso);
			txtConcepto.setText(concepto);
			txtCantidad.setText(cantidad);
			// formato de la fecha string
			SimpleDateFormat formatoDate = new SimpleDateFormat("dd/MM/yyyy");
			// convierte la fecha string en Date con el formato anterior
			Date date = (Date) formatoDate.parse(fecha);
			// Muestra la fecha en dateChooser
			dateChooser.setDate(date);
			
		} catch (ParseException e) {
			// Muestra error 
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(10, 304, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Modifique algun dato si es necesario.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setBounds(10, 26, 254, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setBounds(31, 164, 46, 14);
		contentPane.add(lblFecha);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(104, 164, 126, 20);
		dateChooser.getDateEditor().addPropertyChangeListener(
		    new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evento) {
					if (evento.getPropertyName().equals("date")) {
						// Se le da formato a la fecha obtenida
						resultFecha = new SimpleDateFormat("yyyy-MM-dd").format(dateChooser.getDate());
	                }
				}
		    });
		contentPane.add(dateChooser);
		
		JLabel lblConcepto = new JLabel("Concepto:");
		lblConcepto.setForeground(new Color(0, 139, 139));
		lblConcepto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblConcepto.setBounds(255, 117, 95, 14);
		contentPane.add(lblConcepto);
		
		txtConcepto = new JTextField();
		txtConcepto.setText("");
		txtConcepto.setColumns(10);
		txtConcepto.setBounds(328, 114, 126, 20);
		contentPane.add(txtConcepto);
		
		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setForeground(new Color(0, 139, 139));
		lblCantidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantidad.setBounds(255, 164, 95, 14);
		contentPane.add(lblCantidad);
		
		txtCantidad = new JTextField();
		txtCantidad.setText("");
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(328, 161, 126, 20);
		contentPane.add(txtCantidad);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener((ActionListener)this);
		btnEditar.setBounds(216, 250, 108, 41);
		btnEditar.setBackground(new Color(184, 231, 255));
		btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/edit2.png")));
		contentPane.add(btnEditar);	
		
		JLabel lblID = new JLabel("ID:");
		lblID.setForeground(new Color(0, 139, 139));
		lblID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblID.setBounds(29, 117, 48, 14);
		contentPane.add(lblID);
		
		txtID = new JTextField();
		txtID.setText("<dynamic>");
		txtID.setEnabled(false);
		txtID.setColumns(10);
		txtID.setBounds(104, 117, 126, 20);
		contentPane.add(txtID);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setBackground(new Color(184, 231, 255));
		btnVolver.setBounds(96, 250, 108, 41);
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/back.png")));
		contentPane.add(btnVolver);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setBackground(new Color(184, 231, 255));
		btnBorrar.setBounds(336, 250, 111, 41);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/borrar.png")));
		contentPane.add(btnBorrar);
		
	} // Cierre componentes
	
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en Volver y vuelve a la ventana de tabla
		if(evento.getSource()==btnVolver){
			// Se instancia la ventana correspondiente
			TablaIngresos ventanaTablaGastosFijos=new TablaIngresos();
			// Coloca la ventana en el centro de la pantalla
			ventanaTablaGastosFijos.setLocationRelativeTo(null);
			// Hace visible la ventana
			ventanaTablaGastosFijos.setVisible(true);
	  		// Desparece esta ventana
	  		setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnEditar){ 
			boolean validar=false;			
			try{
				validar=Utilidades.validarCamposIngreso(dateChooser.getDate().toString(), txtConcepto.getText(), txtCantidad.getText(), getDateChooser(), getTxtConcepto(), getTxtCantidad());
			}catch(NullPointerException npe){ // Muestra un error
				JOptionPane.showMessageDialog(null, "Introduce los datos, por favor.","Error ",JOptionPane.ERROR_MESSAGE);
			}
			
			if(validar==false){}
			else{
				// Obtener los datos de cada componente de la ventana
				id=Integer.parseInt(txtID.getText());
				resultConcepto=txtConcepto.getText();
				if(Utilidades.esNumero(txtCantidad.getText())==false){
					JOptionPane.showMessageDialog(null, "La 'Cantidad' debe ser n�mero.","Error ",JOptionPane.ERROR_MESSAGE);
				}
				else if(Utilidades.esNumero(txtCantidad.getText())==true){
					resultCantidad=Double.parseDouble(txtCantidad.getText());
					//Sentencia UPDATE
					sql="UPDATE ingresos SET idIngreso="+id+", fecha='"+resultFecha+"', concepto='"+resultConcepto+"', tipo_gasto='"+resultConcepto+"', cantidad='"+resultCantidad+"' WHERE idIngreso='"+id+"'";
					// Ejecuta la actualizacion
					if(BD.actualizar(sql)==true){
						// Muestra un aviso al usuario
						JOptionPane.showMessageDialog(null, "El ingreso se ha editado correctamente.");
						// Se instancia el controlador Principal
				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
				  		// Desparece esta ventana
				  		setVisible(false);
					}
				}
			}
		}// Cierre evento btnMenu
		if(evento.getSource()==btnBorrar){ 
			
			// Obtener el id de la ventana
			id=Integer.parseInt(txtID.getText());
			// Sentencia DELETE
			sql="DELETE FROM ingresos WHERE idIngreso=?;";
			// Elimina el registro
			if(BD.borrarRegistro(sql,id)==true){
				// Muestra aviso al usuario
				JOptionPane.showMessageDialog(null, "El ingreso ha sido borrado.");
				// Se instancia el controlador Principal
		  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
		  		// Desparece la ventana editar Gastos de Tienda
		  		setVisible(false);
			}			
		} // Cierre del btnBorrar
	} // Cierre del metodo actionPerformed
	
	// Getters
	public JDateChooser getDateChooser() {
		return dateChooser;
	}
	public JTextField getTxtCantidad() {
		return txtCantidad;
	}
	public JTextField getTxtConcepto() {
		return txtConcepto;
	}		
} // Cierre clase