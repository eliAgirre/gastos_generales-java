package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPrincipal;
import utilidades.BD;

@SuppressWarnings("serial")
public class TablaPresupuestos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private String id;
	private String fecha;
	private String tipo;
	private String cantidad;
	private JButton btnMenu;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"ID","Fecha","Cantidad","Tipo"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datosBD=new String[4];
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
		// Atributos del resultados user
		
	
	public TablaPresupuestos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 555, 475); // tama�o
		setTitle("Presupuestos a editar"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();		
			// Sentencia SQL SELECT
			sql = "SELECT * FROM presupuestos";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan en el array los datos de BD
				datosBD[0]=rs.getString(1); //ID
				fecha=rs.getString(2);
				int pos=fecha.indexOf("-");
				String year=fecha.substring(0,pos);
				String mes=fecha.substring(pos+1,fecha.length());
				int pos2=mes.indexOf("-");
				mes=mes.substring(0,pos2);
				String dia=fecha.substring(pos2+pos+2,fecha.length());
				//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
				//datosBD[1]=rs.getString(2); //fecha
				datosBD[1]=dia+"/"+mes+"/"+year; //fecha
				datosBD[2]=Double.toString(rs.getDouble(3))+" �"; //cantidad
				datosBD[3]=rs.getString(4); //tipo
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
			
			} // Cierre de while
				
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Constructor
	
	private void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		scrollPane.setEnabled(false);
		scrollPane.setBounds(24, 77, 498, 263);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se establece una anchura de las columnas
		tabla.getColumn("ID").setMinWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setMaxWidth(0); // Se oculta la columna
		tabla.getColumn("ID").setPreferredWidth(0); // Se oculta la columna
		tabla.setEnabled(false);
		// Se a�ade un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
			public void mouseClicked(MouseEvent evento) {
		    	
		    	String nl = System.getProperty("line.separator"); // Salto de linea
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
		        id=(String) modelo.getValueAt(fila,columna);
		        fecha=(String) modelo.getValueAt(fila,1);
		        int pos=fecha.indexOf("/");
				String dia=fecha.substring(0,pos);
				String mes=fecha.substring(pos+1,fecha.length());
				int pos2=mes.indexOf("/");
				mes=mes.substring(0,pos2);
				String year=fecha.substring(pos2+pos+2,fecha.length());
				//System.out.println("a�o: "+year+" mes:"+mes+" dia: "+dia);
				fecha=year+"-"+mes+"-"+dia;
		        cantidad=(String) modelo.getValueAt(fila,2);
		        tipo=(String) modelo.getValueAt(fila,3);
		        JOptionPane.showMessageDialog(null, "Compra a editar: "+nl+nl+"   Fecha: "+fecha+nl+"   Cantidad: "+cantidad+nl+"   Concepto: "+tipo+nl+" ");
		        EditarPresupuesto editarPresupuesto = new EditarPresupuesto(id,fecha,cantidad,tipo);
				// Coloca la ventana en el centro de la pantalla
		        editarPresupuesto.setLocationRelativeTo(null);
				// Hace visible la ventana
		        editarPresupuesto.setVisible(true);
		        // Desaparece esta ventana
				setVisible(false);
				 
		    } // Cierre del mouseClicked
		});	
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 414, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Seleccione un ingreso a editar, por favor.");
		lblAviso.setFont(new Font("Arial", Font.BOLD, 12));
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(24, 36, 254, 14);
		contentPane.add(lblAviso);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(226, 368, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre componentes

	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	

		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		this.setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre clase