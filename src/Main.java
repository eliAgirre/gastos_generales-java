import controlador.ControladorPrincipal;

/**
 * Ejecuta la aplicación.
 * 
 * @see ControladorPrincipal
 */
public class Main {
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		// Declaracion de atributos locales
		ControladorPrincipal ventanaPrincipal;
		// Instancia la clase Principal (controlador)
		ventanaPrincipal = new ControladorPrincipal();

	} // Cierre del metodo main
} // Cierre de la clase