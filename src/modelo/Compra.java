package modelo;

public class Compra {
	
	// Atributos de la clase
	private int idCompra;
	private String fecha;
	private String tipoCompra;
	private String tipoTienda;
	private String tienda;
	private String producto;
	private double coste;
	private String formaPago;
	private String tarjeta;
	
	public Compra(int idCompra, String fecha, String tipoCompra, String tipoTienda, String tienda, String producto, 
			double coste, String formaPago, String tarjeta) {

		this.idCompra = idCompra;
		this.fecha = fecha;
		this.tipoCompra = tipoCompra;
		this.tipoTienda = tipoTienda;
		this.tienda = tienda;
		this.producto = producto;
		this.coste = coste;
		this.formaPago = formaPago;
		this.tarjeta = tarjeta;
		
	} // Cierre constructor

	// Getters
	public int getIdCompra() {
		return idCompra;
	}
	
	public String getFecha(){
		return fecha;
	}

	public String getTipoCompra() {
		return tipoCompra;
	}

	public String getTipoTienda() {
		return tipoTienda;
	}

	public String getTienda() {
		return tienda;
	}

	public String getProducto() {
		return producto;
	}

	public double getCoste() {
		return coste;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	// Setters
	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	
	public void setFecha(String fecha){
		this.fecha = fecha;
	}

	public void setTipoCompra(String tipoCompra) {
		this.tipoCompra = tipoCompra;
	}

	public void setTipoTienda(String tipoTienda) {
		this.tipoTienda = tipoTienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public void setCoste(double coste) {
		this.coste = coste;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

} // Cierre de la clase