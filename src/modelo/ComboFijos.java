package modelo;

public class ComboFijos {
	
	// Atributos de la clase
	private int idFijo;
	private String nombre;
	
	public ComboFijos(String string) {
		
	}
	
	public ComboFijos(int idFijo, String nombre) {
		
		this.idFijo = idFijo;
		this.nombre = nombre;
	} // Cierre constructor

	// Getters
	public int getIdFijo() {
		return idFijo;
	}

	public String getNombre() {
		return nombre;
	}

	// Setters
	public void setIdFijo(int idFijo) {
		this.idFijo = idFijo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		
		return this.nombre;
	} // Cierre toString

} // Cierre de clase