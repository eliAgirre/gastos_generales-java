package modelo;

public class Tipo_tienda {
	
	// Atributos de la clase
	private int idTipoTienda;
	private String tipoTienda;
	
	public Tipo_tienda(String string){}
	
	public Tipo_tienda(int idTipoTienda, String tipoTienda) {
		
		this.idTipoTienda = idTipoTienda;
		this.tipoTienda = tipoTienda;
	} // Cierre constructor

	// Getters
	public int getIdTipoTienda() {
		return idTipoTienda;
	}

	public String getTipoTienda() {
		return tipoTienda;
	}

	// Setters
	public void setIdTipoTienda(int idTipoTienda) {
		this.idTipoTienda = idTipoTienda;
	}

	public void setTipoTienda(String tipoTienda) {
		this.tipoTienda = tipoTienda;
	}

	/**
	 * El tipo de tienda aparece en el JComboBox.
	 * 
	 * @return tipoTienda Devuelve el tipo de tienda en String.
	 * 
    */
	@Override
	public String toString() {
		return tipoTienda;
	} // Cierre toString
		
} // Cierre de clase