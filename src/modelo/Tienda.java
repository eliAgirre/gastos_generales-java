package modelo;

public class Tienda {
	
	// Atributos de la clase
	private int idTienda;
	private String nombreTienda;
	
	public Tienda(String string){}
	
	public Tienda(int idTienda, String nombreTienda){
		
		this.idTienda = idTienda;
		this.nombreTienda = nombreTienda;
	} // Cierre constructor

	// Getters
	public int getIdTienda() {
		return idTienda;
	}

	public String getNombreTienda() {
		return nombreTienda;
	}

	// Setters
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public void setNombreTienda(String nombreTienda) {
		this.nombreTienda = nombreTienda;
	}
	
	// Sirve para que apareca en JComboBox el nombre de la tienda.
	@Override
	public String toString() {
		return nombreTienda;
	} // Cierre toString

} // Cierre clase