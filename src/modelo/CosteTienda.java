package modelo;

public class CosteTienda {
	
	// Atributos de la clase
	private double costeTotal;
	private String nombreTienda;
	
	public CosteTienda(double costeTotal, String nombreTienda) {
		
		this.costeTotal = costeTotal;
		this.nombreTienda = nombreTienda;
	} // Constructor

	// Getters
	public double getCosteTotal() {
		return costeTotal;
	}

	public String getNombreTienda() {
		return nombreTienda;
	}

} // Cierra clase