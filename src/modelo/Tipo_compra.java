package modelo;

public class Tipo_compra {
	
	// Atributos de la clase
	private int idTipo_compra;
	private String tipoCompra;
	
	public Tipo_compra(String string){}
	
	/**
	 * Constructor con parametros todos los atributos.
	 * 
	 * @param idTipo_compra Es un identificador del tipo de compra.
	 * @param tipoCompra Tipo de compra (fisica/online).
	 * 
    */
	public Tipo_compra(int idTipo_compra, String tipoCompra) {

		this.idTipo_compra = idTipo_compra;
		this.tipoCompra = tipoCompra;
	} // Cierre constructor
	
	// Getters
	public int getIdTipo_compra() {
		return idTipo_compra;
	}

	public String getTipoCompra() {
		return tipoCompra;
	}
	
	// Setter 
	public void setIdTipo_compra(int idTipo_compra) {
		this.idTipo_compra = idTipo_compra;
	}

	public void setTipoCompra(String tipoCompra) {
		this.tipoCompra = tipoCompra;
	}
	
	/**
	 * El tipo de compra aparece en el JComboBox.
	 * 
	 * @return tipoCompra Devuelve el tipo de compra en String.
	 * 
    */
	@Override
	public String toString() {
		
		return this.tipoCompra;
	} // Cierre toString	

} // Cierre de clase