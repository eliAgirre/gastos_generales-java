package modelo;

public class Ahorro {
	
	// Atributos de la clase
	private String fechaInicio;
	private String fechaFin;
	private double presu;
	private double gasto;
	private double ahorro;
	
	public Ahorro(String fechaInicio, String fechaFin, double presu, double gasto, double ahorro) {
		
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.presu = presu;
		this.gasto = gasto;
		this.ahorro = ahorro;
		
	} // constructor

	// Getters
	public String getFechaInicio() {
		return fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public double getPresu() {
		return presu;
	}

	public double getGasto() {
		return gasto;
	}

	public double getAhorro() {
		return ahorro;
	}

} // class