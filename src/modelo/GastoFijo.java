package modelo;

/**
 * 
 * Modelo para el grafico.
 *
 */
public class GastoFijo {

	// Atributos de la clase
	private double importe;
	private String fechaDoc;
	
	public GastoFijo(double importe, String fechaDoc) {
		
		this.importe = importe;
		this.fechaDoc = fechaDoc;
		
	} // Constructor

	// Getters
	public double getImporte() {
		return importe;
	}

	public String getFechaDoc() {
		return fechaDoc;
	}

	// Setters
	public void setImporte(double importe) {
		this.importe = importe;
	}

	public void setFechaDoc(String fechaDoc) {
		this.fechaDoc = fechaDoc;
	}
	
} // Cierre clase