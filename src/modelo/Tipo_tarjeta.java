package modelo;

public class Tipo_tarjeta {
	
	// Atributos de la clase
	private int idTarjeta;
	private String nombre;
	
	public Tipo_tarjeta(String s){}
	
	public Tipo_tarjeta(int idTarjeta, String nombre) {
		
		this.idTarjeta = idTarjeta;
		this.nombre = nombre;
	} // Constructor

	// Getters
	public int getIdTarjeta() {
		return idTarjeta;
	}

	public String getNombre() {
		return nombre;
	}

	// Setters
	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	// Sirve para que apareca en JComboBox el nombre de la tarjeta.
	@Override
	public String toString() {
		
		return nombre;
	} // Cierre toString		

} // Cierre de clase