package modelo;

public class Fijos {
	
	// Atributos de la clase
	private int idFijo;
	private String fechaFract;
	private String numFract;
	private String tipoGasto;
	private String fechaInicio;
	private String fechaFin;
	private String duracion;
	private double importe;
	
	
	public Fijos(int idFijo, String fechaFract, String numFract,
			String tipoGasto, String fechaInicio, String fechaFin,
			String duracion, double importe) {
		
		this.idFijo = idFijo;
		this.fechaFract = fechaFract;
		this.numFract = numFract;
		this.tipoGasto = tipoGasto;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.duracion = duracion;
		this.importe = importe;
	
	} // Cierre constructor

	// getters
	public int getIdFijo() {
		return idFijo;
	}
	public String getFechaFract() {
		return fechaFract;
	}
	public String getNumFract() {
		return numFract;
	}
	public String getTipoGasto() {
		return tipoGasto;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public String getDuracion() {
		return duracion;
	}
	public double getImporte() {
		return importe;
	}

	// setters
	public void setIdFijo(int idFijo) {
		this.idFijo = idFijo;
	}
	public void setFechaFract(String fechaFract) {
		this.fechaFract = fechaFract;
	}
	public void setNumFract(String numFract) {
		this.numFract = numFract;
	}
	public void setTipoGasto(String tipoGasto) {
		this.tipoGasto = tipoGasto;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
} // Cierre de la clase