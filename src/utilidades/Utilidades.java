package utilidades;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

public class Utilidades {
	
	//M�todo para saber si todos los campos estan rellenados
	public static boolean validarCamposCompra(String fecha,String tipoCompra,String tipoTienda,String tienda, String producto, String importe, String formaPago, String tarjeta, JTextField txtProducto, JTextField txtCoste){
			
		boolean correcto=true;		
		
		if(fecha.equals("")|| fecha.equals("dd/MM/yyyy")){
			
			JOptionPane.showMessageDialog(null, "Introduce la 'Fecha Compa'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(producto.equals("")){
			JOptionPane.showMessageDialog(txtProducto, "Introduce el 'Producto'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(importe.equals("")){
			JOptionPane.showMessageDialog(txtCoste, "Introduce el 'Importe'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		
		if(tipoCompra.equals("")){
			JOptionPane.showMessageDialog(null, "Introduce la 'Forma Compra'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(tipoTienda.equals("")){
			JOptionPane.showMessageDialog(null, "Introduce el 'Tipo Tienda'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(formaPago.equals("")){
			JOptionPane.showMessageDialog(null, "Introduce la 'Forma Pago'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(tarjeta.equals("")){
			JOptionPane.showMessageDialog(null, "Introduce la 'Tarjeta'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		return correcto;
	}
	
	//M�todo para saber si todos los campos estan rellenados
	public static boolean validarCamposGastosFijos(String fecha,String numFactura,String fechaInicio,String fechaFn, String tipoGasto, String duracion, String importe, JTextField txtNumFactura, JTextField txtDuracion, JTextField txtImporte){
			
		boolean correcto=true;		
		
		if(fecha.equals("")|| fecha.equals("dd/MM/yyyy")){
			
			JOptionPane.showMessageDialog(null, "Introduce la 'Fecha Doc'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(numFactura.equals("")){
			JOptionPane.showMessageDialog(txtNumFactura, "Introduce la 'N� Factura'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(fechaInicio.equals("")|| fechaInicio.equals("dd/MM/yyyy")){
			JOptionPane.showMessageDialog(null, "Introduce el 'Fecha Inicio'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(fechaFn.equals("")|| fechaFn.equals("dd/MM/yyyy")){
			JOptionPane.showMessageDialog(null, "Introduce el 'Fecha Fin'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(duracion.equals("")){
			JOptionPane.showMessageDialog(txtDuracion, "Introduce la 'Duracion'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(importe.equals("")){
			JOptionPane.showMessageDialog(txtImporte, "Introduce la 'Importe'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(tipoGasto.equals("")){
			JOptionPane.showMessageDialog(null, "Introduce la 'Tipo Gasto'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}

		return correcto;
	}
	
	//M�todo para saber si todos los campos estan rellenados
	public static boolean validarCamposIngreso(String fecha, String concepto, String cantidad , JDateChooser dataChooser, JTextField txtConcepto, JTextField txtCantidad){
			
		boolean correcto=true;		
		
		if(fecha.equals("")|| fecha.equals("dd/MM/yyyy")){
			
			JOptionPane.showMessageDialog(dataChooser, "Introduce la 'Fecha'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(concepto.equals("")){
			JOptionPane.showMessageDialog(txtConcepto, "Introduce el 'Concepto'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(cantidad.equals("")){
			JOptionPane.showMessageDialog(txtCantidad, "Introduce la 'Cantidad'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		return correcto;
	}
	
	//M�todo para saber si todos los campos estan rellenados
	public static boolean validarCamposPresu(String fecha, String presu, JDateChooser dataChooser, JTextField txtPresu){
				
		boolean correcto=true;		
		
		if(fecha.equals("")|| fecha.equals("dd/MM/yyyy")){
			
			JOptionPane.showMessageDialog(dataChooser, "Introduce la 'Fecha'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		if(presu.equals("")){
			JOptionPane.showMessageDialog(txtPresu, "Introduce el 'Presupuesto'.","Campo vac�o", JOptionPane.WARNING_MESSAGE);
			correcto=false;
		}
		return correcto;
	}
	
	//Comprobar si la cadena es n�mero.
    public static boolean esNumero(String cadena){
	    try {
	    	Double.parseDouble(cadena);
	    	return true;
	    } catch (NumberFormatException nfe){
	    	return false;
	    }
    }
    
    // Obtiene la hora y fecha del sistema
 	public static Timestamp obtenerHoraSistema(){
 		
 		// Se crea el objeto calendar y obtiene la hora del sistema
 		Calendar cal = Calendar.getInstance();
 		// Se convierte el objeto calendar a timestamp
 		Timestamp ts = new Timestamp(cal.getTimeInMillis()); 
 		// Devuelve fecha y hora en Timestamp 
 		return ts;
 	} //Cierre obtenerHoraSistema
 	
	// Metodo para limpiar los datos de la tabla.
	public static void limpiarTabla(DefaultTableModel modelo) {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	// Crear y modificar  un fichero log de Compras
	public static void comprasLog(String evento, int idCompra, String fecha_compra, String tipoTienda, String tienda, String coste, String formaPago, String producto){
		
		try {
			
			//FileWriter ficheroW=new FileWriter("src/archivos/compras.log",true);
			FileWriter ficheroW=new FileWriter("archivos/compras.log",true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora=Utilidades.obtenerHoraSistema();
			
			String datos=evento+"   "+hora +"   "+Integer.toString(idCompra)+"   "+fecha_compra+"         "+tipoTienda+"          "+tienda+"      "+coste+"          "+formaPago+"      "+producto;

			bW.write(datos);
			bW.newLine();
			bW.newLine();
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	} //Cierre comprasLog
	
	// Crear y modificar  un fichero log de Gastos Fijos
	public static void fijosLog(String evento, int idGastoFijo, String documento, String nombreGasto, String importe, String fecha_inicio, String fecha_fin, String fecha_doc){
		
		try {
			
			//FileWriter ficheroW=new FileWriter("src/archivos/gastosFijos.log",true);
			FileWriter ficheroW=new FileWriter("archivos/gastosFijos.log",true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora=Utilidades.obtenerHoraSistema();
			
			String datos=evento+"   "+hora +"   "+Integer.toString(idGastoFijo)+"   "+documento+"         "+nombreGasto+"          "+importe+"      "+fecha_inicio+"          "+fecha_fin+"      "+fecha_doc;

			bW.write(datos);
			bW.newLine();
			bW.newLine();
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	} //Cierre fijosLog
}
