package utilidades;

import java.sql.*;
import javax.swing.*;

/**
 * 
 * Hace conexion con la base de datos MySQL.
 *
 */

public class BD {
	
	//Atributos de la clase estaticos
	private static Connection conexionMYSQL=null;
	private static Statement stmt = null;
	private static boolean empty=true;
	private static PreparedStatement ps=null;
	
	/**
	 * Utiliza el driver para conectarse con MySQL y muestra excepcion si no se ha conectado.
	 * 
	 * @throws SQLException Muestra aviso de que no se ha conectado.
	 *
	 */
	public static void conectar() throws SQLException {
        try {
        	Class.forName("org.gjt.mm.mysql.Driver"); //El driver 
        	// Conexion localhost
        	//conexionMYSQL= DriverManager.getConnection("jdbc:mysql://localhost/gastos","root", "");
        	conexionMYSQL = DriverManager.getConnection("jdbc:mysql://localhost:3307/gastos", "root", "usbw");
        	//Conexion clearDB OpenShift
        	//conexionMYSQL= DriverManager.getConnection("jdbc:mysql://us-cdbr-iron-east-03.cleardb.net:3306/ad_9b045c92334d409","bf39c9a3599080","3b1966e5");
        	
        } 
        catch (Exception e) {        	
        	//Aviso cuando no es posible conectar
        	JOptionPane.showMessageDialog(null, "Imposible realizar la conexion a BD.","Error ",JOptionPane.ERROR_MESSAGE);
        }
    }  // Cierre metodo conectar

	 /**
     * Establece la conexion con la base de datos y devuelve el estado de la conexion.
     *
     * @return Statement Devuelve el estado de la conexion.
     */
	public static Statement conexion() {		
        try {        	
        	 stmt = conexionMYSQL.createStatement();	
        } 
        catch (SQLException e) {        	
        	//Aviso cuando la conexion es incorrecta
        	JOptionPane.showMessageDialog(null, "Conexion incorrecta.","Error ",JOptionPane.ERROR_MESSAGE);
        }        
        return stmt;
       
    }  // Cierre de metodo conexion
	
	/**
	 * Comprueba si la tabla esta vacia o no.
	 *
	 * @param stmt Conexion con la base de datos.
	 * @param cadena La sentencia SQL SELECT.
	 * @throws SQLException Muestra el error si la sentencia SELECT no es adecuada.
	 * @return empty Devuelve true o false.
	   */
	public static boolean tablaVacia(Statement stmt,String cadena) throws SQLException{
		
		// Definicion del atributo local para el resultado
		ResultSet rs = null;
		 
		try {
			 // Se conecta con MySQL pasando la sentencia SQL
			 stmt = conexionMYSQL.prepareStatement(cadena);
			 // Se guarda el resultado de la consulta
			 rs = stmt.executeQuery(cadena); 
			 // Si tiene datos, la tabla no esta vacia
			 while( rs.next() ) {
				 // Establece el valor false
				 empty = false;
			 }
		  
		}catch (SQLException sql) {			 
			// Muestra excepcion si la sentencia SELECT no esta bien escrita
			JOptionPane.showMessageDialog(null, "Error con: " + cadena,"Error ",JOptionPane.ERROR_MESSAGE);			
			// Muestrar el error si se ha producido un error de SQL
		 	JOptionPane.showMessageDialog(null, "SQLException: " + sql.getMessage(),"SQL ",JOptionPane.ERROR_MESSAGE);
		}
		// Devuelve true o false
		return empty;
		
	} // Cierre del metodo tablaVacia
	
	 /**
	 * Se realizan las consultas de tipo: SELECT * FROM tabla WHERE..." y muestra el error acerca de la SQL.
	 *
	 * @param stmt Conexion con la base de datos.
	 * @param cadena La sentencia SQL SELECT.
	 * @throws SQLException Muestra el error si la sentencia SELECT no es adecuada.
	 * @return ResultSet Devuelve el resultado de la consulta realizada.
	   */
	 public static ResultSet consulta(Statement stmt,String cadena) throws SQLException {
		 // Definicion del atributo local para el resultado
		 ResultSet rs = null;
		 
		 try {
			 // Se conecta con MySQL pasando la sentencia SQL
			 stmt = conexionMYSQL.prepareStatement(cadena);
			 // Se guarda el resultado de la consulta
			 rs = stmt.executeQuery(cadena); 
          
		 }catch (SQLException sql) {			 
			// Muestra excepcion si la sentencia SELECT no esta bien escrita
			JOptionPane.showMessageDialog(null, "Error con: " + cadena,"Error ",JOptionPane.ERROR_MESSAGE);
			
			// Muestrar el error si se ha producido un error de SQL
         	JOptionPane.showMessageDialog(null, "SQLException: " + sql.getMessage(),"SQL ",JOptionPane.ERROR_MESSAGE);
		 }
		 // Devuelve el resultado de la consulta
		 return rs;
		 
	 } // Cierre del metodo consulta	 
	 
	 /**
     * Realiza las consultas de actualizacion o insercion. No retorna nada pero si la cadena no es adecuada el resultado es -1.
     *
     * @param cadena La consulta en concreto
     */
	@SuppressWarnings("unused")
	public static boolean actualizar(String cadena){ 
		
		boolean actualizado=false;
		int rs=-1;			
        try {
        	// Actualiza los datos de la BD
            rs=stmt.executeUpdate(cadena);	
            actualizado=true;
        }
        catch (SQLException sql) {
        	actualizado=false;
        	// Muestra excepcion si la sentencia SELECT no esta bien escrita
			JOptionPane.showMessageDialog(null, "Error con: " + cadena,"Error ",JOptionPane.ERROR_MESSAGE);
			// Muestrar el error si se ha producido un error de SQL
         	JOptionPane.showMessageDialog(null, "SQLException: " + sql.getMessage(),"SQL ",JOptionPane.ERROR_MESSAGE);
        }
        return actualizado;
	 } //Cierre del metodo actualizar
	
	/**
     * Elimina registro de una tabla.
     *
     * @param cadena La sentencia SQL.
     * @param id El id para eliminar la fila correspondiente.
     */
	 public static boolean borrarRegistro(String cadena,int id){
		 
		 boolean borrado=false;		 
		 try {			 
		 	// Prepara la conexion obteniendo la cadena SQL
			ps=conexionMYSQL.prepareStatement(cadena);
			ps.setInt(1, id); //establece el numero de campo y el dato
		 	ps.executeUpdate(); //Ejecuta la accion		
		 	borrado=true;
		 }
		 catch (SQLException sql) {
			borrado=false;
        	// Muestra excepcion si la sentencia SELECT no esta bien escrita
			JOptionPane.showMessageDialog(null, "Error con: " + cadena,"Error ",JOptionPane.ERROR_MESSAGE);
			
			// Muestrar el error si se ha producido un error de SQL
         	JOptionPane.showMessageDialog(null, "SQLException: " + sql.getMessage(),"SQL ",JOptionPane.ERROR_MESSAGE);
        }
		return borrado;
	 } //Cierre de borrarRegistro
	
	 /**
     * Cierra la consulta o muestra error de que no se ha podido cerrar.
     *
     * @param rs Resultado de la consulta.
     * @throws SQLException Muestra el error de la consulta.
     */
	public static void cerrarConsulta(ResultSet rs) throws SQLException {
		 
		 if (rs!= null){ //Si no hay datos de la consulta			 
            try{
            	// Cierra la consulta
                rs.close();                 
            }
            catch (Exception e){            	
            	// Muestra el error cuando no se ha podido cerrar la consulta
            	JOptionPane.showMessageDialog(null, "No es posible cerrar la consulta.","Error ",JOptionPane.ERROR_MESSAGE);
            }            
	     } //Cierre del if
		 
	} // Cierre del metodo cerrarConsulta
	
	/**
     * Cierra la conexion con la MySQL y muestra un error si no se ha podido cerrar.
     *
     * @throws SQLException Muestra el error de la conexion MySQL.
     */
	 public static void cerrar() throws SQLException {
        try{
        	// Cerrar conexion MySQL
        	conexionMYSQL.close();        	 
        }catch(Exception e){
        	// Muestrar el error cuando se ha podido cerrar la conexion MySQL
    		JOptionPane.showMessageDialog(null, "Mal al cerrar la conexion","Error ",JOptionPane.ERROR_MESSAGE);
        }
	 } // Cierre del metodo cerrar
	
} // Cierre de clase