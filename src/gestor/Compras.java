package gestor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modelo.Compra;
import utilidades.BD;

public class Compras {
	
	// Atributos de la clase
	private ArrayList<Compra> gestorCompra;
	private int id;
	private String fecha;
	private String tipoCompra;
	private String tipoTienda;
	private String tienda;
	private String producto;
	private double coste;
	private String formaPago;
	private String tarjeta;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
	
	public Compras(){
		
		gestorCompra=new ArrayList<Compra>();		
	} // Cierre constructor
	
	public void anadir(Compra compra){
		
		gestorCompra.add(compra);		
	} // Cierre anadir
	
	/**
	 * 
     * Devuelve el ultimo id de la base de datos.
	 * @throws SQLException 
     * 
     */
	public int ultimoID() throws SQLException{
		
		// Sentencia SQL SELECT
		sql = "SELECT MAX(idCompra) FROM compras;";
		rs = BD.consulta(stmt, sql);
		// Si hay datos
		if (rs.next()){
			id=rs.getInt(1);
        }
		// Devuelve el ultimo ID
		return id;		
	} // Cierre del metodo ultimoID
	
	/**
	 * 
     * Inserta los datos del arraylist en la BD.
     * 
     */
	public boolean insertarBD(){
		
		boolean insertado=false;		
		// Se recorre el arraylist
		for(int i=0;i<gestorCompra.size();i++){
			
			// Se obtienen los datos de la clase Gastos_T
			id=gestorCompra.get(i).getIdCompra();
			fecha=gestorCompra.get(i).getFecha();
			tipoCompra=gestorCompra.get(i).getTipoCompra();
			tipoTienda=gestorCompra.get(i).getTipoTienda();
			tienda=gestorCompra.get(i).getTienda();
			producto=gestorCompra.get(i).getProducto();
			coste=gestorCompra.get(i).getCoste();
			formaPago=gestorCompra.get(i).getFormaPago();
			tarjeta=gestorCompra.get(i).getTarjeta();
			
			// Sentencia INSERT
			sql="INSERT INTO compras VALUES ("+id+",'"+fecha+"','"+tipoCompra+"','"+tipoTienda+"','"
			+tienda+"','"+producto+"',"+coste+",'"+formaPago+"','"+tarjeta+"');";
			if(BD.actualizar(sql)==true){ //Ejecuta la actualizacion
				insertado=true;
			}
		}		
		return insertado;
	} // Cierre del metodo insertarBD
} // Cierre de la clase