package gestor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modelo.Fijos;
import utilidades.BD;

public class GastosFijos {
	
	// Atributos de la clase
	private ArrayList<Fijos> gestorGastosFijos;
	private int id;
	private String fechaFract;
	private String numFract;
	private String tipoGasto;
	private String fechaInicio;
	private String fechaFin;
	private String duracion;
	private double importe;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
		
	public GastosFijos(){
		
		gestorGastosFijos=new ArrayList<Fijos>();
				
	} // Cierre constructor
	
	public void anadir(Fijos gastoFijo){
		
		gestorGastosFijos.add(gastoFijo);
		
	} // Cierre anadir
	
	/**
	 * 
     * Devuelve el ultimo id de la base de datos.
	 * @throws SQLException 
     * 
     */
	public int ultimoID() throws SQLException{
		
		// Sentencia SQL SELECT
		sql = "SELECT MAX(idGasto) FROM gastos_fijos;";
		rs = BD.consulta(stmt, sql);
		// Si hay datos
		if (rs.next()){
			id=rs.getInt(1);
        }
		// Devuelve el ultimo ID
		return id;		
	} // Cierre del metodo ultimoID
	
	/**
	 * 
     * Inserta los datos del arraylist en la BD.
     * 
     */
	public boolean insertarBD(){
		
		boolean insertado=false;
		// Se recorre el arraylist
		for(int i=0;i<gestorGastosFijos.size();i++){
			
			// Se obtienen los datos de la clase Gastos_T
			id=gestorGastosFijos.get(i).getIdFijo();
			fechaFract=gestorGastosFijos.get(i).getFechaFract();
			numFract=gestorGastosFijos.get(i).getNumFract();
			tipoGasto=gestorGastosFijos.get(i).getTipoGasto();
			fechaInicio=gestorGastosFijos.get(i).getFechaInicio();
			fechaFin=gestorGastosFijos.get(i).getFechaFin();
			duracion=gestorGastosFijos.get(i).getDuracion();
			importe=gestorGastosFijos.get(i).getImporte();
			// Sentencia INSERT
			sql="INSERT INTO gastos_fijos VALUES ("+id+",'"+fechaFract+"','"+numFract+"','"+tipoGasto+"','"
			+fechaInicio+"','"+fechaFin+"',"+duracion+",'"+importe+"');";
			//Ejecuta la actualizacion
			if(BD.actualizar(sql)==true){ 
				insertado=true;
			}
		}	
		return insertado;
	} // Cierre del metodo insertarBD

} // Cierre de la clase