-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-03-2016 a las 18:44:44
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gastos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ahorro_mensual`
--

CREATE TABLE `ahorro_mensual` (
  `idMensual` int(11) NOT NULL,
  `fecha_inicio` varchar(10) NOT NULL,
  `fecha_fin` varchar(10) NOT NULL,
  `presu` double(9,2) NOT NULL,
  `gasto` double(9,2) NOT NULL,
  `ahorroMensual` double(9,2) NOT NULL,
  PRIMARY KEY (`idMensual`),
  UNIQUE KEY `idMensual_UNIQUE` (`idMensual`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idCompra` int(11) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  `tipo_compra` varchar(6) NOT NULL,
  `tipo_tienda` varchar(45) NOT NULL,
  `tienda` varchar(45) NOT NULL,
  `producto` varchar(45) DEFAULT NULL,
  `coste` double(9,2) NOT NULL,
  `forma_pago` varchar(15) NOT NULL,
  `tarjeta` varchar(15) NOT NULL,
  PRIMARY KEY (`idCompra`),
  UNIQUE KEY `idCompras_UNIQUE` (`idCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fijos`
--

CREATE TABLE `fijos` (
  `idFijos` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `meses` int(2) DEFAULT NULL,
  PRIMARY KEY (`idFijos`),
  UNIQUE KEY `idFijos_UNIQUE` (`idFijos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_pago`
--

CREATE TABLE `forma_pago` (
  `idFormaPago` int(1) NOT NULL,
  `tipo_pago` varchar(15) NOT NULL,
  `idTipoCompra` int(1) NOT NULL,
  PRIMARY KEY (`idFormaPago`),
  KEY `idCompra_idx` (`idTipoCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos_fijos`
--

CREATE TABLE `gastos_fijos` (
  `idGasto` int(11) NOT NULL,
  `fecha_factura` varchar(10) NOT NULL,
  `num_factura` varchar(45) DEFAULT NULL,
  `tipo_gasto` varchar(45) NOT NULL,
  `fecha_inicio` varchar(10) NOT NULL,
  `fecha_fin` varchar(10) NOT NULL,
  `duracion` varchar(45) NOT NULL,
  `importe` double(9,2) NOT NULL,
  PRIMARY KEY (`idGasto`),
  UNIQUE KEY `idGasto_UNIQUE` (`idGasto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

CREATE TABLE `ingresos` (
  `idIngreso` int(11) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  `concepto` varchar(50) NOT NULL,
  `cantidad` double(9,2) NOT NULL,
  PRIMARY KEY (`idIngreso`),
  UNIQUE KEY `idIngreso_UNIQUE` (`idIngreso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuestos`
--

CREATE TABLE `presupuestos` (
  `idPresu` int(11) NOT NULL,
  `fecha_presu` varchar(10) NOT NULL,
  `cantidad` double(9,2) NOT NULL,
  `tipo` varchar(7) NOT NULL,
  PRIMARY KEY (`idPresu`),
  UNIQUE KEY `idPresu_UNIQUE` (`idPresu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semanal`
--

CREATE TABLE `semanal` (
  `idSemanal` int(11) NOT NULL,
  `fecha_inicio` varchar(10) NOT NULL,
  `fecha_fin` varchar(10) NOT NULL,
  `presu` double(9,2) NOT NULL,
  `gasto` double(9,2) NOT NULL,
  `ahorro` double(9,2) NOT NULL,
  PRIMARY KEY (`idSemanal`),
  UNIQUE KEY `idSemanal_UNIQUE` (`idSemanal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiendas`
--

CREATE TABLE `tiendas` (
  `idTienda` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `idTipo` int(2) DEFAULT NULL,
  PRIMARY KEY (`idTienda`),
  UNIQUE KEY `idTienda_UNIQUE` (`idTienda`),
  KEY `idTipo_idx` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_compra`
--

CREATE TABLE `tipo_compra` (
  `idCompra` int(1) NOT NULL,
  `tipo` varchar(6) NOT NULL,
  PRIMARY KEY (`idCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_tarjetas`
--

CREATE TABLE `tipo_tarjetas` (
  `idTarjeta` int(1) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `idTipoTarjeta` int(1) NOT NULL,
  PRIMARY KEY (`idTarjeta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_tienda`
--

CREATE TABLE `tipo_tienda` (
  `idTipo` int(2) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `forma_pago`
--
ALTER TABLE `forma_pago`
  ADD CONSTRAINT `idCompra` FOREIGN KEY (`idTipoCompra`) REFERENCES `tipo_compra` (`idCompra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD CONSTRAINT `idTipo` FOREIGN KEY (`idTipo`) REFERENCES `tipo_tienda` (`idTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
